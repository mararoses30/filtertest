# Change Log

## Version {{version}} - {{date}}


### New Features

{{#each commits.features}}
* {{this}}
{{else}}
* No features
{{/each}}

### Updates
{{#each commits.updates}}
* {{this}}
{{else}}
* No updates
{{/each}}

### Bug Fixes
{{#each commits.bugfixes}}
* {{this}}
{{else}}
* No bug fixes
{{/each}}
