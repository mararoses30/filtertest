/*
 * BalancePanel
 * ============================================================================
 *
 * Displays the current balance. Wrapper for better positioning, etc
 */

import {DynamicText} from '../lib/DynamicText';

class BalancePanel extends Phaser.Group {
  constructor (gameRef, gameContainerGroup) {
    super(gameRef, gameContainerGroup, 'BalancePanel', false);
    this.game = gameRef;

    this.balancePos = this.game.cache.getJSON('layout')['layout']['balance'][0];

   // console.log(this.balancePos);
    let fontsize = 80 * this.game.SCALE_FACTOR;

    this.style = { fontFamily: 'Roboto',
                  fontSize: fontsize,
                  fontWeight: 'normal',
                  fill: '#fff',
                  boundsAlignH: 'left',
                  boundsAlignV: 'middle',
                  align: 'left',
                  // margin: {left:0},
                  padding: {left:5, right:5, top:0, bottom:0},
                  };

    this.balanceText = new DynamicText(this.game,
                          this.game.locale.get('g_balance'),
                          this.balancePos.x, 
                          this.balancePos.y,
                          413 * this.game.SCALE_FACTOR, 
                          37 * this.game.SCALE_FACTOR,
                          this.clone(this.style));

    this.balanceText.align = 'left';
    this.add(this.balanceText);


   


    this.ticketIDStyle = {  font: 20 * this.game.SCALE_FACTOR + 'px Roboto',
                            fill: '#fff',
                      align: 'center',
                      wordWrap: false
                      };

    this.ticketIDPos = this.game.cache.getJSON('layout')['layout']['ticket'][0];
    this.ticketIDText = new Phaser.Text(this.game,
                                          this.ticketIDPos.x/*balancePos.x -26 * gameRef.SCALE_FACTOR*/,
                                          this.ticketIDPos.y/*balancePos.y + 30 * gameRef.SCALE_FACTOR*/,
                                          '',
                                          this.ticketIDStyle);

    this.add(this.ticketIDText);
    this.ticketIDText.visible = false;
    if(this.game.api.config.displayTicketId){

      this.ticketIDText.visible = true;

    }

  }


  resetTicket (){
    this.ticketIDText.setText('');
  }

  set ticketId (value){
    this.ticketIDText.setText(value);
  }

  set balance (value) {
    this.balanceText.setText(this.game.locale.get('g_balance') + ' ' + this.game.locale.formatCurrency(value, this.game.api.config.currencyPrecision));
  }

  clone (obj){
    var clone = {};
    clone.prototype = obj.prototype;
    for (var property in obj){
      clone[property] = obj[property];
    }
    return clone;
  }
}

export default BalancePanel;
