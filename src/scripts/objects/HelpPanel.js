/*
 * HelpPanel
 * ============================================================================
 *
 * Panel displaying game instructions
 */

import {TAtlasButton} from '../lib/buttons';
import {DynamicCompositeText} from '../lib/DynamicCompositeText';
import {DynamicText} from '../lib/DynamicText';

class HelpPanel extends Phaser.Group {

  constructor (game, parent) {
    super(game, parent, 'HelpPanel', false);

    this.semVersion = document.getElementById('version').innerHTML;

    /* Add Shadow when help is displayed*/
    this.shadowSprite = new Phaser.Sprite(game, 0, 0, 'background');
    this.shadowSprite.alpha = 0;
    this.shadowSprite.visible = true;
    this.add(this.shadowSprite);

    //initial alpha
    this.alpha = 0;

    // add the help's background
    this.bg = new  Phaser.Sprite(game, 0, 0, 'help');
    this.bg.x = parent.width/2;
    this.bg.anchor.x = 0.5;
    this.add(this.bg);

    // Add version number
    var style = {font: 20 * this.game.SCALE_FACTOR + 'px Roboto',
                      fill: '#fff',
                      align: 'left', // the alignment of the text is independent of the bounds, try changing to 'center' or 'right'
                      wordWrap: true,
                      stroke : 'black',

                      wordWrapWidth: this.bg.width * this.game.SCALE_FACTOR
                     };
    // this.howToPlayText = new Phaser.Text(
    //                   this.game,
    //                   -90 * this.game.SCALE_FACTOR,
    //                   153  * this.game.SCALE_FACTOR,
    //                   this.game.locale.get('g_how_to_play'),
    //                   this.clone(style));
    // this.howToPlayText.fontSize = 38 * this.game.SCALE_FACTOR;

    // this.bg.addChild(this.howToPlayText);
    // window.bgtext = this.howToPlayText;

    this.versionText = new Phaser.Text(
                      this.game,
                      0,
                      0,
                      this.semVersion,
                      style);


    this.bg.addChild(this.versionText);
    this.versionText.y = this.bg.height - 50 * this.game.SCALE_FACTOR;
    this.versionText.x = this.bg.width/2 - 120 * this.game.SCALE_FACTOR;

    var btnStyle = {
      fontFamily: 'Arial',
      fontSize: 10,
      fontWeight: 'bold',
      fill: '#ffd700',
      overFill: '#d3d3d3',
      offFill: '#5a615d',
      align: 'center',
      boundsAlignH: 'center',
      boundsAlignV: 'middle',
      padding: {left:30, right:30, top:0, bottom:0},
      shadowFill: true,
      shadowColor: '#ffac00',
      overShadowColor: '#c8c8c8',
      shadowBlur: 20,
      wordWrap: true
    };

    // Add button to close help screen
    this.closeBtn = new TAtlasButton(game, 0, 0, 'menu_ui', this.onCloseHelpBtnClicked, this,  'BUTTON', 'PlayTheGameButton_OVER', 'PlayTheGameButton_ON', 'PlayTheGameButton_OFF', {text: this.game.locale.get('g_help_return'), style: btnStyle});
    this.closeBtn.visible = false;
    this.closeBtn.alpha = 0;
    this.add(this.closeBtn);

    // Add button to close help screen
    this.gameRulesBtn = new TAtlasButton(game, 0, 0, 'menu_ui', this.onGameRulesBtnClicked, this,  'BUTTON', 'GameRules_OVER', 'GameRules_ON', 'GameRules_OFF', {text: this.game.locale.get('g_help_game_rules'), style: btnStyle, boundingBox: null});
    this.gameRulesBtn.visible = false;
    this.gameRulesBtn.alpha = 0;
    this.add(this.gameRulesBtn);

    // Add close button event that will propagete to the game
    // Added help text
    var boundsBoxText = {
      x: 669 * this.game.SCALE_FACTOR, //669
      y: 200 * this.game.SCALE_FACTOR,
      width: 1200 * this.game.SCALE_FACTOR,
      height: 580 * this.game.SCALE_FACTOR
    };



    let listStyle = {
      fontFamily: 'Arial',
      fontSize: 26 * this.game.SCALE_FACTOR,
      fontWeight: 'bold',
      fill: '#fff',
      offFill: '#5a615d',
      align: 'center',
      boundsAlignH: 'center',
      boundsAlignV: 'middle',
      padding: {left:30, right:30, top:0, bottom:0},
      wordWrap: true,
      wordWrapWidth: boundsBoxText.width,
      margin: {left:10 * this.game.SCALE_FACTOR},
      padding:{x:0 * this.game.SCALE_FACTOR, y:0 * this.game.SCALE_FACTOR}
    };

    var headerBoundsBoxText = {
      x: 500 * this.game.SCALE_FACTOR,
      y: 153 * this.game.SCALE_FACTOR,
      width: 1400 * this.game.SCALE_FACTOR,
      height: 50 * this.game.SCALE_FACTOR
    };

        // How to play
    var headerText = new DynamicText(this.game,
      this.game.locale.get('g_how_to_play'),
      headerBoundsBoxText.x,
      headerBoundsBoxText.y,
      headerBoundsBoxText.width,
      headerBoundsBoxText.height,
      this.clone(listStyle)
    );


    // var graphics = game.add.graphics(0,0)
    // graphics.lineStyle(1, 0xffd900 ,1)
    // graphics.drawRect(headerText.x, headerText.y, headerText.width, headerText.height)

    this.addChild(headerText);

    listStyle.margin.left = 25 * this.game.SCALE_FACTOR;
    listStyle.fill = 'white';
    listStyle.align = 'left';
    listStyle.boundsAlignH = 'left';

    let text = new DynamicCompositeText(game,
      this,
      this.game.locale.get('g_help_screen_1'),
      [
        {
          '${img_help_symbol_choose}':{
            'atlas':'help_atlas',
            'frame': 'img_help_symbol_choose',
          }
        }
      ],
      boundsBoxText.x,
      boundsBoxText.y,
      boundsBoxText.width,
      boundsBoxText.height,
      this.clone(listStyle), 
      0.6,
      -0.2,
      0
      );
    this.addChild(text);

    
    var btnWagerStyle = {
      fontFamily: 'Arial',
      fontSize: 9 * this.game.SCALE_FACTOR,
      fontWeight: 'bold',
      fill: '#ffd000',
      offFill: '#5a615d',
      align: 'center',
      boundsAlignH: 'right',
      boundsAlignV: 'middle',
      padding: {left:90, right:-10, top:0, bottom:0},
      shadowFill:false,
      shadowColor: '#ffe900',
      shadowBlur: 10,
      wordWrap: true
    };
    var text1 = new DynamicCompositeText(game,
      this,
      this.game.locale.get('g_help_screen_2'),
      [
        {
          '${img_help_wager}':{
            'atlas':'help_atlas',
            'frame': 'img_help_wager',
            'text':  this.game.locale.get('img_help_wager'),
            'style': this.clone(btnWagerStyle)
          }
        },
        {
          '${img_help_arrowsbtn}':{
            'atlas':'help_atlas',
            'frame': 'img_help_arrowsbtn'
          }
        }
      ],

      boundsBoxText.x,
      boundsBoxText.y + text.height,
      boundsBoxText.width,
      boundsBoxText.height,
      this.clone(listStyle),
      0.7,
      0,
      -0.2
    );

    this.addChild(text1);

    var text2 = new DynamicCompositeText(game,
      this,
      this.game.locale.get('g_help_screen_3'),
      [
        {
          '${img_help_playbtn}':{
            'atlas':'help_atlas',
            'frame': 'img_help_playbtn',
          }
        }
      ],
      boundsBoxText.x,
      boundsBoxText.y + text.height + text1.height,
      boundsBoxText.width,
      boundsBoxText.height,
      this.clone(listStyle),
      0.75,
      0,
      -0.15
    );

    this.addChild(text2);

    var text3 = new DynamicCompositeText(game,
      this,
      this.game.locale.get('g_help_screen_4'),
      [],
      boundsBoxText.x,
      boundsBoxText.y + text.height + text1.height + text2.height,
      boundsBoxText.width,
      boundsBoxText.height,
      this.clone(listStyle),
      0.75,
      0,
      -0.15
    );

    this.addChild(text3);

    var text4 = new DynamicCompositeText(game,
      this,
      this.game.locale.get('g_help_screen_5'),
      [],
      boundsBoxText.x,
      boundsBoxText.y + text.height + text1.height + text2.height + text3.height,
      boundsBoxText.width,
      boundsBoxText.height,
      this.clone(listStyle),
      0.75,
      0,
      -0.15
    );

    this.addChild(text4);

    var text5 = new DynamicCompositeText(game,
      this,
      this.game.locale.get('g_help_screen_6'),
      [
        {
          '${img_help_homebtn}':{
            'atlas':'help_atlas',
            'frame': 'img_help_homebtn',
          }
        }
      ],
      boundsBoxText.x,
      boundsBoxText.y + text.height + text1.height + text2.height + text3.height + text4.height,
      boundsBoxText.width,
      boundsBoxText.height,
      this.clone(listStyle),
      0.75,
      0,
      -0.2
    );

    this.addChild(text5);

    this.events = [];
    this.events.onCloseHelpBtnClickedEvent = new Phaser.Signal();
  }

  initAnimation (){
    return this.show().then(()=>{
      this.close();
    });
  }

  show (){
    var showPromise = new Promise((resolve)=>{
      this.game.add.tween(this.shadowSprite).to({ alpha : 1}, 100, Phaser.Easing.Linear.None, true);
      var tween =this.game.add.tween(this).to({ alpha : 1}, 100, Phaser.Easing.Linear.None, true);
      // var tween = this.game.add.tween(this.scale).to({x:1, y: 1}, 200, Phaser.Easing.Linear.None, true);

      tween.onComplete.add(()=>{
        this.game.add.tween(this.closeBtn).to({ alpha : 1}, 100, Phaser.Easing.Linear.None, true);
        this.closeBtn.visible = true;

        this.game.add.tween(this.gameRulesBtn).to({ alpha : 1}, 100, Phaser.Easing.Linear.None, true);
        this.gameRulesBtn.visible = true;

        resolve();
      });
    });

    return showPromise;
  }

  close (){
    var closePromise = new Promise((resolve)=>{
      this.game.add.tween(this).to({ alpha : 0}, 100, Phaser.Easing.Linear.None, true);

      let tween = this.game.add.tween(this.closeBtn).to({ alpha : 0.0}, 100, Phaser.Easing.Linear.None, true);
      this.closeBtn.visible = false;

      this.game.add.tween(this.gameRulesBtn).to({ alpha : 0.0}, 100, Phaser.Easing.Linear.None, true);
      this.gameRulesBtn.visible = false;

      tween.onComplete.add(()=>{
        this.game.paused = false;
        resolve();
      });
    });

    return closePromise;
  }

  onGameRulesBtnClicked (){
    this.game.time.events.add(300,
      () => {
        let gameRulesUrl = this.game.api.config.helpURL;

        if (this.game.net.medium == 'ma') {
          window.location.href = this.game.net.LobbyUrl + '?url=' + encodeURIComponent(gameRulesUrl);
        }
        else {
          window.open(gameRulesUrl);
        }

        this.gameRulesBtn.disable();
        this.gameRulesBtn.enable();
      }, this);
  }

  onCloseHelpBtnClicked (){
    this.closeBtn.disable();
    this.closeBtn.enable();
    this.events.onCloseHelpBtnClickedEvent.dispatch(this);
  }

  clone (obj){
    var clone = {};
    clone.prototype = obj.prototype;
    for (var property in obj){
      clone[property] = obj[property];
    }
    return clone;
  }

}


export default HelpPanel;
