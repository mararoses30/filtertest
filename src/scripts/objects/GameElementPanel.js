/*
 * GameElementPanel
 * ============================================================================
 *
 * Game elements generator
 */

import GameElement from './GameElement';

class GameElementPanel extends Phaser.Group {

  constructor (game, parent) {
    super(game, parent, 'GameElementPanel', false);

    //TODO: Get current minimum matches to hghlight
    // this should be provided from config file
    this.MIN_MATCHES = 3;

    // variable for holding the number of matched items revealed
    this.numWinItemsrevealed = 0;

    //add signal to Panel to be fired when all items have been revealed
    this.events = {};
    this.events.onGameItemsRevealed = new Phaser.Signal();
    // signal to fire up when a winner items is revealed
    this.events.highlightPaytable = new Phaser.Signal();

    //the positioning of elements x,y. We have to multiply with SCALE_FACTOR. We make use of map for the transformation
    this.elementsPos = this.game.cache.getJSON('layout')['layout']['gameItems'];

    // On reveal promise  null at initialization
    this.onRevealPromise = null;

    //when initializing there is no symbols to render
    this.symbols = null;

    this.gameElementsGroup = new Phaser.Group(game, null, 'gameElementsGroup', true);
    this.add(this.gameElementsGroup);

    this.createGameElements();
  }

  _reset (){
    this.symbols = null;
    //destroy previous GameElements

    this._destroyGameElements();

    this.numWinItemsrevealed = 0;
  }

  _destroyGameElements (){
    this.gameElementsGroup.removeChildren();
  }

  createGameElements (symbols){
    //reset the pyramid
    var atlaskey = 'atlas';
    this._reset();
    // If no symbol is provided the Panel is initialized without symbols so
    // we create a set of pseudosymbols
    if (!symbols || symbols === [] || (symbols && (symbols.length != this.elementsPos.length))){
      symbols = Array(this.elementsPos.length).fill().map(() => {return {frame:'Symbol_0001', isWinner:false, isRevealed: false};});
    }
    this.elementsPos.forEach((el, index)=> {
      let item = new GameElement(this.game,
                                this,
                                index,
                                el.x,
                                el.y,
                                'animation_cross',
                                atlaskey,
                                symbols[index]
                              );

      // add index of parent group to elementsPos in order to have a full position description for each gameElement
      this.elementsPos[index]['groupIndex'] = index;
      this.elementsPos[index]['isRevealed'] = symbols[index].isRevealed;
      this.elementsPos[index]['isWinner'] = symbols[index].isWinner;
      this.gameElementsGroup.addAt(item, index, true);
      item.events.onItemRevealed.addOnce(this._itemReavealed, [this, item]);
    });

    this.updateZ();
  }

  _itemReavealed (){
    // if is winner add to revealed items
    if(this[1].isWinner) {
      this[0].numWinItemsrevealed += 1;
    }
    if(this[1].isWinner && (this[0].numWinItemsrevealed >= this[0].MIN_MATCHES)){
      this[0].events.highlightPaytable.dispatch(this[0]);
    }

    if (this[0].unreavealedItems().length === 0){
      this[0].events.onGameItemsRevealed.dispatch(this[0]);
    }
  }

  reveal (){
    this.onRevealPromise = new Promise((resolve)=>{
      this.events.onGameItemsRevealed.add(()=>{resolve('everything fine');});
    });

    //repeat event for revealing item every x second
    this.game.time.events.repeat(Phaser.Timer.SECOND * 0.2, this.elementsPos.length, this._revealItem, this);

    return this.onRevealPromise;
  }

  _revealItem (){
    let item = this.game.rnd.pick(this.unreavealedItems()); // filters elementsPos for isRevealed === false entries and pick one of those
    if (item){
      //remove it from elementsPos
      this.gameElementsGroup.children[item['groupIndex']].reveal();
      item['isRevealed'] = true;
    }
  }

  unreavealedItems (){
    return  this.elementsPos.filter((el)=>{
      return el['isRevealed'] === false;
    });
  }

}


export default GameElementPanel;
