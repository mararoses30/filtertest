/*
 * SplashScreen
 * ============================================================================
 *
 * Shows a busy, decorated image, containing a widget displaying the resource
 * loading progress rate.
 */


class SplashScreen extends Phaser.Sprite {

  constructor (game) {
    super(game, 0, 0, 'splash');
  }

}


export default SplashScreen;
