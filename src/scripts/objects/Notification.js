/*
 * Notification
 * ============================================================================
 *
 * Network Error, Win Lose notification system
 */

class Notification extends Phaser.Group {
  constructor (game, parent) {
    super(game, parent, 'Notification', false);
    //initial state
    this.is_open = false;

    //intial scale
    this.scale.setTo(0.1, 0.1);
    //initial alpha
    this.alpha = 0;

    // Add close button event that will propagete to the game
    this.events = [];
    this.events.onCloseNotificationEvent = new Phaser.Signal();

    var bmd_width = 0.8* game.SAFE_AREA_WIDTH;
    var bmd_height = 0.5 * parent.height;
    var bmd = this._roundRect(bmd_width, bmd_height, 20 * game.SCALE_FACTOR, '0x000', 0.8);

    console.log(bmd_width);
    this.x = (parent.width )/2;
    this.y = (parent.height)/2 - (100 * game.SCALE_FACTOR);


    this.bg = new Phaser.Sprite(
                  game,
                  0,
                  0,
                  bmd);
    this.add(this.bg);

    this.bg.anchor.x = 0.5;
    this.bg.anchor.y = 0.5;
    // Enable input on bg and Add signal on bg clicked
    this.bg.inputEnabled = true;
    this.bg.events.onInputDown .add(this.close, this);


    this.bg.inputEnabled = false;
    this.bg.events.onInputDown.add(this.closeNotification, this);

    // Different Animation for win situation
    this.winBg = new Phaser.Group(game, this, 'WinGroup', true);
    parent.add(this.winBg);


    this.plate = new Phaser.Sprite(game, 0, 0, 'atlas', 'Messages_plate');
    this.plate.inputEnabled = false;
    this.plate.events.onInputDown.add(this.closeNotification, this);
    this.plate.x = (parent.width)/2;
    // this.plate.y = -this.plate.height;
    this.plate.anchor.x = 0.5;
    this.winBg.add(this.plate);


    // Add dust animation
    this.dustAnimation = new Phaser.Sprite(game, 0, 0, 'dust_anim');
    this.dustAnimation.y = -this.dustAnimation.height;
    this.dustAnimation.animations.add('dust', null, 20);

    //Add Textgroup
    this.textGroup = new Phaser.Group(game, this, 'textGroup', true);
    this.add(this.textGroup);


    parent.add(this.dustAnimation);

    this.winBg.y = -this.plate.height;
    this.isWinner = false;

    window.not = this;
    this.initMessages();
  }

 

  initMessages (){
    // lose message
    var fontsize = 90 * this.game.SCALE_FACTOR;
    var style = { font: fontsize + 'px Arial',
                fill: '#D0A800',
                align: 'center', // the alignment of the text is independent of the bounds, try changing to 'center' or 'right'
                boundsAlignH: 'center',
                wordWrap: true,
                wordWrapWidth: this.bg.width
              };

    this.winText = new Phaser.Text(
                                this.game,
                                1280 * this.game.SCALE_FACTOR,
                                290 * this.game.SCALE_FACTOR,
                                this.game.locale.get('g_notification_win_text'),
                                style
                              );
    this.winText.anchor.x = 0.5;
    this.winText.anchor.y = 0.5;
    this.winBg.add(this.winText);



    fontsize = 90 * this.game.SCALE_FACTOR;
    style = { font: fontsize + 'px Arial',
                  fill: '#D0A800',
                  align: 'center', // the alignment of the text is independent of the bounds, try changing to 'center' or 'right'
                  boundsAlignH: 'center',
                  wordWrap: true,
                  wordWrapWidth: this.bg.width * 0.8 // if 1 overlaps borders
                 };

    this.price_text = new Phaser.Text(this.game, 1280 * this.game.SCALE_FACTOR, 630 * this.game.SCALE_FACTOR, '', style);
    this.price_text.anchor.x = 0.5;
    this.price_text.anchor.y = 1.7;
    this.winBg.add(this.price_text);

    // lose message
    var fontsize = 90 * this.game.SCALE_FACTOR;
    var style = { font: fontsize + 'px Arial',
                fill: '#fff',
                align: 'center', // the alignment of the text is independent of the bounds, try changing to 'center' or 'right'
                boundsAlignH: 'center',
                wordWrap: true,
                wordWrapWidth: this.bg.width
              };

    this.lose_text = new Phaser.Text(
                                this.game,
                                0,
                                0,
                                this.game.locale.get('g_notification_lose'),
                                style
                              );

    this.lose_text.anchor.x = 0.5;
    this.lose_text.anchor.y = 1;
    this.textGroup.add(this.lose_text);

    // error message
    fontsize = 60 * this.game.SCALE_FACTOR;
    style = { font: fontsize + 'px Arial',
                  fill: '#fff',
                  align: 'center', // the alignment of the text is independent of the bounds, try changing to 'center' or 'right'
                  boundsAlignH: 'center',
                  wordWrap: true,
                  wordWrapWidth: this.bg.width * 0.95
                 };
    console.log(this.bg.width);
    this.error_text = new Phaser.Text(this.game, this.bg.x, this.bg.y + 50 * this.game.SCALE_FACTOR, this.game.locale.get('g_error_message'), this.clone(style));
    this.error_text.anchor.x = 0.5;
    this.error_text.anchor.y = 0.7;
    this.textGroup.add(this.error_text);

    // no bet Text
    fontsize = 70 * this.game.SCALE_FACTOR;
    let noBalanceStyle = { font: fontsize + 'px Arial', fill: '#fff',
                  align: 'center', // the alignment of the text is independent of the bounds, try changing to 'center' or 'right'
                  boundsAlignH: 'center',
                  wordWrap: true,
                  wordWrapWidth: this.bg.width
                 };

    this.noBalance_text = new Phaser.Text(this.game, this.bg.x, this.bg.y + 50 * this.game.SCALE_FACTOR, this.game.locale.get('g_notification_no_balance'), noBalanceStyle);
    this.noBalance_text.anchor.x = 0.5;
    this.noBalance_text.anchor.y = 1;
    this.textGroup.add(this.noBalance_text);



    // make all texts invisible
    this.textGroup.children.forEach((text)=>{
      text.visible = false;
    });
    this.winBg.visible = false;
  }

  show (message, value, error){
    var showPromise = new Promise((resolve)=>{
      if(!this.is_open){
        //enable background input
        console.log(message);
        this.bg.inputEnabled = true;
        switch (message) {
        case 'win':
          this.isWinner = true;
          this.winText.visible = true;
          this.winBg.visible = true;

          this.price_text.setText(this.game.locale.formatCurrency(value, this.game.api.config.currencyPrecision));
          this.price_text.visible = true;
          break;
        case 'lose':

          this.lose_text.visible = true;
          break;
        case 'error':
          this.error_text.visible = true;
          break;
        case 'noBalance':
          this.noBalance_text.visible = true;
          break;
        case 'rgsError':
          this.error_text.visible = true;
        }

        if(message != 'win' ){
          var tween = this.game.add.tween(this).to({ alpha : 1}, 200, Phaser.Easing.Linear.None, true);
          this.game.add.tween(this.scale).to({x:1, y: 1}, 200, Phaser.Easing.Linear.None, true);

          tween.onComplete.add(()=>{
            resolve();
          });
        }
        else{ //is winner
          this.isWinner = true;
          this.winTween = this.game.add.tween(this.winBg).to({ y : 70 * this.game.SCALE_FACTOR}, 500, Phaser.Easing.Quintic.In, true);
          this.winTween.onComplete.add(()=>{
            this.dustAnimation.visible = true;
            this.dustAnimation.play('dust');
            this.dustAnimation.events.onAnimationComplete.add(()=>{
              this.plate.inputEnabled = true;
              this.dustAnimation.visible = false;
              resolve();
            });
          });

        }
        this.is_open = true;
      }
    });

    return showPromise;
  }

  closeNotification (){
    this.events.onCloseNotificationEvent.dispatch(this);
  }

  close (){
    this.winBg.visible = false;


    this.bg.inputEnabled = false;
    this.plate.inputEnabled = false;
    this.is_open = false;

    var closePromise = new Promise((resolve)=>{
      if(!this.isWinner){
        this.game.add.tween(this).to({ alpha : 0}, 200, Phaser.Easing.Linear.None, true);
        let tween = this.game.add.tween(this.scale).to({x:0.1, y: 0.1}, 200, Phaser.Easing.Linear.None, true);

        tween.onComplete.add(()=>{
          resolve();
          this.winBg.visible = false;
          this.textGroup.children.forEach((text)=>{
            text.visible = false;
          });
        });
      }
      else{
        this.winBg.y = -this.plate.height;
        resolve();
      }

      this.isWinner = false;
    });
    return closePromise;
  }

  _roundRect (width, height, radius, fill, alpha) {
    var bmd = new Phaser.BitmapData(this.game, 'roundedbg', width, height);
    var x = 0;
    var y = 0;

    if (typeof radius === 'undefined') {
      radius = 5;
    }

    bmd.ctx.beginPath();
    bmd.ctx.fillStyle = 'rgba(0, 0, 0,' + alpha + ')';
    bmd.ctx.moveTo(x + radius, y);
    bmd.ctx.lineTo(x + width - radius, y);
    bmd.ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
    bmd.ctx.lineTo(x + width, y + height - radius);
    bmd.ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
    bmd.ctx.lineTo(x + radius, y + height);
    bmd.ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
    bmd.ctx.lineTo(x, y + radius);
    bmd.ctx.quadraticCurveTo(x, y, x + radius, y);
    bmd.ctx.closePath();
    bmd.ctx.fill();
    return bmd;
  }

  clone (obj){
    var clone = {};
    clone.prototype = obj.prototype;
    for (var property in obj){
      clone[property] = obj[property];
    }
    return clone;
  }
}


export default Notification;
