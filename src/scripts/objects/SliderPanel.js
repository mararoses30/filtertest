/*
 * Slider
 * ============================================================================
 *
 * Slider for choosing your favorite symbol
 */

import {TAtlasButton} from '../lib/buttons';
import Slider from './slider';

class SliderPanel extends Phaser.Group {

  constructor (game, parent) {
    var atlaskey = 'atlas';
    super(game, parent, 'Slider', false);

    this.symbolRightArrowBtn = new TAtlasButton(this.game, 0, 0, atlaskey, null, this,  'SLIDE', 'RLarrow_Over', 'RLarrow_Normal', 'RLarrow_Off');

    this.symbolRightArrowBtn.name = 'SliderRightButton';
    this.symbolLeftArrowBtn = new TAtlasButton(this.game, 0, 0, atlaskey, null, this,  'SLIDE', 'Larrow_Over', 'Larrow_Normal', 'Larrow_Off');
    this.symbolLeftArrowBtn.name = 'SliderLeftButton';

    var block1 = new Phaser.Sprite(this.game, 0, 0, atlaskey, 'White_0000');
    block1.scale.setTo(1.3);
    var block2 = new Phaser.Sprite(this.game, 0, 0, atlaskey, 'White_0001');
    block2.scale.setTo(1.3);
    var block3 = new Phaser.Sprite(this.game, 0, 0, atlaskey, 'White_0002');
    block3.scale.setTo(1.3);
    var block4 = new Phaser.Sprite(this.game, 0, 0, atlaskey, 'White_0003');
    block4.scale.setTo(1.3);
    var block5 = new Phaser.Sprite(this.game, 0, 0, atlaskey, 'White_0004');
    block5.scale.setTo(1.3);
    var block6 = new Phaser.Sprite(this.game, 0, 0, atlaskey, 'White_0005');
    block6.scale.setTo(1.3);
    var block7 = new Phaser.Sprite(this.game, 0, 0, atlaskey, 'White_0006');
    block7.scale.setTo(1.3);
    var block8 = new Phaser.Sprite(this.game, 0, 0, atlaskey, 'White_0007');
    block8.scale.setTo(1.3);
    var block9 = new Phaser.Sprite(this.game, 0, 0, atlaskey, 'White_0000');
    block9.scale.setTo(1.3);

    var options = {
      'atlas': atlaskey,
      sliderBG: 'Slider_bg',
      handlePrev: this.symbolLeftArrowBtn,
      handleNext: this.symbolRightArrowBtn,
      animationEffect: 'slide', // slide, fade, cover
      animationDelay: 2,
      animationDuration: 100,
      animationEasing: Phaser.Easing.Cubic.Out,
      objects:[block1, block2, block3, block4, block5, block6, block7, block8, block9]
    };

    this.slider = new Slider(this.game, parent, options);
    this.add(this.slider);
    this.add(this.symbolRightArrowBtn);
    this.add(this.symbolLeftArrowBtn);

    // map of RGS SymbolIds to local references
    var symbol_local_references = ['White_0000',
                                  'White_0001',
                                  'White_0002',
                                  'White_0003',
                                  'White_0004',
                                  'White_0005',
                                  'White_0006',
                                  'White_0007'
                                ];
    this.symbolMap = {};
    game.api.config.symbols.forEach((symbolId, idx) => {
      this.symbolMap[symbol_local_references[idx]] = symbolId;
    });

  }

  disable (){
    this.symbolLeftArrowBtn.frameName = 'Larrow_Off';
    this.symbolRightArrowBtn.frameName = 'RLarrow_Off';
    this.symbolLeftArrowBtn.inputEnabled = false;
    this.symbolRightArrowBtn.inputEnabled = false;
  }

  enable (){
    this.symbolLeftArrowBtn.frameName = 'Larrow_Normal';
    this.symbolRightArrowBtn.frameName = 'RLarrow_Normal';
    this.symbolLeftArrowBtn.inputEnabled = true;
    this.symbolRightArrowBtn.inputEnabled = true;
  }

  get SymbolID (){
    return Object.keys((this.symbolMap[this.slider.getCurrentSymbolsFrameName()]))[0];
  }

}

export default SliderPanel;
