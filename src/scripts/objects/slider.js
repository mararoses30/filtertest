
export default class Slider extends Phaser.Group{
  constructor (game, parent, options) {
    super(game, parent, 'Slider', false);
    this.slider_timer = null;
    this.tweenObj = {};

    // Initialize index
    this.slideIndex = 0;

    this.options = options;

    // Add background box
    this.selectionBG = new Phaser.Sprite(this.game, 0, 0, 'atlas', 'box');
    parent.add(this.selectionBG);
    this.bgBounds = {};
    this.bgBounds.y = this.selectionBG._frame.spriteSourceSizeY;
    this.bgBounds.w = this.selectionBG._frame.spriteSourceSizeW;
    this.bgBounds.h = this.selectionBG._frame.spriteSourceSizeH;
    this.bgBounds.x = this.selectionBG._frame.spriteSourceSizeX;
    /*
      Objects Group
     */
    this.sliderMainGroup = new Phaser.Group(game, parent, 'sliderMainGroup', false);

    // this.betButton._frame.spriteSourceSizeY
    this.sliderMainGroup.y = this.bgBounds.y;
    this.sliderMainGroup.x = this.bgBounds.x;

    //load mask's dimensions and coordinates
    var maskBound = this.game.cache.getJSON('layout')['layout']['sliderMask'][0];

    this.maskGraphics = this.game.add.graphics(0, 0);
    this.maskGraphics.beginFill(0x000);
    this.maskGraphics.drawRect(maskBound.x, maskBound.y, maskBound.w, maskBound.h);
    this.add(this.maskGraphics);

    var sprite = new Phaser.Sprite(this.game, 0, 0);
    sprite.addChild(this.maskGraphics);
    sprite.visible = true;
    this.add(sprite);

    this.sliderMainGroup.mask = this.maskGraphics;

    // add controls
    var buttonRight = this.options.handleNext;
    var buttonLeft = this.options.handlePrev;


    buttonRight.inputEnabled = true;
    buttonRight.events.onInputDown.add(() => {

      if (this.tweenObj.isRunning !== true) {
        this.stopSlider();
        this.goToNext();
      }

    });

    buttonLeft.inputEnabled = true;
    buttonLeft.events.onInputDown.add(() => {

      if (this.tweenObj.isRunning !== true) {
        this.stopSlider();
        this.goToPrev();
      }
    });

    // ADDING THE BLOCKS
    if (this.options.objects.length > 0) {
      let objArr = this.options.objects.slice(0);
      let length = Number(objArr.length);
      for (let i = 0; i < length; i++) {

        objArr[i].x = ( (this.bgBounds.w - this.options.objects[0].width) / 2) + (this.bgBounds.w * i);
        objArr[i].y = (this.bgBounds.h - this.options.objects[0].height) /2;

        if(i>0){
          objArr[i].visible = false;
        }
        this.sliderMainGroup.addAt(objArr[i], i);
      }

    }
  }

  goToNext (){
    var indexToGoInvisible = this.slideIndex;
    if (this.slideIndex >= this.options.objects.length - 1) {
      this.sliderMainGroup.x = this.bgBounds.x;
      this.slideIndex = 0;
    }
    this.slideIndex = (this.slideIndex + 1) % (this.options.objects.length );

    this.moveToSlide(this.slideIndex, indexToGoInvisible, true);
  }

  goToPrev (){
    var indexToGoInvisible = this.slideIndex;
    if (this.slideIndex <= 0) {
      //move last symbol to be the first
      this.sliderMainGroup.x -= this.bgBounds.w * (this.options.objects.length -1);
      this.slideIndex = this.options.objects.length -2;
    }
    else{
      this.slideIndex -= 1;
    }

    this.moveToSlide(this.slideIndex, indexToGoInvisible, true);
  }

  startSlider (){
    var _timer = this.game.time.create(false);
    _timer.start();
    _timer.loop(Phaser.Timer.SECOND * this.options.sliderBG, this.goToNext, this);
    this.slider_timer = _timer;
  }

  stopSlider (){
    if (this.slider_timer !== null) {
      this.slider_timer.stop(true);
      this.slider_timer = null;
    } else {
      return false;
    }
  }

  moveToSlide (index, previousIndex, animate){
    var finalX;
    if (index >= this.options.objects.length) {
      return false;
    }
    finalX = (this.bgBounds.x - (this.bgBounds.w * (index)));
    this.sliderMainGroup.children[index].visible = true;
    if (animate === true) {
      this.tweenObj = this.game.add.tween(this.sliderMainGroup).to(
        { x: finalX },
        this.options.animationDuration,
        this.options.animationEasing,
        true,
        0,
        0,
        false);
      this.tweenObj.onComplete.add(()=>{
        //make the symbol out of the box invisible so as not to increase the size of the group
        this.sliderMainGroup.children[previousIndex].visible = false;
      });
    } else {
      this.sliderMainGroup.x = finalX;
    }
  }

  getCurrentSymbolsFrameName () {
    //get modulo of index
    return this.sliderMainGroup.children[this.slideIndex % (this.options.objects.length - 1)].frameName;
  }
}
