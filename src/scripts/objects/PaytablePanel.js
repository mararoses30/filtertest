/*
 * PaytablePanel
 * ============================================================================
 *
 * Paytable Constructor
 */


class PaytablePanel extends Phaser.Group {

  constructor (game, parent) {
    super(game, parent, 'PaytablePanel', false);
    // the atls key where we find the paytable images
    var atlaskey = 'atlas';
    //retrieve positions from layout json file
    this.winningsPos = this.game.cache.getJSON('layout')['layout']['paytableItems'];
    this.winningsTextPos = this.game.cache.getJSON('layout')['layout']['paytableTextItems'];

    // //add Background image
    this.BgLayer = new Phaser.Group(game, parent, 'BgLayer', false);
    this.add(this.BgLayer);
  //  this.bg = new Phaser.Sprite(this.game, 0, 0, atlaskey, 'papyrus_paytable');
  //  this.BgLayer.add(this.bg);

    // The green marken will be in this group
    this.MarkingLayer = new Phaser.Group(game, parent, 'MarkingLayer', false);
    this.add(this.MarkingLayer);
    // add highlight bar

   // this.highlightBar = new Phaser.Sprite(this.game, 0, this.winningsPos[0].y, atlaskey, 'Green_line');
   // this.highlightBar.x = this.bg._frame.spriteSourceSizeX + (this.bg._frame.spriteSourceSizeW - this.highlightBar.width) * 0.5 - 5 * this.game.SCALE_FACTOR;

 //   this.highlightBar.visible = true;
  //  this.MarkingLayer.add(this.highlightBar);

    this.TextLayer = new Phaser.Group(game, parent, 'TextLayer', false);
    this.add(this.TextLayer);

    // Add fixed text
    this.fontSize = 40;
    this.winningsTextPos.forEach((pos, idx)=>{
      let fontsize = this.fontSize * game.SCALE_FACTOR;
      var style = {font: fontsize + 'px Roboto',
                      fill: '#000',
                      align: 'left', // the alignment of the text is independent of the bounds, try changing to 'center' or 'right'
                      wordWrap: true,
                      stroke : 'black',

                      wordWrapWidth: this.bg.width * this.game.SCALE_FACTOR
                     };

      var win_text = new Phaser.Text(
                      this.game,
                      pos.x,
                      pos.y,
                      parseInt(idx + 3) + '' + 'x',
                      style);
      this.TextLayer.add(win_text);
    });

    // All text and winnings will be in this group
    this.WinningsLayer = new Phaser.Group(game, parent, 'PaytableWinningsLayer', false);
    this.add(this.WinningsLayer);

    // All text and winnings will be in this group
    this.winGroup = new Phaser.Group(game, parent, 'PaytableWinningsLayer', false);
    this.add(this.winGroup);

    //initial stake for getting the corresponding paytable
    this.stake =  this.game.api.config.availableBets[0];
    console.log(this.game.api.config.availableBets);
    // Highlight color Fill
    this.FILL_COLOR = '#c8a539';
    // we map the matches to the position of paytable matches
    this.match_paytable = {
      3: this.winningsPos[0],
      4: this.winningsPos[1],
      5: this.winningsPos[2],
      6: this.winningsPos[3],
      7: this.winningsPos[4],
      8: this.winningsPos[5]
    };

    this.winGroupIdx = 0;
    // this.switchToTier(this.stake);
    this.create();
    this.switchToTier(this.stake);

    window.pay = this;

  }

  create (){
    // 6 bets
    this.game.api.config.availableBets.forEach((stake)=>{
      var matchesGroup = new Phaser.Group(this.game, null, 'MatchesBet'+stake, false);

      //paytable has the form of :
      //{3: "1", 4: "2", 5: "10", 6: "50", 7: "100", 8: "5000", PayTableID: "00000001-0000-0000-0001-000000000000"}
      var paytable = this.game.api.config.paytable(stake);
      var sorted_keys = Object.keys(paytable).sort();
      sorted_keys.forEach((matches, index) =>{
        let fontsize = this.fontSize * this.game.SCALE_FACTOR;
        var style = { font: fontsize + 'px Roboto',
                      fill: '#000',
                      align: 'center', // the alignment of the text is independent of the bounds, try changing to 'center' or 'right'
                      // boundsAlignH: 'center',
                      wordWrap: true,
                      stroke : 'black',
                      wordWrapWidth: this.bg.width * this.game.SCALE_FACTOR
                     };
        let text = new Phaser.Text(this.game,
                                    this.winningsPos[index].x,
                                    this.winningsPos[index].y,
                                    this.game.locale.formatCurrency(paytable[matches], this.game.api.config.currencyPrecision),
                                    style);
        text.anchor.x =1;
        text.align = 'right';
        matchesGroup.alpha = 0;
        matchesGroup.add(text);
      });

      this.winGroup.add(matchesGroup);
    });
  }

  switchToTier (stake){
    // this.cacheAsBitmap = false;
    this.currStakeIdx = null;

    this.currStakeIdx = this.game.api.config.availableBets.indexOf(stake);
    this.stake = this.game.api.config.availableBets[this.currStakeIdx];

    this.winGroup.getChildAt(this.winGroupIdx).alpha = 0;
    this.winGroup.getChildAt(this.currStakeIdx).alpha = 1;

    this.winGroupIdx = this.currStakeIdx;
    this._reset();
  }

  highlight (matches){
    this.highlightBar.visible = true;

    this.removeTextHighlight();
    if(this.currStakeIdx != null && matches !=3){
      this.winGroup.getChildAt(this.currStakeIdx).children[matches-3 -1].addColor('#000', 0);
      this.TextLayer.getChildAt((matches -3) - 1).addColor('#000', 0);
    }

    var tween = this.game.add.tween(this.highlightBar).to({ y: this.match_paytable[matches].y  + 2 * this.game.SCALE_FACTOR}, 50, 'Linear', false, 0);

    //change the text's color
    this.winGroup.getChildAt(this.currStakeIdx).children[matches-3].addColor(this.FILL_COLOR, 0);
    this.TextLayer.getChildAt(matches -3).addColor(this.FILL_COLOR, 0);

    tween.start();

  }

  highlightReset (){
    this.highlight(3);
    this.highlightBar.visible = false;
  }

  get paytableId (){
    console.log('-->Stake: ', this.stake);
    console.log('--> ', this.game.api.config.paytableID(this.stake * this.game.api.config.currencyDivisor));
    return this.game.api.config.paytableID(this.stake * this.game.api.config.currencyDivisor);
  }

  removeTextHighlight (){
    this.winGroup.children.forEach((cl)=>{
      cl.children.forEach((text)=>{
        text.addColor('#000000', 0);
      });
    });
    this.TextLayer.children.forEach((cl)=>{
      cl.addColor('#000000', 0);
    });
  }

  _reset (){
    this.highlightBar.visible = false;
    this.removeTextHighlight();
  }
}

export default PaytablePanel;
