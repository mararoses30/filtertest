/*
 * GameElement
 * ============================================================================
 *
 * Item to be placed inside GameElementsPanel
 */


class GameElement extends Phaser.Group {
  /*
    groupindex: number . The index stored to it's parent group
   */
  constructor (game, group, groupIndex, x, y, coverSprite, atlasKey, symbol) {
    super(game, group, 'GameElement', true);

    // the index that can be found in parent's group children
    this.groupIndex = groupIndex;

    this.cover = new Phaser.Sprite(game, x, y, coverSprite);
    // is winner
    this.isWinner = symbol.isWinner;
    this.add(this.cover);
    //store x, y position values
    this.cover.x = x;
    this.cover.y = y;

    if (!symbol.isRevealed){
      //initial frame
      this.cover.frame = 0;
      // add revealing animation
      this.cover.animations.add('reveal', null, 30);
    }
    else{
      this.cover.visible = false;
    }

    // create symbol
    // if is not winner we tint the white sprite otherwise we
    // use Symbol_win_xxxx aymbol already provided
    this.symbol  = new Phaser.Sprite(game, x, y, atlasKey, symbol.frame);
    this.symbol.x = x;
    this.symbol.y = y;
    if(symbol.isRevealed){
      this.symbol.visible = true;
    }
    else{
      this.symbol.visible = false;
    }
    this.add(this.symbol);

    // Add signal for notifying Panel that revealing is over
    this.events = [];
    this.events.onItemRevealed = new Phaser.Signal();
  }

  reveal (){
    this.game.fx.play('SPIN');
    this.cover.play('reveal');
    this.cover.events.onAnimationComplete.add(this._makesymbolVisible, this);
  }

  _makesymbolVisible (){
    if(this.isWinner){
      this.game.fx.play('MATCH');
    }
    this.cover.visible = false;
    this.symbol.visible = true;
    this.events.onItemRevealed.dispatch(this);
  }

}

export default GameElement;
