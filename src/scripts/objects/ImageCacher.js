/*
 * ImageCacher
 * ============================================================================
 *
 * A class to assist with image asset loading and GPU caching(indirectly)
 */


class ImageCacher extends Phaser.Group
{
  constructor (game, parent, imageArray) {
    super (game, parent, 'ImageCacher', false);
    this.counter = 0;
    this.alpha = 0.001;
    this.scale.setTo(0.01, 0.01);
    this.playSpriteCoordX = window.innerWidth/2;
    this.playSpriteCoordY = 0;


    this.fps = 120;

    this.creationDelayMs = 150;

    this.imagesInCache = imageArray;
  }

  performCache () {
    let initCaching = new Promise( (resolve)=>{


      if (this.imagesInCache == null ||  this.imagesInCache.length == 0) {
        console.log('ImageCacher: No cacheables found, Proceeding normally');
        resolve();
      }

      this.spriteArray = [];

      this.resolve = resolve;


      let time = 0;
      this.imagesInCache.forEach( (texture, index, allImagesArray)=>{

        this.game.time.events.add(time, this.addCacheSprite, this, texture);

        time += this.creationDelayMs;
        // LastSprite
        if (index == allImagesArray.length - 1)
        {
          // Add 300 ms to the last sprite creation just to be sure
          // it has been created.
          this.game.time.events.add(time + 300, ()=>{
            this.spriteArray[0].play('play');
          }, this);
        }
      });
    });

    return initCaching;
  }

  addCacheSprite (texture) {
    var aniSprite = new Phaser.Sprite(this.game, this.playSpriteCoordX, this.playSpriteCoordY, texture);

    this.spriteArray.push(aniSprite);

    this.add(aniSprite);

    let anim = aniSprite.animations.add('play', null, this.fps);
    anim.onComplete.add((/*aniSprite*/)=>{
      this.playNext();
    });
    // this.resolve = resolve;
  }

  playNext () {
    this.counter += 1;
    if( this.counter < this.spriteArray.length ) {
      this.spriteArray[this.counter-1].destroy();
      /*let currAnim = */this.spriteArray[this.counter].play('play');
    }
    else if (this.counter == this.spriteArray.length) {

      this.spriteArray.forEach( (leftSprite)=>{
        leftSprite.destroy();
      });
      this.spriteArray = [];
      this.resolve();
    }
  }

}

export default ImageCacher;
