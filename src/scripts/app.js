/*
 * The `app` module
 * ============================================================================
 *
 * The module providing the main routine of the game application launch.
 */

// Import all declared states as an object.
import * as states from './states';


export default function () {
  var game;
  var config = {
    width: 2520,
    height: 1080,
    renderer: Phaser.AUTO,
    parent: 'Game',
    transparent: false,
    antialias: true,
    forceSetTimeOut: false
  };

  detectCanvas().then( (value)=>{
    // If device runs iOS (ipad /iphone) set renderer to CANVAS
    var net = getQueryString();

    if ((value == 'IOS' && net.medium != 'ma') || value == 'IE') {
      config.renderer = Phaser.CANVAS;
    }

    game =  new Phaser.Game(config);
    if (value == 'IE') {
      game.isIE = true;
    }
    else {
      game.isIE = false;
    }

    Object.keys(states).forEach((key) => game.state.add(key, states[key]));

    game.state.start('PreBoot');

    return game;
  });
}

  // Detect if the device is iphone or ipad
function detectCanvas () {
  var detectPromise = new Promise( (resolve)=>{
    let ua = window.navigator.userAgent;

    if (ua.match(/iPhone/i) || ua.match(/iPad/i) /*|| navigator.userAgent.match(/Android/i)*/) {
      resolve('IOS');
    }
    else if (ua.match(/MSIE/i) || ua.match(/Trident/i) || ua.match(/Edge/i)) {
      console.log('IE-->Switch to Canvas rendering!');
      resolve('IE');
    }
    else { resolve();}
  });
  return detectPromise;
}

function getQueryString (parameter) {
  if (parameter === undefined) { parameter = ''; }

  var output = {};
  var keyValues = location.search.substring(1).split('&');

  for (var i in keyValues){
    var key = keyValues[i].split('=');

    if (key.length > 1){
      if (parameter && parameter == decodeURI(key[0])){
        return decodeURI(key[1]);
      }
      else {
        output[decodeURI(key[0])] = decodeURI(key[1]);
      }
    }
  }
  return output;
}
