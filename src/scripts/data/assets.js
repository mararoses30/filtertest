/*
 * The `assets` module
 * ============================================================================
 *
 * Use this module to declare static Phaser Asset Packs, that would be loaded
 * using the `Loader#pack` API.
 *
 * Regarding how the game assets should be declared using this file, refer to
 * the sample `assetPack.json` included in the Phaser package, under
 * `node_modules/phaser/resources/` directory, for a more complete
 * reference.
 *
 */
var COMMON = {
  commonAssets: [
    {
      type: 'json',
      key: 'config',
      url: 'config.json'
    },
    {
      //   Loads a Bitmap Font File.
      'type': 'bitmapFont',
      'key': 'winFont',
      'textureURL': 'assets/winFont.png',
      'atlasURL': 'assets/winFont.xml',
      'atlasData': null,
      'xSpacing': 0,
      'ySpacing': 0
    },
    {
      type: 'json',
      key: 'sfx',
      url: './assets/sfx.json'
    }
  ]
};

var AMBIENT = [
  './assets/ambient.ogg', './assets/ambient.m4a', './assets/ambient.mp3'
];

var HDR = {
  // - Boot Assets ------------------------------------------------------------
  boot: [
    {
      key:'flare_anim',
      type:'atlasJSONHash',
      textureURL: 'flare_anim.png',
      atlasURL: 'flare_anim.json'
    },
    {
      key: 'splash',
      type: 'image',
      url: 'splash.jpg'
    }
  ],

  'boot_en-US': [
    {
      key: 'logo',
      type: 'atlasJSONHash',
      textureURL: 'en-US/logo.png',
      atlasURL: 'en-US/logo.json',
      isCached: true
    },
    {
      key: 'loading',
      type: 'image',
      url: 'en-US/loading.png',
      isCached: true
    },
  ],
  'boot_en-GB': [
    {
      key: 'logo',
      type: 'atlasJSONHash',
      textureURL: 'en-GB/logo.png',
      atlasURL: 'en-GB/logo.json',
      isCached: true
    },
    {
      key: 'loading',
      type: 'image',
      url: 'en-GB/loading.png',
      isCached: true
    },
  ],
  'boot_es-CL': [
    {
      key: 'logo',
      type: 'atlasJSONHash',
      textureURL: 'es-CL/logo.png',
      atlasURL: 'es-CL/logo.json',
      isCached: true
    },
    {
      key: 'loading',
      type: 'image',
      url: 'es-CL/loading.png',
      isCached: true
    },
  ],
  'boot_bs-BA': [
    {
      key: 'logo',
      type: 'atlasJSONHash',
      textureURL: 'bs-BA/logo.png',
      atlasURL: 'bs-BA/logo.json',
      isCached: true
    },
    {
      key: 'loading',
      type: 'image',
      url: 'bs-BA/loading.png',
      isCached: true
    },
  ],
  // - Game assets ------------------------------------------------------------
  game: [
    {
      key: 'help_atlas',
      type: 'atlasJSONHash',
      textureURL: 'help_atlas.png',
      atlasURL: 'help_atlas.json',
      isCached: true
    },
    {
      key: 'help',
      type: 'image',
      url: 'help.png',
      isCached: true
    },
    {
      key: 'menu_ui',
      type: 'atlasJSONHash',
      textureURL: 'menu_ui.png',
      atlasURL: 'menu_ui.json',
      isCached: true
    },
    {
      key: 'atlas',
      type: 'atlasJSONHash',
      textureURL: 'atlas.png',
      atlasURL: 'atlas.json',
      isCached: true
    },
    {
      key:'spinner_anim',
      type:'atlasJSONHash',
      textureURL: 'spinner_anim.png',
      atlasURL: 'spinner_anim.json'
    },
    {
      key:'layout',
      type:'json',
      url: 'layout.json'
    },
    {
      key:'animation_cross',
      type:'atlasJSONHash',
      textureURL: 'animation_cross.png',
      atlasURL: 'animation_cross.json',
      isCached: true
    },
    {
      key: 'dust_anim',
      type: 'atlasJSONHash',
      textureURL: 'dust_anim.png',
      atlasURL: 'dust_anim.json',
      isCached: true
    },
    {
      key: 'background',
      type: 'image',
      url: 'background.jpg'
    }
  ],
  'game_en-US': [
    {
      key: 'logo',
      type: 'atlasJSONHash',
      textureURL: 'en-US/logo.png',
      atlasURL: 'en-US/logo.json',
      isCached: true
    },
    {
      key: 'help',
      type: 'image',
      url: 'en-US/help.png',
      isCached: true
    },

  ],
  'game_en-GB': [
    {
      key: 'logo',
      type: 'atlasJSONHash',
      textureURL: 'en-GB/logo.png',
      atlasURL: 'en-GB/logo.json',
      isCached: true
    }
  ],
  'game_es-CL': [
    {
      key: 'logo',
      type: 'atlasJSONHash',
      textureURL: 'es-CL/logo.png',
      atlasURL: 'es-CL/logo.json',
      isCached: true
    }
  ],
  'game_bs-BA': [
    {
      key: 'logo',
      type: 'atlasJSONHash',
      textureURL: 'bs-BA/logo.png',
      atlasURL: 'bs-BA/logo.json',
      isCached: true
    }
  ],
};

var HD = HDR;

var SD = HDR;

var RESOLUTIONS = {
  'HDR':1,
  'HD': 0.666667,
  'SD': 0.333333
};
export {HDR, HD, SD, COMMON, AMBIENT, RESOLUTIONS};

