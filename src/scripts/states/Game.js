/*
 * Game state
 * ============================================================================
 *
 * A sample Game state, displaying the Phaser logo.
 */

import {TAtlasButton} from '../lib/buttons';
import GameElementPanel from '../objects/GameElementPanel';
import SliderPanel from '../objects/SliderPanel';
//import PaytablePanel from '../objects/PaytablePanel';
import BalancePanel from '../objects/BalancePanel';
import HelpPanel from '../objects/HelpPanel';
import Notification from '../objects/Notification';
import Ticket from '../api/Ticket';
import UpdateTicket from '../api/UpdateTicket';
import * as Machina from '../../../node_modules/machina/lib/machina';
import Locale from '../lib/locale';
import UIPlayButton from '../lib/UIPlayButton';
import UIAutoPlayPanel from '../lib/UIAutoPlayPanel';
import UIBetPanel from '../lib/UIBetPanel';
import ImageCacher from '../objects/ImageCacher';
import Spinner from '../lib/Spinner';
import Storage from '../lib/Storage';
import Sounds from '../lib/Sounds';
import RTClock from '../lib/RTClock';

export default class Game extends Phaser.State {

  init (preloadContainerGroup) {
    this.preloadContainerGroup = preloadContainerGroup;
    // Setup request retries for getting config file from server
    // after initial request (i.e. 1 + CONF_REQUEST = total config requests)
    this.CONF_REQUESTS = 4;
    this.filter;
    this.waveform;
    this.slices = [];
    this.xl;

   
    // Setup new Locale for game
    this.game.locale = new Locale(this.game, this.game.api.config.locale_settings);


    // Initialize an empty result
    this.game.api.ticket = new Ticket(this.game);
    //Initialize update ticket object
    this.game.api.ticketUpdate = new UpdateTicket(this.game);

    // create autoplay object attached to the game
    this.game.play = {};
    this.game.play.isAutoPlaySelected = false;
    this.game.play.isAutoPlayMode = false;
    this.game.play.isRequestedImmediatePlay = false; // Is used by Beteselect Panel and AutoplayPanel to trigger immediate play

    /*
    Create LocalStorage
     */
    this.game.storage = new Storage(this.game);
    this.game.isFirstVisit = true;

    this.CSSGameDiv = document.getElementById('Game');
    this.orientationPanel = document.getElementById('orientation');

    //change game's name respecting the locale id
    document.title  = this.game.locale.get('g_title');
  }

  create (){

    var atlaskey = 'atlas';
    /*
      Machina FSM for the game
    */
    let gameFSM = Machina.Fsm.extend({
      namespace: 'gameFsm',

      initialState: 'INITANIMATIONS',
      // The states object's top level properties are the
      // states in which the FSM can exist. Each state object
      // handled while in that state.
      states: {
        INIT:{
          _onEnter: ()=>{
            console.info('Game->INIT->_onEnter()');
            /*
              Configure game to go fullscreen when anywhere inside game is tapped
            */
            // Listen for postMessage
            window.addEventListener('message', this.handlePostMessage.bind(this));

            // Listen for window resize and run the callback
            window.removeEventListener('resize', this.game.resizeCallback);
            window.addEventListener('resize', this.gameResize.bind(this));

            //visibility listeners handlers
            this.game.stage.disableVisibilityChange = false;
            window.onfocus = this.focusin.bind(this);
            window.onblur = this.focusout.bind(this);

            if (!(this.game.net.medium == 'ds' || this.game.net.medium == 'ma')) {
              if(!this.game.device.desktop && this.game.device.requestFullscreen != ''){
                this.game.scale.startFullScreen(true);
                this.game.input.onTap.add(()=>{
                  if (!this.game.scale.isFullScreen){
                    this.game.scale.startFullScreen(true);
                    this.game.scale.setUserScale(window.screen.height/this.game.height, window.screen.height/this.game.height);
                  }

                }, this);
              }

              // fix for ios8/9 for iphone . Show slide up animation
              if(this.game.device.iPhone && (this.game.device.iOSVersion === 9 || this.game.device.iOSVersion === 8)){
                this.CSSscrollUpDiv = document.getElementsByClassName('scrollup')[0];
                this.CSSscrollUpImageDiv = document.getElementsByClassName('scrollup-image')[0];
              }
            }
            // Disable right Click
            this.game.canvas.oncontextmenu = (e)=>{
              e.preventDefault();
            };

            // Set the desired fps to be 30
            this.game.time.desiredFps = 30;

            /*
              Background color
            */
            this.game.stage.backgroundColor = 0x0D0906;
            /*
              Attach sound effects to the game object
            */


            this.game.fx = new Sounds(this.game);
            //----------------------------------------------

            /**
             * Create layers and fill with objects
            */
            // WARNING !!! All elements Sprites, Groups, etx should be attached to
            // gameContainerGroup  !!!
            this.gameContainerGroup = new Phaser.Group(this.game, null,  'GameContainer', true);
            this.game.add.existing(this.gameContainerGroup);

            // Add game elements

            this.bg = new Phaser.Sprite(this.game, 0, 0, 'background');
            this.gameContainerGroup.add(this.bg);


            this.testImage  = new Phaser.Sprite(this.game, 700, 50, 'atlas', 'test');
            this.gameContainerGroup.add(this.testImage);

            this.UIBackground = new Phaser.Sprite(this.game, 0, 0, 'menu_ui', 'MenuBar');
            this.gameContainerGroup.add(this.UIBackground);

     

            /****************************************
              Paytable
            *****************************************/
           // this.paytablePanel = new PaytablePanel(this.game, this.gameContainerGroup);

            /****************************************
              Notification
            *****************************************/
            this.notification = new Notification(this.game,
                                                 this.gameContainerGroup);
            this.notification.events.onCloseNotificationEvent.add(()=>{
              this.gamefsm.popupClicked();
            });

            // we make this group to hide UI elements behind it
            this.behindUIBarGroup = new Phaser.Group(this.game, this, 'behindUIBarGroup', true);
            this.gameContainerGroup.add(this.behindUIBarGroup);

            /****************************************
              AutoPLay Button/Panel
            *****************************************/
            /***
                Auto play conf
            ***/
            if(this.game.api.config.hasAutoPlay){
              this.UIAutoPlayPanel = new UIAutoPlayPanel(this.game, this.gameContainerGroup, this.game.api.config.availableAutoplays);
              this.UIAutoPlayPanel.events.onAutoPlayClicked.add(()=>{
                this.gamefsm.handle('autoPlayClicked');
              });
              this.UIAutoPlayPanel.events.onAutoPlayMinusClicked.add(()=>{
                this.gamefsm.handle('autoPlayMinusClicked');
              });
              this.UIAutoPlayPanel.events.onAutoPlayPlusClicked.add(()=>{
                this.gamefsm.handle('autoPlayPlusClicked');
              });
            }
            /****************************************
              Menu Bg
            *****************************************/
            this.UIBackground = new Phaser.Sprite(this.game, 0, 0, 'menu_ui', 'MenuBar');
            this.gameContainerGroup.add(this.UIBackground);

            /****************************************
              Bet Button/Panel
            *****************************************/
            this.UIBetPanel = new UIBetPanel(this.game, this.gameContainerGroup, this.behindUIBarGroup, this.game.api.config.availableBets);

            // listen on Bet Clicked
            this.UIBetPanel.events.onBetPanelSelected.add(()=>{
              this.gamefsm.handle('betPanelClicked');
            });
            this.UIBetPanel.events.onUpClicked.add(()=>{
              this.gamefsm.handle('betPanelUpClicked');
            });
            this.UIBetPanel.events.onDownClicked.add(()=>{
              this.gamefsm.handle('betPanelDownClicked');
            });
            /****************************************
              Play Button
            *****************************************/
            this.UIPlayButton = new UIPlayButton(this.game, this.gameContainerGroup);

            // listen on Play Button pressed
            this.UIPlayButton.events.onPlaySelected.add(()=>{
              this.gamefsm.handle('playClicked');
            });

            this.UIPlayButton.events.onStopSelected.add(()=>{
              this.gamefsm.handle('stopClicked');
            });

            /*******************************************
              Fix for ios8iphone5.
            ********************************************/
            if (!(this.game.net.medium == 'ds' || this.game.net.medium == 'ma')) {
              if(this.game.device.iPhone && (this.game.device.iOSVersion === 9 || this.game.device.iOSVersion === 8) && (this.game.device.iPhoneDevice === 'iPhone5')){
                var bmd = this._roundRect (this.game.width, 80 * this.game.SCALE_FACTOR, 0, '0xffff', 1);

                this.iPhone5BlackBar = new Phaser.Sprite(
                      this.game,
                      0,
                      this.game.height - 80 * this.game.SCALE_FACTOR,
                      bmd);
                this.gameContainerGroup.add(this.iPhone5BlackBar);
              }
            }

            /****************************************
              Exit Button
            *****************************************/
            this.exitBtn = new TAtlasButton(this.game, 0, 0, 'menu_ui', this.exitBtnClick, this,  'BUTTON', 'HomeButton_OVER', 'HomeButton_ON', 'HomeButton_OFF');
            this.gameContainerGroup.add(this.exitBtn);

            /****************************************
              Sound Buttons
            *****************************************/
            this.soundBtn = new TAtlasButton(this.game, 0, 0, 'menu_ui', this.soundBtnClick, this,  'BUTTON', 'SoundButton_OVER', 'SoundButton_ON', 'SoundButton_OFF');
            this.gameContainerGroup.add(this.soundBtn);

            this.disabledSoundBtn = new TAtlasButton(this.game, 0, 0, 'menu_ui', this.soundBtnClick, this,  'BUTTON', 'SoundButton_OFF', 'SoundButton_OFF', 'SoundButton_OFF');
            this.gameContainerGroup.add(this.disabledSoundBtn);
            this.disabledSoundBtn.visible = false;

            /****************************************
              Help Buttons
            *****************************************/
            this.helpBtn = new TAtlasButton(this.game, 0, 0, 'menu_ui', this.helpBtnClicked, this,  'SE 16', 'HelpButton_OVER', 'HelpButton_ON', 'HelpButton_OFF');
            this.gameContainerGroup.add(this.helpBtn);

            /****************************************
              Clock
            *****************************************/
            if(this.game.locale.settings.code != 'bs-BA'){
              this.clock = new RTClock(this.game, this.gameContainerGroup);
            }

            /****************************************
              Balance Panel
            *****************************************/
            this.balancePanel = new BalancePanel(this.game, this.gameContainerGroup);

            /****************************************
              Helpscreen
            *****************************************/
            this.helpPanel = new HelpPanel(this.game, this.gameContainerGroup);
            this.helpPanel.events.onCloseHelpBtnClickedEvent.add(()=>{
              this.gamefsm.closeHelpBtnClicked();
            });

            /****************************************
              Spinner
            *****************************************/
            this.spinner = new Spinner(this.game, this.gameContainerGroup, 0, 0, 'spinner_anim');
            this.gameContainerGroup.add(this.spinner);

            this.isTicketRetry = false;
            this.isUpdateTicketRetry = false;

            // After all game elements added run the first resize
            this.gameResize();
            this.gamefsm.handle('advanceToInitialBalance');

          },
          advanceToInitialBalance: 'INITIALBALANCE'
        },
        INITANIMATIONS:{
          _onEnter: ()=>{
            console.info('Game->INITANIMATIONS->_onEnter()');

            this.game.ImageCacher = new ImageCacher(this.game, this.gameContainerGroup,
                                                    this.game.imagesToCache);
            this.game.ImageCacher.performCache()
              .then( ()=>{
                this.preloadContainerGroup.destroy();

                this.preloadContainerGroup = null;
                // Remove strictly loading assets from cache
                // Not needed anymore.
                this.game.cache.removeImage('splash');
                this.game.cache.removeImage('flare_anim');
                this.game.ImageCacher = null;

                this.gamefsm.transition('INIT');
              });
          }
        },
        INITIALBALANCE:{
          _onEnter: () => {
            console.info('Game->INITIALBALANCE->_onEnter()');

            this.balancePanel.balance = this.game.api.config.balance;
            this.game.LobbyProxy.lobbyProxy.SendMessage(this.game.LobbyProxy.LOBBY_MESSAGES_ENUM.LOBBY_MESSAGE_REFRESH_BALANCE);

            this.balancePanel.resetTicket();
            var availableBets = this.UIBetPanel.updateBets(this.game.api.config.balance);

            if(availableBets.length > 0){
              this.gamefsm.handle('advanceToIdle');
            }
            else{
              this.gamefsm.handle('noAvailableBets');
            }
          },
          advanceToIdle: ()=>{
            if(this.game.storage.getItem('isFirstVisit') === null ){
              this.game.storage.setItem('isFirstVisit', 'true');
              this.gamefsm.transition('HELPDISPLAY');
            }
            else{
              this.gamefsm.transition('IDLE');
              // this.gamefsm.transition('NOBALANCE');
              // this.gamefsm.transition('NETWORKERROR');
            }
          },
          noAvailableBets: 'NOBALANCE'
        },
        NOBALANCE:{
          _onEnter: ()=>{
            console.info('Game->NOBALANCE->_onEnter()');

            this.UIPlayButton.state = 'PLAY';
            this.UIPlayButton.disable();
            this.UIBetPanel.inactive();
            this.exitBtn.enable();
            this.helpBtn.enable();

            this.balancePanel.resetTicket();
            if (this.UIAutoPlayPanel) {
              this.UIAutoPlayPanel.disable();
            }

            this.notification.show('noBalance').then(()=>{
              this.noBalanceTimer = this.game.time.events.add(Phaser.Timer.SECOND * 3, ()=>{
                this.notification.close()
                  .then( ()=>{
                    this.gamefsm.handle('timeout');
                  });
              }, this);
            });

          },
          exitBtnClick: ()=>{this.gotoLobbyFcn();},
          popupClicked: ()=>{
            this.notification.close();
            this.gamefsm.handle('timeout');
          },
          timeout: ()=>{
            // clearTimeout(this.noBalanceTimer);
            this.game.time.events.remove(this.noBalanceTimer);

            // window.location.href = CONFIG.rgsURL;
          },
          _onExit: ()=>{
            this.notification.close();
            this.game.time.events.remove(this.noBalanceTimer);
          },

          helpClicked: 'HELPDISPLAY'
        },
        IDLE: {
          _onEnter:()=>{
            console.info('Game->IDLE->_onEnter()');
            this.CONF_REQUESTS = 4;
            // contains input handlers for the different inputs
            this.UIBetPanel.enable();
            this.UIPlayButton.enable();

            this.helpBtn.enable();
            this.exitBtn.enable();

            this.gamefsm.transition('BETPICKED');
          },
          exitBtnClick: ()=>{this.gotoLobbyFcn();},
          playClicked: 'NOBETSELECTED',
          betClicked: 'BETPICKED',
          helpClicked: 'HELPDISPLAY',
        },
        BETSELECT:{
          _onEnter: ()=>{
            console.info('Game->BETSELECT->_onEnter()');
            this.UIBetPanel.open();
            this.UIBetPanelopenTimer = this.game.time.events.add(Phaser.Timer.SECOND * 4,
              () => {
                this.gamefsm.handle('timeout');
              }, this);
          },
          timeout: ()=>{
            this.UIBetPanel.close().then(()=>{
              this.gamefsm.transition('BETPICKED');
            });
          },
          autoPlayClicked:()=>{
            this.UIBetPanel.close().then(()=>{
              this.gamefsm.transition('AUTOPLAYSELECT');
            });
          },
          helpClicked: ()=>{
            this.UIBetPanel.close().then(()=>{
              this.gamefsm.transition('HELPDISPLAY');
            });
          },
          betPanelClicked: ()=>{
            this.UIBetPanel.close().then(()=>{
              this.gamefsm.transition('BETPICKED');
            });
          },
          betPanelUpClicked: ()=>{
           // this.paytablePanel.switchToTier(this.UIBetPanel.wager);

            this.game.time.events.remove(this.UIBetPanelopenTimer);
            this.UIBetPanelopenTimer = this.game.time.events.add(Phaser.Timer.SECOND * 4,
              () => {
                this.gamefsm.handle('timeout');
              }, this);
          },
          betPanelDownClicked: ()=>{
            //this.paytablePanel.switchToTier(this.UIBetPanel.wager);

            this.game.time.events.remove(this.UIBetPanelopenTimer);
            this.UIBetPanelopenTimer = this.game.time.events.add(Phaser.Timer.SECOND * 4,
              () => {
                this.gamefsm.handle('timeout');
              }, this);
          },
          playClicked: ()=>{
            this.UIBetPanel.close().then(()=>{
              this.game.play.isRequestedImmediatePlay = true;
              this.gamefsm.transition('BETPICKED');
            });
          },
          exitBtnClick: ()=>{this.gotoLobbyFcn();},
          _onExit: ()=>{
            this.game.time.events.remove(this.UIBetPanelopenTimer);
          }
        },
        AUTOPLAYSELECT:{
          _onEnter: ()=>{
            this.game.play.isAutoPlayMode = false;
            this.UIPlayButton.state = 'PLAY';
            this.UIAutoPlayPanel.open();
            this.UIAutoPlayPanelopenTimer = this.game.time.events.add(Phaser.Timer.SECOND * 4,
              () => {
                this.gamefsm.handle('timeout');
              }, this);
          },
          betPanelClicked:()=>{
            this.UIAutoPlayPanel.close().then(()=>{
              this.gamefsm.transition('BETSELECT');
            });
          },
          timeout: ()=>{
            this.UIAutoPlayPanel.close().then(()=>{
              this.gamefsm.transition('BETPICKED');
            });
          },
          autoPlayClicked: ()=>{
            this.UIAutoPlayPanel.close().then(()=>{
              this.gamefsm.transition('BETPICKED');
            });
          },
          autoPlayMinusClicked: ()=>{
            this.game.time.events.remove(this.UIAutoPlayPanelopenTimer);
            this.UIAutoPlayPanelopenTimer = this.game.time.events.add(Phaser.Timer.SECOND * 4,
              () => {
                this.gamefsm.handle('timeout');
              }, this);
          },
          autoPlayPlusClicked: ()=>{
            this.game.time.events.remove(this.UIAutoPlayPanelopenTimer);
            this.UIAutoPlayPanelopenTimer = this.game.time.events.add(Phaser.Timer.SECOND * 4,
              () => {
                this.gamefsm.handle('timeout');
              }, this);
          },
          helpClicked: ()=>{
            this.UIAutoPlayPanel.close().then(()=>{
              this.gamefsm.transition('HELPDISPLAY');
            });
          },
          playClicked: ()=>{
            this.UIAutoPlayPanel.close().then(()=>{
              this.game.play.isAutoPlayMode = true;
              this.game.play.isRequestedImmediatePlay = true;
              this.gamefsm.transition('BETPICKED');
            });
          },
          exitBtnClick: ()=>{this.gotoLobbyFcn();},
          _onExit: ()=>{
            this.game.time.events.remove(this.UIAutoPlayPanelopenTimer);

            this.UIAutoPlayPanel.enable();

            if(this.UIAutoPlayPanel.autoPlayTimesChosen >1 ){
              this.game.play.isAutoPlaySelected = true;

              this.UIPlayButton.state = 'PLAYTIMES';
            }
            else{
              this.game.play.isAutoPlaySelected = false;
            }
            this.game.play.turnsLeft = this.UIAutoPlayPanel.autoPlayTimesChosen;
            this.UIPlayButton.turnsLeft = this.game.play.turnsLeft;
          }
        },
        HELPDISPLAY:{
          _onEnter: ()=>{
            console.info('Game->HELPDISPLAY->_onEnter()');
            //Disable all other button panels
            this.UIBetPanel.disable();
            if(this.game.api.config.hasAutoPlay){
              this.UIAutoPlayPanel.disable();
            }

            this.UIPlayButton.disable();
            this.exitBtn.inputEnabled = false;
            this.helpBtn.inputEnabled = false;
            this.soundBtn.inputEnabled = false;
            this.helpPanel.show();
          },
          closeHelpClicked: ()=>{
            this.helpPanel.close()
            .then(()=>{
              if(this.game.storage.getItem('isFirstVisit') === 'true'){
                this.game.isFirstVisit = false;
                this.game.storage.setItem('isFirstVisit', 'false');
                this.gamefsm.transition('IDLE');
              }
              else{
                // this.gamefsm.transition('BETPICKED');
                if (this.gamefsm.priorState === 'NOBALANCE' || this.gamefsm.priorState === 'NETWORKERROR') {
                  this.gamefsm.transition(this.gamefsm.priorState);
                }
                else {
                  this.gamefsm.transition('BETPICKED');
                }
              }
            });
          },
          _onExit: ()=>{
            console.info('Game->HELPDISPLAY->_onExit()');
            //enable all bet panels
            this.UIBetPanel.enable();
            if(this.game.api.config.hasAutoPlay){
              this.UIAutoPlayPanel.enable();
            }

            this.UIPlayButton.enable();

            this.helpBtn.inputEnabled = true;
            this.soundBtn.inputEnabled = true;
            this.exitBtn.inputEnabled = true;
          }
        },
        BETPICKED:{
          _onEnter: ()=>{
            console.info('Game->BETPICKED->_onEnter()');
            this.game.LobbyProxy.lobbyProxy.SendMessage(this.game.LobbyProxy.LOBBY_MESSAGES_ENUM.LOBBY_MESSAGE_STATE_WAIT_FOR_BET);

            if(this.clock){
              this.clock.visible = true;
            }

            let shouldShowTicketId = this.game.api.config.displayTicketId;
            if(shouldShowTicketId){
              this.balancePanel.ticketIDText.visible = false;
            }


            if(this.game.api.config.hasAutoPlay && this.game.play.isAutoPlayMode){
              this.UIBetPanel.disable();
              this.helpBtn.disable();
              this.exitBtn.disable();
            }
            else{
              this.UIBetPanel.enable();
              this.helpBtn.enable();
              this.exitBtn.enable();
            }
            this.soundBtn.enable();

            //this.paytablePanel.highlightReset();

            //this.paytablePanel.switchToTier(this.UIBetPanel.wager);

            // if play was pressed while in BETSELECTED state then
            // simulate playClicked
            if(this.game.play.isRequestedImmediatePlay){
              this.gamefsm.handle('playClicked');
            }
            /***
                Auto play conf
            ***/

            if(this.game.api.config.hasAutoPlay){
              if(this.game.play.isAutoPlaySelected){

                if(this.game.play.isAutoPlayMode){
                  this.UIPlayButton.state = 'STOP';
                  this.UIPlayButton.enable();
                  this.gamefsm.transition('BETREQUEST');
                }
                else{
                  this.UIAutoPlayPanel.enable();
                  this.UIPlayButton.state = 'PLAYTIMES';
                  this.UIPlayButton.enable();
                }
              }
              else{
                this.UIAutoPlayPanel.enable();
                this.UIPlayButton.state = 'PLAY';
                this.UIPlayButton.enable();
              }
            }
            else{
              this.UIPlayButton.enable();
              this.UIPlayButton.state = 'PLAY';
            }

          },
          exitBtnClick: ()=>{this.gotoLobbyFcn();},
          betPanelClicked: 'BETSELECT',
          autoPlayClicked: 'AUTOPLAYSELECT',
          playClicked: ()=>{
            var promises = [];
            /***
                Auto play conf
            ***/
            if(this.game.api.config.hasAutoPlay){
              promises.push(this.UIAutoPlayPanel.close());
              if(this.game.play.isAutoPlaySelected){
                this.game.play.isAutoPlayMode = true;
              }
            }

            promises.push(this.UIBetPanel.close());


            Promise.all(promises).then(()=>{
              this.gamefsm.transition('BETREQUEST');
            });
          },
          helpClicked: 'HELPDISPLAY',
          _onExit: ()=>{
            //reset immediate play variable
            this.game.play.isRequestedImmediatePlay = false;
          }
        },
        BETREQUEST:{
          _onEnter: ()=>{
            console.info('Game->BETREQUEST->_onEnter()');
            this.game.LobbyProxy.lobbyProxy.SendMessage(this.game.LobbyProxy.LOBBY_MESSAGES_ENUM.LOBBY_MESSAGE_STATE_ACTIONS_BEGIN);
            if(this.game.api.config.hasAutoPlay && this.game.play.isAutoPlayMode){

              this.UIPlayButton.disable();
              this.UIBetPanel.disable();
              this.UIAutoPlayPanel.disable();
            }
            else{
              this.UIAutoPlayPanel.disable();
              this.UIPlayButton.disable();
              this.UIBetPanel.disable();
            }


            var wagerData = {
              SymbolID: [this.sliderPanel.SymbolID], //needs array
              PayTableID: 3 /*this.paytablePanel.paytableId*/,
              Amount: this.UIBetPanel.wager * this.game.api.config.currencyDivisor,
              BetMultiplier:0,
              CData: null
            };
            console.log(wagerData);
            var bet = '{"bet":' + this.UIBetPanel.wager * this.game.api.config.currencyDivisor +'}'
            this.game.LobbyProxy.lobbyProxy.SendMessage(this.game.LobbyProxy.LOBBY_MESSAGES_ENUM.LOBBY_MESSAGE_TICKET_REQUEST, bet);

            this.game.api.ticket.request(wagerData, this.game.api.TicketCData, this.isTicketRetry)
            .then(()=>{
              this.game.LobbyProxy.lobbyProxy.SendMessage(this.game.LobbyProxy.LOBBY_MESSAGES_ENUM.LOBBY_MESSAGE_TICKET_RESPONSE);
              this.game.LobbyProxy.lobbyProxy.SendMessage(this.game.LobbyProxy.LOBBY_MESSAGES_ENUM.LOBBY_MESSAGE_STATE_NETWORK_GET_TICKET);

              this.game.api.TicketCData = this.game.api.ticket.CData;
              this.spinner.stop();

              let shouldShowTicketId = this.game.api.config.displayTicketId;
              if(shouldShowTicketId)
              {
                this.balancePanel.ticketIDText.visible = true;
                this.balancePanel.ticketId = this.game.api.ticket.ticketID;
              }


              // Reset BETREQUEST data
              this.CONF_REQUESTS = 4;
              this.game.api.ticket.clearRetryRequestID();
              this.isTicketRetry = false;

              this.gamefsm.handle('advancetoPlay');
            })
            .catch((errorData)=>{
              this.CONF_REQUESTS -= 1;

              this.errorData = errorData;

              if (this.CONF_REQUESTS <= 0 || !(errorData.ResponseCode == '' || errorData.ResponseCode == '408' || errorData.ResponseCode == '429')) {
                this.gamefsm.transition('NETWORKERROR');
              }
              else if (errorData.ResponseCode == '429') {
                this.CONF_REQUESTS += (this.CONF_REQUESTS < 4) ? 1 : 0;
                this.gamefsm.handle('failRequest');
              }
              else{
                this.gamefsm.handle('failRequest');
              }
            });
          },
          stopClicked: ()=>{
            this.gamefsm.stopBtnClicked();
          },
          advancetoPlay: 'PLAY',
          failRequest: 'REENTERBETREQ',
        },
        REENTERBETREQ:{
          _onEnter: ()=>{
            this.spinner.start();

            this.isTicketRetry = true;

            this.timer = setTimeout( ()=>{
              this.gamefsm.handle('timeout');
            }, 1000 );
          },
          timeout: 'BETREQUEST',
          _onExit: ()=>{
            clearTimeout( this.timer );
          }
        },
        NETWORKERROR: {
          _onEnter: ()=>{
            console.info('Game->NETWORKERROR->_onEnter()');

            this.spinner.stop();


            if(this.game.api.config.hasAutoPlay){
              this.UIAutoPlayPanel.disable();
            }

            this.balancePanel.resetTicket();
            this.UIPlayButton.disable();
            this.UIBetPanel.disable();


            this.helpBtn.enable();
            this.exitBtn.enable();
            this.notification.show('rgsError', '', this.errorData).then(()=>{
              this.timer = this.game.time.events.add(Phaser.Timer.SECOND * 3,
                    () => {
                      this.gamefsm.handle('timeout');
                    }, this);
            });
          },
          exitBtnClick: ()=>{this.gotoLobbyFcn();},
          timeout: ()=>{this.gotoLobbyFcn();},
          popupClicked: ()=>{
            this.game.time.events.remove(this.timer);
            this.notification.close();
            this.gamefsm.handle('timeout');
          },
          _onExit: ()=>{
            this.notification.close();
            this.game.time.events.remove(this.timer);
          },
          helpClicked: 'HELPDISPLAY'
        },
        // TODO consider ith needs to be removed
        NOBETSELECTED:{
          _onEnter: ()=>{
            console.info('Game->NOBETSELECTED->_onEnter()');
            this.UIBetPanel.disable();
            this.exitBtn.disable();
            this.helpBtn.disable();

            this.notification.show('noBetSelected').then(()=>{
              this.timer = setTimeout( ()=>{
                this.gamefsm.handle('timeout');
              }, 3000 );
            });
          },
          timeout: 'IDLE',
          popupClicked: ()=>{
            clearTimeout( this.timer );
            this.notification.close();
            this.gamefsm.transition('IDLE');
          },
          _onExit: ()=>{
            this.UIBetPanel.enable();
            this.notification.close();
            clearTimeout( this.timer );
          }
        },
        PLAY:{
          _onEnter: ()=>{
            console.info('Game->PLAY->_onEnter()');
            // Set the desired fps to be 60
            this.game.time.desiredFps = 60;

            //Update balance
            this.balancePanel.balance = this.game.api.ticket.initialBalance;
            this.game.LobbyProxy.lobbyProxy.SendMessage(this.game.LobbyProxy.LOBBY_MESSAGES_ENUM.LOBBY_MESSAGE_REFRESH_BALANCE);

            if(this.game.api.config.hasAutoPlay){
              if(this.game.play.isAutoPlayMode){
                this.UIPlayButton.enable();
              }
              else{
                this.UIPlayButton.disable();
              }
              this.UIAutoPlayPanel.disable();

            }

            this.UIBetPanel.disable();

            this.helpBtn.disable();
            this.exitBtn.disable();

            /*
              Create Game Elements and Reveal
            */
           

            /***
                Auto play conf
            ***/
            if(this.game.api.config.hasAutoPlay){
              if(this.game.play.isAutoPlaySelected){
                //TODO check if is needed here because of BETPICKED
                this.UIPlayButton.state = 'STOP';
                this.UIPlayButton.turnsLeft = this.game.play.turnsLeft;
              }
              else{
                this.UIPlayButton.disable();
                this.UIPlayButton.state = 'PLAY';
              }
            }
            else{
              this.UIPlayButton.disable();
              this.UIPlayButton.state = 'PLAY';
            }
          },
          stopClicked: ()=>{
            this.gamefsm.stopBtnClicked();
          },
          revealed : ()=>{
            this.winTimer = this.game.time.events.add(Phaser.Timer.SECOND * 2.5,
                    () => {
                      this.gamefsm.transition('RESULT');
                    }, this);
          },
          _onExit: ()=>{
            this.game.time.events.remove(this.winTimer);
            // Set the desired fps to be 30
            this.game.time.desiredFps = 30;
          }
        },
        RESULT: {
          _onEnter: ()=>{
            console.info('Game->RESULT->_onEnter()');
            if(this.game.api.ticket.isWinner){
              this.game.fx.play('WIN');
              this.notification.show('win', this.game.api.ticket.winAmount).then(()=>{
                this.gamefsm.resultTimer = this.game.time.events.add(Phaser.Timer.SECOND * 3,
                    () => {
                      this.gamefsm.handle('timeout');
                    }, this);
              });
            }
            else{
              this.game.fx.play('LOOSE');
              this.notification.show('lose').then(()=>{
                this.gamefsm.resultTimer = this.game.time.events.add(Phaser.Timer.SECOND * 3,
                    () => {
                      this.gamefsm.handle('timeout');
                    }, this);
              });
            }
          },
          stopClicked: ()=>{
            this.gamefsm.stopBtnClicked();
          },
          timeout: ()=>{
            this.notification.close().then(()=>{
              this.game.LobbyProxy.lobbyProxy.SendMessage(this.game.LobbyProxy.LOBBY_MESSAGES_ENUM.LOBBY_MESSAGE_STATE_ACTIONS_FINISH);
              this.gamefsm.transition('UPDATEBALANCE');
            });
          },
          popupClicked: ()=>{
            this.game.time.events.remove(this.gamefsm.resultTimer);
            this.game.fx.stop();
            this.notification.close().then(()=>{
              this.gamefsm.handle('timeout');
            });
          },
          _onExit: ()=>{
            this.game.time.events.remove(this.gamefsm.resultTimer);
          }
        },
        UPDATEBALANCE: {
          _onEnter: ()=>{
            console.info('Game->UPDATEBALANCE->_onEnter()');
            this.balancePanel.balance = this.game.api.ticket.finalBalance;
            this.game.LobbyProxy.lobbyProxy.SendMessage(this.game.LobbyProxy.LOBBY_MESSAGES_ENUM.LOBBY_MESSAGE_REFRESH_BALANCE);

            this.gamefsm.handle('updateTicket');
          },
          stopClicked: ()=>{
            this.gamefsm.stopBtnClicked();
          },
          updateTicket: 'UPDATETICKET'
        },
        UPDATETICKET:{
          _onEnter: ()=>{
            console.info('Game->UPDATETICKET->_onEnter()');

            var ticketData = {
              TicketID: this.game.api.ticket.ticketID,
              State:  0, //should close this tuicket
              ActionID:  this.game.api.ticket.actionID,
              SymbolDistribution: this.game.api.ticket.symbolDistribution
            };
            this.game.api.ticketUpdate.request(ticketData, this.game.api.TicketCData, this.isUpdateTicketRetry)
            .then(()=>{
              this.spinner.stop();

              this.CONF_REQUESTS = 4;
              this.game.api.ticketUpdate.clearRetryRequestID();
              this.isUpdateTicketRetry = false;

              this.gamefsm.handle('successRequest');
            })
            .catch((errorData)=>{
              this.CONF_REQUESTS -= 1;

              this.errorData = errorData;

              if (this.CONF_REQUESTS <= 0 || !(errorData.ResponseCode == '' || errorData.ResponseCode == '408' || errorData.ResponseCode == '429')) {
                this.gamefsm.transition('NETWORKERROR');
              }
              else if (errorData.ResponseCode == '429') {
                this.CONF_REQUESTS += (this.CONF_REQUESTS < 4) ? 1 : 0;
                this.gamefsm.handle('failRequest');
              }
              else{
                this.gamefsm.handle('failRequest');
              }
            });
          },
          stopClicked: ()=>{
            this.gamefsm.stopBtnClicked();
          },
          successRequest: ()=>{
            this.game.LobbyProxy.lobbyProxy.SendMessage(this.game.LobbyProxy.LOBBY_MESSAGES_ENUM.LOBBY_MESSAGE_STATE_NETWORK_CLOSE_ACTION);
            /***
                Auto play conf
            ***/
            if(this.game.api.config.hasAutoPlay && this.game.play.isAutoPlayMode){
              this.game.play.turnsLeft -= 1;
              this.UIPlayButton.turnsLeft = this.game.play.turnsLeft;
              if(this.game.play.turnsLeft === 0){
                this.game.play.isAutoPlayMode = false;
                this.game.play.isAutoPlaySelected = false;
                this.UIAutoPlayPanel.autoPlayTimesChosen = 1;
                this.UIAutoPlayPanel.resetAvailableAutoplays();
              }
            }


            if((this.game.api.config.hasAutoPlay && this.game.play.isAutoPlayMode) && (this.UIBetPanel.wager > this.game.api.ticket.finalBalance)){
              console.log('Stopping AutoPlay, insufficient funds for current wager. wager/funds ', this.UIBetPanel + ' / ' + this.game.api.ticket.finalBalance);
              this.gamefsm.handle('stopClicked');
            }

            var availableBets = this.UIBetPanel.updateBets(this.game.api.ticket.finalBalance);

            if(availableBets.length > 0){
              this.gamefsm.handle('goToBetPicked');
            }
            else{
              this.gamefsm.handle('noAvailableBets');
            }
          },
          failRequest: 'REENTERUPDATETICKET',
          goToBetPicked: 'BETPICKED',
          noAvailableBets: 'NOBALANCE'
        },
        REENTERUPDATETICKET:{
          _onEnter: ()=>{
            console.info('Game->REENTERUPDATETICKET->_onEnter()');
            this.spinner.start();

            this.isUpdateTicketRetry = true;

            this.timer = setTimeout( ()=>{
              this.gamefsm.handle('timeout');
            }, 1000 );
          },
          timeout: 'UPDATETICKET',
          _onExit: ()=>{
            clearTimeout( this.timer );
          }
        }
      },

      stopBtnClicked: ()=>{
        this.UIPlayButton.state = 'PLAY';
        this.UIPlayButton.disable();
        this.game.play.isAutoPlayMode = false;
        this.game.play.isAutoPlaySelected = false;
        this.UIAutoPlayPanel.autoPlayTimesChosen = 1;
        this.UIAutoPlayPanel.resetAvailableAutoplays();
      },

      goToInitAnimations: ()=>{
        this.gamefsm.handle('advanceToInitAnimations');
      },
      goToInitialBalance: ()=>{
        this.gamefsm.handle('advanceToInitialBalance');
      },
      helpBtnClicked: ()=>{
        this.gamefsm.handle('helpClicked');
      },
      closeHelpBtnClicked: ()=>{
        this.gamefsm.handle('closeHelpClicked');
      },
      popupClicked: ()=>{
        this.gamefsm.handle('popupClicked');
      }
    });

    // Initialize FSM
    this.gamefsm = new gameFSM();
    this.gamefsm.goToInitAnimations();
    window.fsm = this.gamefsm;

  }
  handlePostMessage (event){
    console.log('received response:  ', event.data);
    if(event.data === 'gameClose'){
      this.game.fx.destroySound();
    }
  }

  // This resize function is the callback of browser's window resize event directly
  gameResize (){

    var ratio;
    var width, height;

    if(!this.game.device.desktop && this.game.net.medium != 'ds') {
      this.checkOrientation();
    }

    if(this.game.device.iPhone4 && this.game.device.iOSVersion === 7){
      width = window.outerWidth;
      height = window.outerHeight;
      ratio = height / this.game.SCALE_INITIAL_HEIGHT;
    }
    else{
      ratio = window.innerHeight / this.game.SCALE_INITIAL_HEIGHT;
      if(this.game.device.iPhone && (this.game.device.iOSVersion === 9 || this.game.device.iOSVersion === 8)){
        width = document.documentElement.clientWidth;
        height = document.documentElement.clientHeight;
      }
      else{
        width = window.innerWidth;
        height = window.innerHeight;
      }

      ratio = height / this.game.SCALE_INITIAL_HEIGHT;
    }
    document.getElementById('Game').style.width = width + 'px';
    document.getElementById('Game').style.height = height + 'px';

    // display width smaller than game's
    if(this.game.SCALE_INITIAL_WIDTH * ratio > width){
      this.game.scale.setGameSize (width/ratio, this.game.SCALE_INITIAL_HEIGHT);
      //catch the case that is smaller than safe area
      if(width < this.game.SAFE_AREA_WIDTH * ratio){
        this.game.scale.setGameSize (this.game.SAFE_AREA_WIDTH, this.game.SCALE_INITIAL_HEIGHT);
      }
    }
    else{// display width bigger than game's
      this.game.scale.setGameSize (this.game.SCALE_INITIAL_WIDTH, this.game.SCALE_INITIAL_HEIGHT);
    }

    if(this.game.device.iPhone && (this.game.device.iOSVersion === 9 || this.game.device.iOSVersion === 8)){
      if (!(this.game.net.medium == 'ds' || this.game.net.medium == 'ma')) {
        this._gameRePosition();
        this.ios9FullscreenCheck();
      }
    }
    this._gameRePosition();
  }

  _gameRePosition (){
    this.gameContainerGroup.x = -Math.abs((this.game.width - this.gameContainerGroup.width)/2);
  }

  ios9FullscreenCheck (){
    if(window.innerHeight< window.innerWidth){ //landscape
      if (window.innerHeight == document.documentElement.clientHeight){
        this.CSSscrollUpDiv.style.visibility = 'hidden';
        this.CSSscrollUpImageDiv.style.visibility = 'hidden';
        this.CSSscrollUpImageDiv.className = 'scrollup-image';
      }
      else{
        this.CSSscrollUpDiv.style.visibility = 'visible';
        this.CSSscrollUpImageDiv.style.visibility = 'visible';
        this.CSSscrollUpImageDiv.className = 'scrollup-image slideUp';
      }
    }
    else{//portrait
      this.CSSscrollUpDiv.style.visibility = 'hidden';
      this.CSSscrollUpImageDiv.style.visibility = 'hidden';
      this.CSSscrollUpImageDiv.className = 'scrollup-image';
    }
  }

  gotoLobbyFcn (){
    if(this.game.device.desktop){
      this.game.LobbyProxy.lobbyProxy.SendMessage(this.game.LobbyProxy.LOBBY_MESSAGES_ENUM.LOBBY_MESSAGE_GO_HOME);
    }
    else{
      if (this.game.net.medium && this.game.net.medium === 'mb') {
        window.location.href = this.game.net.LobbyUrl || window.location.href;
      }
      else if (this.game.net.medium && (this.game.net.medium === 'ds' || this.game.net.medium === 'gb')) {
        this.game.LobbyProxy.lobbyProxy.SendMessage(this.game.LobbyProxy.LOBBY_MESSAGES_ENUM.LOBBY_MESSAGE_GO_HOME);

      }
      else {
        window.location.href = this.game.net.LobbyUrl || window.location.href;
      }
    }
  }

  exitBtnClick (){
    this.gamefsm.handle('exitBtnClick');
  }

soundBtnClick (){

////test code/////////

var sprite;


  
    //   var fragmentSrc = [
    //     "precision mediump float;",
    //     "uniform vec2      resolution;",
    //     "uniform float     time;",

    //     "float size = 0.002;",

    //     "void main( void ) {",
    //         "vec2 view = ( gl_FragCoord.xy - resolution / 2.0 ) / ( resolution.y / 2.0);",
    //         "float time = time + length(view)*8.;",
    //         "vec4 color = vec4(0);",
    //         "vec2 center = vec2(0);",
    //         "float rotationVelocity = 2.0;",
    //         "for( int j = 0; j < 20; j++ ) {",
    //             "for( int i = 0; i < 20; i++ ) {",
    //                 "float amplitude = ( cos( time / 10.0 ) + sin(  time /5.0 ) ) / 2.0;",
    //                 "float angle =   sin( float(j) * time) * rotationVelocity + 2.0 * 3.14 * float(i) / 20.0;",
    //                 "center.x = cos( 7.0 * float(j) / 20.0 * 2.0 * 3.14 ) + sin( time / 4.0);",
    //                 "center.y = sin( 3.0 * float(j) / 20.0 * 2.0 *  3.14 )+ cos( time / 8.0);",
    //                 "vec2 light = center + amplitude * vec2( cos( angle ), sin( angle ));",
    //                 "//size = sin( time ) * 0.005;",
    //                 "float l = size / length( view - light );",
    //                 "vec4 c = vec4( l / 20.0, l, l, 1.0 ) / 5.0;",
    //                 "color += c;",
    //             "}",
    //         "}",
    //         "gl_FragColor = color;",
    //     "}"
    // ];

    // this.filter = new Phaser.Filter(this.game, null, fragmentSrc);
    // this.filter.setResolution(800, 600);


    // this.testImage.filters = [ this.filter ];

 
    

  //   var cx = 0;

  // //  Generate our motion data
  // var motion = { x: 0 };
  // var tween = this.game.add.tween(motion).to( { x: 200 }, 3000, "Bounce.easeInOut", true, 0, -1, true);
  // this.waveform = tween.generateData(60);

  // this.xl = this.waveform.length - 1;


  // var picWidth = this.testImage.width;
  // var picHeight = this.testImage.height;

  // var ys = 4;


  // for (var y = 0; y < Math.floor(picHeight/ys); y++)
  // {
  //     var star = this.game.add.sprite(300, 100 + (y * ys), this.testImage);

  //     star.crop(new Phaser.Rectangle(0, y * ys, picWidth, ys));

  //     star.ox = star.x;

  //     star.cx = this.game.math.wrap(y * 2, 0, this.xl);

  //     star.anchor.set(0.5);
  //     this.slices.push(star);
  // }


  var fragmentSrc = [
    
            // "precision mediump float;",
    
            // "uniform float     time;",
            // "uniform vec2      resolution;",
            // "uniform sampler2D iChannel0;",
    
            // "void main( void ) {",
    
            //     "vec2 uv = gl_FragCoord.xy / resolution.xy;",
            //     "uv.y *= -1.0;",
            //     "uv.y += (sin((uv.x + (time * 0.5)) * 10.0) * 0.1) + (sin((uv.x + (time * 0.2)) * 32.0) * 0.01);",
            //     "vec4 texColor = texture2D(iChannel0, uv);",
            //     "gl_FragColor = texColor;",
    
            // "}"
            
        "precision mediump float;",
        
                "uniform float     time;",
                "uniform vec2      resolution;",
                "uniform sampler2D iChannel0;",
        
                "void main( void ) {",
        
                    "vec2 uv = gl_FragCoord.xy / resolution.xy;",
        
                    "// Flip-a-roo.",
                    "uv.y *= -1.0;",
        
                    "// Represents the v/y coord(0 to 1) that will not sway.",
                    "float fixedBasePosY = 0.0;",
        
                    "// Configs for you to get the sway just right.",
                    "float speed = 1.0;",
                    "float verticleDensity = 3.0;", //6
                    "float swayIntensity = 0.2;",
        
                    "// Putting it all together.",
                    "float offsetX = sin(uv.y * verticleDensity + time * speed) * swayIntensity;",
        
                    "// Offsettin the u/x coord.",
                    "uv.x += offsetX * (uv.y - fixedBasePosY);",
        
                    "gl_FragColor = texture2D(iChannel0, uv);",
        
                "}"
        ];
    
        //  Texture must be power-of-two sized or the filter will break
       
        // sprite = game.add.sprite(0, 0, 'texture');
        // sprite.width = 800;
        // sprite.height = 600;
    

        
        // this.testImage  = new Phaser.Sprite(this.game, 700, 50, 'atlas', 'test');
        // this.gameContainerGroup.add(this.testImage);

        console.log(this.testImage.key)
        this.testImage.frameName = 'test';
        this.testImage.frame = 60;
        this.testImage.frame = 59

        // this.testImage = this.game.add.tileSprite(0, 0, 512, 512, 'atlas', 'test');
        // console.log(this.testImage)
         var frame;

        frame = this.game.cache.getFrameByName('atlas', 'test');//texture = PIXI.TextureCache[frame.uuid]
        this.testImage = this.game.add.sprite(0, 0, 'atlas', 'test');

        this.testImage.loadTexture('atlas', 'test'); 


        // var bmd = this.game.make.bitmapData(512, 512);
        
        //     //  Draw an image to it
        // bmd.copy(this.testImage.bitmapData);

        // sprite = game.add.sprite(300, 300, bmd);
            
        var customUniforms = {
            iChannel0: { type: 'sampler2D', value: this.testImage.texture, textureData: { repeat: true } }
        };
    
        this.filter = new Phaser.Filter(this.game, customUniforms, fragmentSrc);
        this.filter.setResolution(this.testImage.width, this.testImage.height);
    
        this.testImage.filters = [ this.filter ];

///////test code///////////

    if(this.game.fx.mute){
      this.game.fx.enableSounds();
      this.disabledSoundBtn.visible = false;
    }else{
      this.game.fx.disableSounds();
      this.disabledSoundBtn.visible = true;
    }
  }
  // we wrap fsm's helpBtnClicked otherwise we got werro on creation of fsm
  helpBtnClicked (){
    this.gamefsm.helpBtnClicked();
  }

  onCloseHelpBtnClicked () {
    this.helpPanel.onCloseHelpBtnClicked();
  }

  checkOrientation (){
    if(window.innerHeight< window.innerWidth){
      document.getElementById('orientation').style.display = 'none';
      if (this.game.paused) {
        this.game.paused = false;
      }
    }
    else{
      if (this.game.scale.isFullScreen){
        this.game.scale.stopFullScreen(true);
      }

      var orientationPanel = document.getElementById('orientation');
      orientationPanel.style.display = 'block';
      this.game.paused = true;
      console.log('FLIPPED!');

    }
  }

  focusout (){
    this.game.fx.mute = true;
    this.game.paused = true;
    this.game.LobbyProxy.lobbyProxy.SendMessage(this.game.LobbyProxy.LOBBY_MESSAGES_ENUM.LOBBY_MESSAGE_LOST_FOCUS);
    if (this.game.isIE) {

      console.log('Lost focus. Creating invisible DOM element.');
      // Crate div
      this.ieDIV = document.createElement('div');
      document.body.appendChild(this.ieDIV);
      this.ieDIV.id = 'ieDIV';
      this.removeIEListener = this.ieMouseDown.bind(this);
      this.ieDIV.addEventListener('mousedown', this.removeIEListener);
    }
  }

  focusin (){
    this.game.fx.mute = false;
    this.game.paused = false;
    this.game.LobbyProxy.lobbyProxy.SendMessage(this.game.LobbyProxy.LOBBY_MESSAGES_ENUM.LOBBY_MESSAGE_GOT_FOCUS);
    if (this.game.isIE) {

      console.log('Got focus. Deleting invisible DOM element.');
      if (document.getElementById('ieDIV')) {
        document.removeEventListener('mousedown', this.removeIEListener);
        let ieDivRemove = document.getElementById('ieDIV');
        ieDivRemove.parentNode.removeChild(ieDivRemove);
      }
    }
  }

  ieMouseDown () {
    this.game.onFocus.dispatch();
  }

  _roundRect (width, height, radius, fill, alpha) {
    var bmd = new Phaser.BitmapData(this.game, 'roundedbg', width, height);
    var x = 0;
    var y = 0;

    if (typeof radius === 'undefined') {
      radius = 5;
    }

    bmd.ctx.beginPath();
    bmd.ctx.fillStyle = 'rgba(0, 0, 0,' + alpha + ')';
    bmd.ctx.moveTo(x + radius, y);
    bmd.ctx.lineTo(x + width - radius, y);
    bmd.ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
    bmd.ctx.lineTo(x + width, y + height - radius);
    bmd.ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
    bmd.ctx.lineTo(x + radius, y + height);
    bmd.ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
    bmd.ctx.lineTo(x, y + radius);
    bmd.ctx.quadraticCurveTo(x, y, x + radius, y);
    bmd.ctx.closePath();
    bmd.ctx.fill();
    return bmd;
  }

  update() {

    if(this.filter)
      this.filter.update();


      // for (var i = 0, len = this.slices.length; i < len; i++)
      // {
      //     this.slices[i].x = this.slices[i].ox + this.waveform[this.slices[i].cx].x;
  
      //     this.slices[i].cx++;
  
      //     if (this.slices[i].cx > this.xl)
      //     {
      //         this.slices[i].cx = 0;
      //     }
  
      // }
  }
}
