/*
 * Boot state
 * ===========================================================================
 *
 * The first state of the game, responsible for setting up some Phaser
 * features.
 * Also includes a Machina FSM for ticket request/connectivity handling
 */
import {HDR, HD, SD, RESOLUTIONS } from '../data/assets';

export default class Boot extends Phaser.State {

  init () {
    console.info('Boot->init()');

    // The game never looses focus if player in another tab
    this.game.stage.disableVisibilityChange = true;

    this.semVersion = document.getElementById('version').innerHTML;

    // Hacks to load fonts
    let text0 = this.game.add.text(0, 0, 'OpenSans', { font:'1px Roboto', fill:'#FFFFF'});
    let text1 = this.game.add.text(0, 0, 'OpenSans', { font:'1px Roboto-Bold', fill:'#FFFFF'});

    text0.destroy();
    text1.destroy();
  }

  preload () {
    console.info('Boot->preload()');
    // Find out what resolution mode we are running in so we can load appropriate asset packs
    var resolution;
    var designedWidth = 2520;
    var designedHeight;

    if(this.game.device.iPhone && (this.game.device.iOSVersion === 9 || this.game.device.iOSVersion === 8)){
      if (this.game.device.iPhoneDevice === 'iPhone5'){
        designedHeight = 1160;
      }
      else{
        designedHeight = 1080;
      }
    }
    else{
      designedHeight = 1080;
    }

    // Override designed height if we got a medium directive
    if (this.game.net.medium == 'ds' || this.game.net.medium == 'ma') {
      console.log('Medium directive --> ', this.game.net.medium);
      designedHeight = 1080;
    }

    if (this.game.net.medium == 'gb' ) {
      this.game.input.mouse.enabled = false;
    }
    let ua = window.navigator.userAgent;
    //catch ie11 and serve HD
    if (this.game.device.desktop && ua.match(/Trident/i) ){
      resolution = 'HD';
      this.game.SCALE_FACTOR = RESOLUTIONS.HD;
    }
    else if (this.game.device.desktop){
      resolution = 'HDR';
      this.game.SCALE_FACTOR = RESOLUTIONS.HDR;
    }
    // iphone 4
    else if(this.game.device.iOS && this.game.device.iOSVersion<8){
      console.info('DEVICE -> iPhone4');
      resolution = 'SD';
      this.game.SCALE_FACTOR = RESOLUTIONS.SD;
    }
    else if(this.game.device.iPhone && this.game.device.iOSVersion === 8){
      resolution = 'HD';
      this.game.SCALE_FACTOR = RESOLUTIONS.HD;
    }
    else if(this.game.device.iPhone && this.game.device.iOSVersion === 9){
      resolution = 'HD';
      this.game.SCALE_FACTOR = RESOLUTIONS.HD;
    }
    else if (this.game.device.iPad && (this.game.device.pixelRatio < 2)){//Ipad 1/2
      resolution = 'HD';
      this.game.SCALE_FACTOR = RESOLUTIONS.HD;
    }
    else if (this.game.device.iPad && (this.game.device.pixelRatio >= 2)){//Ipad 1/2
      resolution = 'HD';
      this.game.SCALE_FACTOR = RESOLUTIONS.HD;
    }
    else if(!this.game.device.desktop && (this.game.device.pixelRatio >= 2)){
      resolution = 'HD';
      this.game.SCALE_FACTOR = RESOLUTIONS.HD;
    }
    else if(!this.game.device.desktop && (this.game.device.pixelRatio < 2)){
      resolution = 'HD';
      this.game.SCALE_FACTOR = RESOLUTIONS.HD;
    }

    this.game.SAFE_AREA_WIDTH = 1440 * this.game.SCALE_FACTOR;

    this.game.SCALE_INITIAL_WIDTH = designedWidth * this.game.SCALE_FACTOR;
    this.game.SCALE_INITIAL_HEIGHT = designedHeight *this.game.SCALE_FACTOR;
    this.game.scale.setGameSize(this.game.SAFE_AREA_WIDTH, this.game.SCALE_INITIAL_HEIGHT);

    // Adjust how many pointers Phaser will check for input events.
    this.input.maxPointers = 1;

    this.game.scale.pageAlignHorizontally = true;
    this.game.scale.pageAlignVertically = true;

    this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    if (!this.game.device.desktop){

      // Scale mode for when the mobile is in fullscreen
      this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;


      // force orientation to be landscape and
      this.game.scale.forceOrientation(true, false);
    }

    // Tells Phaser to smooth texture corners and sub-pixels when rendering the
    // game canvas.
    this.stage.smoothed = true;
    //attach the resolution to the game in order to be available throughout the game
    this.game.resolution_dir = resolution;


    // Point the Phaser Asset Loader to appropriate resolution folder
    this.load.path = 'assets/' + resolution + '/';


    // Load 'boot' subsection of the appropriate res section,
    //  depending on resolution set above

    var resolutionFolder;
    // loadAssets

    if(this.game.resolution_dir === 'HDR'){
      resolutionFolder = HDR;
      console.info('Boot->Load boot HDR pack');
    }
    else if(this.game.resolution_dir === 'HD'){
      resolutionFolder = HD;
      console.info('Boot->Load boot HD pack');
    }
    else if(this.game.resolution_dir === 'SD'){
      resolutionFolder = SD;
      console.info('Boot->Load boot SD pack');
    }

    this.load.pack('boot', null, resolutionFolder);

    //this.load.pack('boot_' + this.game.api.config.localeID, null, resolutionFolder);
    if('boot_' + this.game.api.config.localeID in resolutionFolder){
      this.load.pack('boot_' + this.game.api.config.localeID, null, resolutionFolder);
    }
    else{//load defalult en-GB
      this.load.pack('boot_' + 'en-GB', null, resolutionFolder);
    }

    this.load.onFileError.add(this.errorLoadingFile, this);

  }

  create (){
    console.info('PreBoot->create()');
    // all assets have been loaded
    if(!this.loadingCorrupted){
      this.assetsReady = true;
    }
  }

  update () {
    if(this.loadingCorrupted){
      this.game.time.events.add(Phaser.Timer.SECOND * 4,
        ()=>{
          if(this.game.device.desktop){
            if(!document.getElementById('error')){
              this.createErrorDiv(this.errorData);
            }
            this.game.LobbyProxy.lobbyProxy.SendMessage(this.game.LobbyProxy.lobbyProxy.LOBBY_MESSAGE_GO_HOME);
          }
          else{
            window.location.href = this.game.net.LobbyUrl || window.location.href;
          }

          this.load.reset(true, true);
          this.load.removeAll();
        }, this);
    }
    else if(this.assetsReady){
      this.state.start('Preload', true, false);
    }
  }

  shutdown (){
    this.load.onFileError.removeAll();
  }

  // if the game stops here the fsm has been created yet
  errorLoadingFile (key){
    this.loadingCorrupted = true;
    this.game.LobbyProxy.lobbyProxy.SendMessage(this.game.LobbyProxy.LOBBY_MESSAGES_ENUM.LOBBY_MESSAGE_GAME_ERROR);
    this.errorData = {ResponseCode:'-', ResponseMessage: 'Missing asset(' + key + ')'};
    console.log('ERooor Loadiiiing ');
  }

  createErrorDiv (data){
    console.log(data);
    var errorDiv = document.createElement('div');
    errorDiv.id = 'error';

    var errorCodeDiv = document.createElement('div');
    errorCodeDiv.id = 'error-code';
    errorCodeDiv.innerHTML = 'Code: ' + data.ResponseCode;
    errorDiv.appendChild(errorCodeDiv);

    var errorMessageDiv = document.createElement('div');
    errorMessageDiv.id = 'error-message';
    errorMessageDiv.innerHTML = 'Message: ' + data.ResponseMessage;
    errorDiv.appendChild(errorMessageDiv);

    document.body.appendChild(errorDiv);
  }
}
