/*
 * Preload state
 * ============================================================================
 *
 * This state comprises two purposes: Take care of loading the remaining
 * assets used within your game, including graphics and sound effects, while
 * displaying a busy splash screen, showing how much progress were made
 * during the asset load.
*/

import {HDR, HD, SD} from '../data/assets';

// To make matters easier, I prepared a SplashScreen class, responsible for
// displaying the decorated splash screen graphic, and the progress bar.
import SplashScreen from '../objects/SplashScreen';

export default class Preload extends Phaser.State {

  init () {
    //version of the game
    this.semVersion = document.getElementById('version').innerHTML;

    this.assetsReady = false;
    this.loadingAnimHasLooped = false;

    this.game.stage.disableVisibilityChange = true;

    this.game.resizeCallback = this.gameResize.bind(this);
    window.addEventListener('resize', this.game.resizeCallback);

    // Fix for iPhone not behaving properly
    if(this.game.device.iOS && this.game.net.medium != 'ds'){
      window.addEventListener('scroll', function () {
        // Do not scroll when keyboard is visible
        if (document.activeElement === document.body && window.scrollY > 0) {
          document.body.scrollTop = 0;
        }
      }, true);
    }
    if(!this.game.device.desktop && this.game.device.requestFullscreen != ''){
      this.game.input.onTap.add(()=>{
        if (!this.game.scale.isFullScreen){
          this.game.scale.startFullScreen(true);
          this.game.scale.setUserScale(window.screen.height/this.game.height, window.screen.height/this.game.height);
        }

      }, this);
    }
    this.showSplashScreen();
    this.gameResize();
  }

  preload () {
    console.info('PRELOAD->preload()');
    this.gameContainerGroup.alpha = 1;
    this.loadAssets();
    this.createImageCacheArray();
  }

  create () {
    this.assetsReady = true;
  }

  createImageCacheArray () {
    // Traverse current asset folder from assets.js
    // and find which assets are cacheable

    this.game.imagesToCache = [];

    let assetPack = null;

    switch(this.game.resolution_dir)
    {
    case 'HDR':
      {
        assetPack = HDR;
        console.info('PRELOAD:->Caching HDR!');
      } break;
    case 'HD':
      {
        assetPack = HD;
        console.info('PRELOAD:->Caching HD!');
      } break;
    case 'SD':
      {
        assetPack = SD;
        console.info('PRELOAD:->Caching SD!');
      } break;
    default:
      {
        console.error('PRELOAD-> You should not be here!!!');
      }
    }

    assetPack.game.forEach( (entry)=>{
      if (entry.hasOwnProperty('isCached') && entry.isCached == true) {
        // Found property isCached
        this.game.imagesToCache.push(entry.key);
      }
    });
  }

  shutdown (){
    this.load.onFileError.removeAll();
  }

  update () {
    // Wait until all sound effects have been decoded into memory.
    if (this.assetsReady && this.loadingAnimHasLooped) {

      if(this.loadingCorrupted){
        this.clearSplashScreen();

        this.timer = this.game.time.events.add(Phaser.Timer.SECOND * 4,
          ()=>{
            if(this.game.device.desktop){
              if(!document.getElementById('error')){
                this.createErrorDiv(this.errorData);
              }
              this.game.LobbyProxy.lobbyProxy.SendMessage(this.game.LobbyProxy.lobbyProxy.LOBBY_MESSAGE_GO_HOME);
            }
            else{
              window.location.href = this.game.net.LobbyUrl || window.location.href;
            }
            this.load.reset(true, true);
            this.load.removeAll();
          }, this);
      }
      else{
        this.state.start('Game', true, false, this.gameContainerGroup);
      }
    }
  }

  clearSplashScreen (){
    this.gameContainerGroup.visible = false;
  }

  showSplashScreen () {
    this.splash = new SplashScreen(this.game);
    this.gameContainerGroup = new Phaser.Group(this.game, this.game, 'SplashContainer', true);
    this.gameContainerGroup.alpha = 0;
    this.gameContainerGroup.add(this.splash);

    this.logo = new Phaser.Sprite(this.game, 0, 0, 'logo');
    this.logo.x = 290 * this.game.SCALE_FACTOR;
    this.gameContainerGroup.add(this.logo);

    this.loading = new Phaser.Sprite(this.game, 0, 0, 'loading');
    this.loading.x = 995 * this.game.SCALE_FACTOR;
    this.loading.y = 862 * this.game.SCALE_FACTOR;
    this.gameContainerGroup.add(this.loading);

    this.flareAnim = new Phaser.Sprite(this.game, 0, 0, 'flare_anim');
    this.flareAnim.animations.add('flare', null, 20, true);
    this.gameContainerGroup.add(this.flareAnim);
    this.flareAnim.play('flare');
    this.flareAnim.events.onAnimationLoop.add(()=>{
      this.loadingAnimHasLooped = true;
    });

    this.game.LobbyProxy.lobbyProxy.SendMessage(this.game.LobbyProxy.LOBBY_MESSAGES_ENUM.LOBBY_MESSAGE_SPLASH_SCREEN_LOADED);
  }

  loadAssets () {
    // Load game resolution dependent assets from
    // 'game' subsection of the appropriate res section,
    //  depending on previously selected resolution.
    var resolutionFolder;

    if(this.game.resolution_dir === 'HDR'){
      resolutionFolder = HDR;
      console.info('Preload->Load game HDR pack');
    }
    else if(this.game.resolution_dir === 'HD'){
      resolutionFolder = HD;
      console.info('Preload->Load game HD pack');
    }
    else if(this.game.resolution_dir === 'SD'){
      resolutionFolder = SD;
      console.info('Preload->Load game SD pack');
    }

    this.load.pack('game', null, resolutionFolder);

    if('game_' + this.game.api.config.localeID in resolutionFolder){
      this.load.pack('game_' + this.game.api.config.localeID, null, resolutionFolder);
    }
    else{//load defalult en-GB
      this.load.pack('game_' + 'en-GB', null, resolutionFolder);
    }

    this.load.onFileError.add(this.errorLoadingFile, this);

  }

  createErrorDiv (data){
    console.log(data);
    var errorDiv = document.createElement('div');
    errorDiv.id = 'error';

    var errorCodeDiv = document.createElement('div');
    errorCodeDiv.id = 'error-code';
    errorCodeDiv.innerHTML = 'Code: ' + data.ResponseCode;
    errorDiv.appendChild(errorCodeDiv);

    var errorMessageDiv = document.createElement('div');
    errorMessageDiv.id = 'error-message';
    errorMessageDiv.innerHTML = 'Message: ' + data.ResponseMessage;
    errorDiv.appendChild(errorMessageDiv);

    document.body.appendChild(errorDiv);
  }

  // if the game stops here the fsm has been created yet
  errorLoadingFile (key){
    this.loadingCorrupted = true;
    this.game.LobbyProxy.lobbyProxy.SendMessage(this.game.LobbyProxy.LOBBY_MESSAGES_ENUM.LOBBY_MESSAGE_GAME_ERROR);
    this.errorData = {ResponseCode:'-', ResponseMessage: 'Missing asset(' + key + ')'};
    console.log('ERooor Loadiiiing ');
  }


  // This resize function is the callback of browser's window resize event directly
  gameResize (){

    console.log('PRELOAD RESIZE!');
    if(!this.game.device.desktop && this.game.net.medium != 'ds'){
      this.checkOrientation();
    }

    var ratio;
    var width, height;

    if(this.game.device.iPhone4 && this.game.device.iOSVersion === 7 && this.game.net.medium != 'ds'){
      width = window.outerWidth;
      height = window.outerHeight;
      ratio = height / this.game.SCALE_INITIAL_HEIGHT;
    }
    else{
      ratio = window.innerHeight / this.game.SCALE_INITIAL_HEIGHT;
      if(this.game.device.iPhone && (this.game.device.iOSVersion === 9 || this.game.device.iOSVersion === 8)){
        width = document.documentElement.clientWidth;
        height = document.documentElement.clientHeight;
      }
      else{
        width = window.innerWidth;
        height = window.innerHeight;
      }

      ratio = height / this.game.SCALE_INITIAL_HEIGHT;
    }
    document.getElementById('Game').style.width = width + 'px';
    document.getElementById('Game').style.height = height + 'px';

    // display width smaller than game's
    if(this.game.SCALE_INITIAL_WIDTH * ratio > width){
      this.game.scale.setGameSize (width/ratio, this.game.SCALE_INITIAL_HEIGHT);
      //catch the case that is smaller than safe area
      if(width < this.game.SAFE_AREA_WIDTH * ratio){
        this.game.scale.setGameSize (this.game.SAFE_AREA_WIDTH, this.game.SCALE_INITIAL_HEIGHT);
      }
    }
    else{// display width bigger than game's
      this.game.scale.setGameSize (this.game.SCALE_INITIAL_WIDTH, this.game.SCALE_INITIAL_HEIGHT);
    }

    if(this.game.device.iPhone && (this.game.device.iOSVersion === 9 || this.game.device.iOSVersion === 8)){
      this._gameRePosition();
    }
    this._gameRePosition();
  }

  _gameRePosition (){
    this.gameContainerGroup.x = -Math.abs((this.game.width - this.gameContainerGroup.width)/2);
  }
  
  checkOrientation (){
    if(window.innerHeight< window.innerWidth){
      document.getElementById('orientation').style.display = 'none';
      if (this.game.paused) {
        this.game.paused = false;
      }
    }
    else{
      if (this.game.scale.isFullScreen){
        this.game.scale.stopFullScreen(true);
      }

      var orientationPanel = document.getElementById('orientation');
      orientationPanel.style.display = 'block';
      this.game.paused = true;
      console.log('FLIPPED!');

    }
  }

  clone (obj){
    var clone = {};
    clone.prototype = obj.prototype;
    for (var property in obj){
      clone[property] = obj[property];
    }
    return clone;
  }
}
