/*
 * PreBoot state
 * ===========================================================================
 *
 * The first state of the game, responsible for setting up some Phaser
 * features.
 * Also includes a Machina FSM for ticket request/connectivity handling
 */


import { COMMON } from '../data/assets';
import * as Machina from '../../../node_modules/machina/lib/machina';
import Config from '../api/Config';

export default class PreBoot extends Phaser.State {

  init () {
    console.info('PreBoot->init()');

    // The game never looses focus if player in another tab
    this.game.stage.disableVisibilityChange = true;

    this.CONF_REQUESTS = 4;

    this.isConfigRetry = false;

    this.semVersion = document.getElementById('version').innerHTML;

    //create object that will contain all API related objects such as config class
    //(for Get Gongiurations) and ticket calls
    this.game.api = [];

    //parse url's queryparameter
    var net = new Phaser.Net(this.game);
    this.game.net = net.getQueryString();
    this.game.device.iPhoneDevice = this.iPhoneVersion();

    //initialize LobbyProxy
    this.game.LobbyProxy = {};

    this.getServerConfig().then(()=>{
      // Create a new config object and attach it to game object
      var confObj = {
        GameID: this.CONFIG.GameID || null,
        OperatorID: this.CONFIG.OperatorID || null,
        LobbyID: this.CONFIG.LobbyID || null,
        LobbyDomain: this.CONFIG.LobbyDomain || null,
        Host: this.CONFIG.Host || null,
        Port: this.CONFIG.Port || null,
        UserToken: this.CONFIG.UserToken || null,
        APIVersionID: this.CONFIG.APIVersionID || null,
        GameLocaleID: this.CONFIG.GameLocaleID || null,
        GameMode: (this.CONFIG.GameMode !=null  && this.CONFIG.GameMode !=undefined) ? this.CONFIG.GameMode : null
      };
      // try catch if lobbyproxy lib is ncluded
      try{
        this.game.LobbyProxy.lobbyProxy = new LobbyProxy(confObj);
        this.game.LobbyProxy. LOBBY_MESSAGES_ENUM = LOBBY_MESSAGES_ENUM;

      }
      catch(e){
        this.loadingCorrupted = true;
        this.errorData = {
          ResponseCode: '-',
          ResponseMessage: 'Cannot locate LobbyProxy.js'
        };
      }
    })
    .catch(()=>{
      var confObj = {
        GameID: this.CONFIG.GameID || null,
        OperatorID: this.CONFIG.OperatorID || null,
        LobbyID: 'CANVAS',
        LobbyDomain: '*',
        Host: this.CONFIG.Host || null,
        Port: this.CONFIG.Port || null,
        UserToken: this.CONFIG.UserToken || null,
        APIVersionID: this.CONFIG.APIVersionID || null,
        GameLocaleID: this.CONFIG.GameLocaleID || null,
        GameMode: this.CONFIG.GameMode || null
      };
      // try catch if lobbyproxy lib is ncluded
      try{
        this.game.LobbyProxy.lobbyProxy = new LobbyProxy(confObj);
        this.game.LobbyProxy. LOBBY_MESSAGES_ENUM = LOBBY_MESSAGES_ENUM;

      }
      catch(e){
        this.loadingCorrupted = true;
        this.errorData = {
          ResponseCode: '-',
          ResponseMessage: 'Cannot locate LobbyProxy.js'
        };
      }
    });
  }

  preload () {
    console.info('PreBoot->preload()');
    // Load COMMON asset file
    this.load.pack('commonAssets', null, COMMON);
    this.load.onFileError.add(this.errorLoadingFile, this);
  }

  create () {
    console.info('PreBoot->create()');
    // Create a new config object and attach it to game object
    var initialState;
    if(this.loadingCorrupted){
      initialState = 'GOTOLOBBY';
    }
    else{
      initialState = 'CONFREQUEST';
    }

    let bootFSM = Machina.Fsm.extend({
      namespace: 'bootFsm',
      initialState: initialState,
      states:{
        CONFREQUEST:{
          _onEnter: ()=>{

            console.info('PreBoot->CONFREQUEST->_onEnter()');
            this.game.api.config = new Config(this.game);
            // Request game's configuration
            this.game.api.config.request(this.isConfigRetry)
              .then(()=> {
                this.game.LobbyProxy.lobbyProxy.SendMessage(this.game.LobbyProxy.LOBBY_MESSAGES_ENUM.LOBBY_MESSAGE_STATE_DOWNLOAD_GAME_CONFIG);
                this.bootFsm.handle('advancetoBoot');
              })
              .catch((errorData)=> {
                this.CONF_REQUESTS -= 1;

                this.errorData = errorData;

                if (this.CONF_REQUESTS <= 0 || !(errorData.ResponseCode == '' || errorData.ResponseCode == '408' || errorData.ResponseCode == '429')) {
                  this.bootFsm.handle('networkError');
                }
                else if (errorData.ResponseCode == '429') {
                  this.CONF_REQUESTS += (this.CONF_REQUESTS < 4) ? 1 : 0;
                  this.bootFsm.handle('failRequest');
                }
                else{
                  this.bootFsm.handle('failRequest');
                }
              });
          },
          advancetoBoot: ()=>{
            this.isConfigRetry = false;
            this.state.start('Boot', true, false);
          },
          failRequest: 'TRANSITIONTOCONFREQ',
          networkError: 'NETWORKERROR'
        },
        TRANSITIONTOCONFREQ:{
          _onEnter: ()=>{
            this.timer = setTimeout( ()=>{
              console.info('PreBoot->TRANSITIONTOCONFREQ->_onEnter()');
              this.isConfigRetry = true;
              this.bootFsm.handle('timeout');
            }, 5000 );
          },
          _onExit: ()=>{
            clearTimeout( this.timer );
          },
          // OUT State handling
          timeout: 'CONFREQUEST'
        },
        NETWORKERROR: {
          _onEnter: ()=>{
            console.info('PreBoot->NETWORKERROR->_onEnter()');
            // TODO(debugRemove)
            //create html
            this.createErrorDiv(this.errorData);
            this.timer = this.game.time.events.add(Phaser.Timer.SECOND * 4,
                    () => {
                      this.bootFsm.handle('timeout');
                    }, this);
          },
          timeout: 'GOTOLOBBY',
          _onExit: ()=>{
            this.game.time.events.remove(this.timer);
          }
        },
        GOTOLOBBY:{
          _onEnter:()=>{
            console.info('PreBoot->GOTOLOBBY->_onEnter()');
            this.game.time.events.add(Phaser.Timer.SECOND * 4,
              () => {
                if(this.game.device.desktop){
                  if(!document.getElementById('error')){
                    this.createErrorDiv(this.errorData);
                  }
                  this.game.LobbyProxy.lobbyProxy.SendMessage(this.game.LobbyProxy.LOBBY_MESSAGES_ENUM.LOBBY_MESSAGE_GO_HOME);
                }
                else{
                  window.location.href = this.game.net.LobbyUrl || window.location.href;
                }

                this.load.reset(true, true);
                this.load.removeAll();
              }, this);
          }
        }
      }
    });
    this.bootFsm = new bootFSM();
  }

  shutdown (){
    this.load.onFileError.removeAll();
  }

  // if the game stops here the fsm has been created yet
  errorLoadingFile (key){
    this.loadingCorrupted = true;
    this.game.LobbyProxy.lobbyProxy.SendMessage(this.game.LobbyProxy.LOBBY_MESSAGES_ENUM.LOBBY_MESSAGE_GAME_ERROR);
    this.errorData = {ResponseCode:'-', ResponseMessage: 'Missing asset(' + key + ')'};
    console.log('ERooor Loadiiiing ');
  }

  iPhoneVersion (){
    //portrait
    if(window.innerHeight > window.innerWidth){
      //iphone 6 && iphone 6S
      if(document.documentElement.clientWidth === 375 || document.documentElement.clientWidth === 414){
        return 'iPhone6';
      }
      else{
        return 'iPhone5';
      }
    }
    else{ //Landscape
      if(document.documentElement.clientWidth === 667 || document.documentElement.clientWidth === 736){
        return 'iPhone6';
      }
      else{
        return 'iPhone5';
      }
    }
  }

  createErrorDiv (data){
    console.log(data);
    var errorDiv = document.createElement('div');
    errorDiv.id = 'error';

    var errorCodeDiv = document.createElement('div');
    errorCodeDiv.id = 'error-code';
    errorCodeDiv.innerHTML = 'Code: ' + data.ResponseCode;
    errorDiv.appendChild(errorCodeDiv);

    var errorMessageDiv = document.createElement('div');
    errorMessageDiv.id = 'error-message';
    errorMessageDiv.innerHTML = 'Message: ' + data.ResponseMessage;
    errorDiv.appendChild(errorMessageDiv);

    document.body.appendChild(errorDiv);
  }

  getServerConfig (){
    var reqPromise = new Promise((resolve, reject)=>{

      var xhr = new XMLHttpRequest();
          // xhr.open('POST',
          //     url + '/api/iwg/v'+ this.CONFIG.APIVersionID+'/game/' + this.CONFIG.GameID);
      xhr.open('GET', 'config.json');
      xhr.addEventListener('load', ()=>{
        if (xhr.status === 200) {
          this.CONFIG = JSON.parse(xhr.responseText);
          resolve();
        }
        else {
          reject();
        }
      });

      xhr.onerror = ()=>{
        reject();
      };

      xhr.ontimeout = ()=>{
        reject();
      };
      xhr.send();
    });
    return reqPromise;
  }
}
