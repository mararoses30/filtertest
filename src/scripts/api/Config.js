/*
 * Config
 * ============================================================================
 *
 * Holds all configuration data needed per game
 */

import {HeaderData, DeviceData} from './ApiObjects';

class Config {

  constructor (game) {
    this.data = null;
    this.response = null;
    this.game = game;
    this.TIMEOUT = 60000;
    this.CONFIG = game.cache.getJSON('config');

    this.retryRequestID = null;
  }

  clearRetryRequestID () {
    this.retryRequestID = null;
  }

  /**
   * Makes a post request to serve in order to get configuration data
   * @param  {array} requestData Array of dictionaries with data eg.
   *  [Header, Device]
   * @return {Promise} Returns a promise for further handling
   */
  request (isRetry = false){
    var reqPromise;
    try {
      // If this is a retry then use stored request id
      // else sent null and HeaderData should use a rnd uuid
      let retryRequestID = isRetry ? this.retryRequestID : null;
      console.log('Config isRetry: ', isRetry, ' --> retryRequestID: ', retryRequestID);

      var headerData = new HeaderData(this.game, retryRequestID);
      this.game.LobbyProxy.lobbyProxy.SetHeader(headerData.Header);

      this.retryRequestID = headerData.Header.RequestID;
      console.log('---->', this.retryRequestID);

      var deviceData = new DeviceData(this.game);

      reqPromise = new Promise((resolve, reject)=>{

        var url = this.CONFIG.Host;
        if(this.CONFIG.Port){
          url += ':' + this.CONFIG.Port;
        }

        if(this.game.net.OperatorGameId){
          this.CONFIG.GameID = this.game.net.OperatorGameId;
        }

        var xhr = new XMLHttpRequest();
       // xhr.open('POST', url + '/api/iwg/v'+ this.CONFIG.APIVersionID+'/game/' + this.CONFIG.GameID);
         xhr.open('GET', 'offline/config_testLocale.json');
        xhr.addEventListener('load', ()=>{
          if (xhr.status === 200) {
            try{
              this.response = JSON.parse(xhr.responseText);
            }
            catch (e){
              reject({ResponseCode: '', ResponseMessage: e});
            }
            if(this.response.Header.ResponseCode === '200'){
              resolve();
            }
            else{
              reject({ResponseCode:this.response.Header.ResponseCode, ResponseMessage: this.response.Header.ResponseMessage});
            }
          }
          else {
            console.log('Request failed.  Returned status of ' + xhr.responseText);
            reject({ResponseCode:xhr.status, ResponseMessage: xhr.statusText});
          }
        });

        xhr.onerror = ()=>{
          reject({ResponseCode:'', ResponseMessage: 'Could not complete request (Possible lack of connectivity)'});
        };

        xhr.ontimeout = ()=>{
          console.log( xhr);
          reject({ResponseCode:'408', ResponseMessage: 'The server timed out waiting for the request.'});
        };
        xhr.timeout = this.TIMEOUT;
        xhr.send(JSON.stringify(this.merge_dicts([headerData, deviceData])));
      });
    }
    catch(err) {
      reqPromise = Promise.reject({ResponseCode:'', ResponseMessage: 'Could not complete request (Possible lack of connectivity)'});
    }

    return reqPromise;
  }

  get CData (){
    return this.response.Header.CData;
  }

  getWholeSettingFromKey (strKey) {
    for (let currSetting of this.response.Game.Settings) {
      if (currSetting.SettingKey === strKey) {
        return currSetting;
      }
    }
    console.warn('Could not find setting with given SettingKey!');
    return null;
  }
  
  /**
   * [Return the sumbolId as expected from RGS]
   * @param  {string} symbolName [Symbol name is the name of the symbol
   * as defined inside the game.Inside symbolId we map the RGS's symbol ids to
   * our local references ]
   *
   */
  get symbols (){
    var symbolArray = [];
    this.response.Game.Symbols.forEach((symbol, idx)=>{
      var symbolID = symbol.SymbolID;
      var entry = {};
      entry[symbolID] = {normal: 'Symbol_000'+idx, win:'Symbol_win_000'+idx};
      symbolArray.push(entry);
    });

    return symbolArray;
  }

  get hasAutoPlay (){
    return true;
  }

  get availableAutoplays (){
    return [1, 3, 5, 10];
  }

  get balance (){
    if (this.currencyPrecision === 0) {
      let truncedBalance = Math.trunc(this.response.Player.CurrentBalance/100) * 100;
      return truncedBalance / this.currencyDivisor;
    }
    else {
      return this.response.Player.CurrentBalance/this.currencyDivisor;
    }
  }

  get displayTicketId (){
    //TODO MARA: replace with the correct val from server
    //Boolean
    let showTicket=false;
    for (let setting of this.response.Game.Settings){
      if(setting.SettingName.toLowerCase()=='showticketid')
      {
        if(setting.SettingValue =='1')
          showTicket = true;
        else
          showTicket = false;

        break;
      }


    }
    return showTicket;
    //return this.response.Game.Configuration.DisplayTicketID;
  }

  get lobbyURL () {
    return this.response.Game.Configuration.LobbyURL;
  }

  get helpURL (){ 
    let helpEntry = this.getWholeSettingFromKey('HelpURL'); 
    return helpEntry.SettingValue; 
  } 
  
  get availableBets (){
    var availableBetsArray = [];
    for (let paytable of this.response.Game.PayTables){
      availableBetsArray.push(parseFloat(paytable.BetAmount)/this.currencyDivisor);
    }
    return availableBetsArray;
  }

  get locale_settings (){
    var format;
    switch (this.response.Game.Configuration.CurrencySymbolPosition){
    case '0':
      format = '%s %v'; break;
    case '1':
      format = '%v %s'; break;

    }
    return {
      code: this.response.Header.GameLocaleID,
      format: format,
      symbol: this.response.Game.Configuration.LocalizedCurrencySymbol,
      thousand: this.response.Game.Configuration.LocalizedThousandsSeparator,
      decimal : this.response.Game.Configuration.LocalizedDecimalSeparator
    };
  }

  get localeID (){
    return this.response.Header.GameLocaleID;
  }
  /**
   * [paytable description]
   * @param  {[type]} stake [description]
   * @return {object} Form://paytable has the form of :
   * {3: "1", 4: "2", 5: "10", 6: "50", 7: "this.currencyDivisor", 8: "5000", PayTableID: "00000001-0000-0000-0001-000000000000"}
   */
  paytable (stake){
     console.log(stake);
    var paytablePerWager = {};
    for (let paytable of this.response.Game.PayTables){
         console.log('paytable.BetAmount/100: ', paytable.BetAmount/this.currencyDivisor);
        // console.log('stake: ', stake);
      if(paytable.BetAmount/this.currencyDivisor == stake){
        paytable.Prizes.forEach((element) => {
          if(element.PrizeLines[0].RequiredQty != 0){ // we dont need 0 matches
            // console.log('-----------------');
            paytablePerWager[element.PrizeLines[0].RequiredQty] = element.WinAmount/this.currencyDivisor;
          }
        });
      }
    }
    console.log(paytablePerWager);
    return paytablePerWager;
  }

  get currencyDivisor (){
    return 100;
  }

  get currencyPrecision (){
    return (parseInt(this.response.Game.Configuration.LocalizedDecimalDigitsCount));
    // return 2;
  }

  paytableID (stake){
    for (let paytable of this.response.Game.PayTables){
      if(paytable.BetAmount === stake.toString()){
        return paytable.PayTableID;
      }
    }
  }
  /**
   * Merge into one obj an array of dictionaries
   * @param  {array} arrayOfDicts
   * @return {dictionary}
   */
  merge_dicts (arrayOfDicts){
    var dicts = arrayOfDicts || [];
    var dict = {};
    if(dicts){
      for (let obj of dicts){
        for (var property in obj) {
          if (obj.hasOwnProperty(property)) {
            dict[property] = obj[property];
          }
        }
      }
    }

    return dict;
  }
}

export default Config;
