/*
 * Result
 * ============================================================================
 *
 * Holds all configuration data needed per game
 */
import {HeaderData, WagerData} from './ApiObjects';

class Ticket {

  constructor (game) {
    this.response = null;
    this.game = game;
    this.TIMEOUT = 60000;
    this.CONFIG = game.cache.getJSON('config');

  	this.retryRequestID = null;
  }

  clearRetryRequestID () {
    this.retryRequestID = null;
  }

  /**
   * Makes a get request to serve in order to get configuration data
   * @param  {array} requestData Array of dictionaries with data eg.
   *  [Header, Device]
   * @return {Promise} Returns a promise for further handling
   */
  request (wagerObj, CData, isRetry = false){
    var reqPromise;
    try {
      reqPromise = new Promise((resolve, reject)=>{

        //created body request's objects
        // If this is a retry then use stored request id
        // else sent null and HeaderData should use a rnd uuid
        let retryRequestID = isRetry ? this.retryRequestID : null;
        console.log('Ticket isRetry: ', isRetry, ' --> retryRequestID: ', retryRequestID);
        var headerData = new HeaderData(this.game, retryRequestID);
        this.game.LobbyProxy.lobbyProxy.SetHeader(headerData.Header);
        headerData.Header.CData = CData || this.game.api.config.CData;

        // Store retryRequestID for retry
        this.retryRequestID = headerData.Header.RequestID;

        var wagerData = new WagerData(wagerObj);

        var url = this.CONFIG.Host;
        if(this.CONFIG.Port){
          url += ':' + this.CONFIG.Port;
        }

        var xhr = new XMLHttpRequest();
      //  xhr.open('POST',  url + '/api/iwg/v'+ this.CONFIG.APIVersionID+'/game/' + this.CONFIG.GameID + '/ticket');
      //   xhr.open('GET', 'offline/ticket_win.json');
        xhr.open('GET', 'offline/ticket_lose.json');
        xhr.addEventListener('load', ()=>{
          if (xhr.status === 200) {
            try{
              this.response = JSON.parse(xhr.responseText);
            }
            catch (e){
              reject({ResponseCode: '', ResponseMessage: e});
            }
            if(this.response.Header.ResponseCode === '200'){
              resolve();
            }
            else{
              reject({ResponseCode:this.response.Header.ResponseCode, ResponseMessage: this.response.Header.ResponseMessage});
            }
          }
          else {
            console.log('Request failed.  Returned status of ' + xhr.responseText);
            reject({ResponseCode:xhr.status, ResponseMessage: xhr.statusText});
          }
        });

        xhr.onerror = ()=>{
          reject({ResponseCode:'', ResponseMessage: 'Could not complete request (Possible lack of connectivity)'});
        };

        xhr.ontimeout = ()=>{
          console.log( xhr);
          reject({ResponseCode:'408', ResponseMessage: 'The server timed out waiting for the request.'});
        };

        xhr.timeout = this.TIMEOUT;
        xhr.send(JSON.stringify(this.merge_dicts([headerData, wagerData])));
      });
    }
    catch(err) {
      reqPromise = Promise.reject({ResponseCode:'', ResponseMessage: 'Could not complete request (Possible lack of connectivity)'});
    }

    return reqPromise;
  }

  get CData (){
    return this.response.Header.CData;
  }

  get isWinner (){
    if(this.response.Ticket.Actions[0].Type === '2'){//is winner
      return true;
    }
    else if(this.response.Ticket.Actions[0].Type === '1'){
      return false;
    }
  }

  get symbolDistribution (){
    return this.response.Ticket.Actions[0].SymbolDistributions[0].Symbols.map((el)=>{
      el.Revealed = 1;
      delete el.CData;
      return el;
    });
  }

  get symbols (){
    var symbolArray = [];

    for(let symbol of this.response.Ticket.Actions[0].SymbolDistributions[0].Symbols){
      var mapSymbol= this.findByKeyValue(this.game.api.config.symbols, symbol.SymbolID);

      var entry = {};
      // check if it is winner
      if(symbol.SymbolState === '1'){//normal
        entry.frame = mapSymbol[symbol.SymbolID].normal,
        entry.isWinner = false;
      }
      else if(symbol.SymbolState === '2'){//winner
        entry.frame = mapSymbol[symbol.SymbolID].win,
        entry.isWinner = true;
      }
      // check if it is revealed
      if(symbol.Revealed === '0'){//normal
        entry.isRevealed = false;
      }
      else if(symbol.Revealed === '1'){//winner
        //HACK in later versions this should bu set properly to true
        entry.isRevealed = false;
      }

      symbolArray.push(entry);
    }
    return symbolArray;
  }

  get initialBalance (){
    if (this.game.api.config.currencyPrecision === 0) {
      let truncedBalance = Math.trunc(this.response.Ticket.Actions[0].InitialBalance/100) * 100;
      return truncedBalance / this.currencyDivisor;
    }
    else {
      return this.response.Ticket.Actions[0].InitialBalance/this.game.api.config.currencyDivisor;
    }
  }

  get winAmount (){
    return this.response.Ticket.Actions[0].WinAmount /this.game.api.config.currencyDivisor;
  }

  get finalBalance (){
    if (this.game.api.config.currencyPrecision === 0) {
      let truncedBalance = Math.trunc(this.response.Ticket.Actions[0].FinalBalance/100) * 100;
      return truncedBalance / this.currencyDivisor;
    }
    else {
      return this.response.Ticket.Actions[0].FinalBalance/this.game.api.config.currencyDivisor;
    }
  }

  get ticketID (){
    return this.response.Ticket.TicketID;
  }

  get actionID (){
    return this.response.Ticket.Actions[0].ActionID;
  }
  /**
   * Merge into one obj an array of dictionaries
   * @param  {array} arrayOfDicts
   * @return {dictionary}
  */
  merge_dicts (arrayOfDicts){
    var dicts = arrayOfDicts || [];
    var dict = {};
    if(dicts){
      for (let obj of dicts){
        for (var property in obj) {
          if (obj.hasOwnProperty(property)) {
            dict[property] = obj[property];
          }
        }
      }
    }
    return dict;
  }

  findByKeyValue (obj, key, value){
    for (var i = 0; i < obj.length; i++) {
      if(value){
        if (obj[i][key] == value) {
          return i;
        }
      }
      else{
        if (obj[i][key]) {
          return obj[i];
        }
      }
    }
  }

}

export default Ticket;
