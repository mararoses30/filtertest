/*
 * Api Objects Classes
 * ============================================================================
 *
 * Holds all request objects that are attached to every request to the RGS
 */

class TicketData {
  constructor (ticketObj){
    this.Ticket = {};
    this.Ticket.TicketID = ticketObj.TicketID;
    this.Ticket.State = ticketObj.State;
    this.Ticket.ActionID = ticketObj.ActionID;
    this.Ticket.SymbolDistribution = ticketObj.SymbolDistribution;
  }
}

class WagerData {
  constructor (wagerObject){
    this.Wager = {};
    this.Wager.SymbolID = wagerObject.SymbolID;
    this.Wager.PayTableID = wagerObject.PayTableID;
    this.Wager.Amount = wagerObject.Amount;
    this.Wager.BetMultiplier = wagerObject.BetMultiplier;
    this.Wager.CData = wagerObject.CData;
  }
}

class HeaderData{
  constructor (game, retryRequestID=null){
    var CONFIG = game.cache.getJSON('config');
    this.Header = {};
    this.Header.APIVersionID = CONFIG.APIVersionID;

    if(game.net.UserToken){
      CONFIG.UserToken = game.net.UserToken;
    }
    if(game.net.GameMode){
      CONFIG.GameMode = game.net.GameMode;
    }
    if(game.net.GameLocaleId){
      CONFIG.GameLocaleID = game.net.GameLocaleId;
    }

    this.Header.GameID = CONFIG.GameID;
    this.Header.GameLocaleID = CONFIG.GameLocaleID;
    this.Header.GameMode = CONFIG.GameMode;
    this.Header.OperatorID = CONFIG.OperatorID;
    this.Header.UserToken = CONFIG.UserToken;

    if (retryRequestID) {
      this.Header.RequestID = retryRequestID;
    }
    else {
      this.Header.RequestID = game.rnd.uuid();
    }

    return {Header: this.Header};
  }
}

class DeviceData {
  constructor (game){
    this.Device = {};

    //Check the Device class
    var deviceClass, browser;

    if(game.device.iOS){
      deviceClass = 'iOS';
    }
    else if(game.device.desktop){
      deviceClass = 'PC';
    }
    else if(game.device.windowsPhone){
      deviceClass = 'WINDOWSPHONE';
    }
    else if(game.device.android){
      deviceClass = 'android';
    }
    else {
      deviceClass = 'PC';
    }

    // Detect browser
    if(game.device.chrome){
      browser = 'chrome';
    }
    else if(game.device.safari){
      browser = 'safari';
    }
    else if(game.device.firefox){
      browser = 'firefox';
    }
    else {
      browser = 'chrome';
    }
    this.Device.DeviceClass = deviceClass;
    this.Device.DeviceModel = 'Device.DeviceModel';
    this.Device.DeviceSDKVersion = 'Device.DeviceSDKVersion';
    this.Device.DeviceMobileBrowserName = browser;
    this.Device.DeviceScreenWidth = window.innerWidth;
    this.Device.DeviceScreenHeight = window.innerHeight;
    this.Device.DevicePixelRatio = game.device.pixelRatio;
    this.Device.CData = null;
  }
}

export {HeaderData, DeviceData, WagerData, TicketData};
