/*
 * Result
 * ============================================================================
 *
 * Holds all configuration data needed per game
 */
import {HeaderData, TicketData} from './ApiObjects';

class UpdateTicket {

  constructor (game) {
    this.response = null;
    this.game = game;
    this.TIMEOUT = 60000;
    this.CONFIG = game.cache.getJSON('config');

    this.retryRequestID = null;
  }

  clearRetryRequestID () {
    this.retryRequestID = null;
  }

  /**
   * Makes a get request to serve in order to get configuration data
   * @param  {array} requestData Array of dictionaries with data eg.
   *  [Header, Device]
   * @return {Promise} Returns a promise for further handling
   */
  request (ticketObj, CData, isRetry = false){
    var reqPromise;
    try {
      reqPromise = new Promise((resolve, reject)=>{

        //created body request's objects
        // If this is a retry then use stored request id
        // else sent null and HeaderData should use a rnd uuid
        let retryRequestID = isRetry ? this.retryRequestID : null;
        console.log('Update Ticket isRetry: ', isRetry, ' --> retryRequestID: ', retryRequestID);
        var headerData = new HeaderData(this.game, retryRequestID);
        this.game.LobbyProxy.lobbyProxy.SetHeader(headerData.Header);

        headerData.Header.CData = CData || this.game.api.config.CData;
        // Store retryRequestID for retry
        this.retryRequestID = headerData.Header.RequestID;

        var ticketData = new TicketData(ticketObj);

        var url = this.CONFIG.Host;
        if(this.CONFIG.Port){
          url += ':' + this.CONFIG.Port;
        }

        var xhr = new XMLHttpRequest();
        xhr.open('PUT', url + '/api/iwg/v'+ this.CONFIG.APIVersionID+'/ticket/' + ticketData.Ticket.TicketID);
        //  xhr.open('GET', 'assets/ticket.json');
       // xhr.open('GET', 'assets/updateTicket_testLocale.json');
        xhr.addEventListener('load', ()=>{
          if (xhr.status === 200) {
            try{
              this.response = JSON.parse(xhr.responseText);
            }
            catch (e){
              reject({ResponseCode: '', ResponseMessage: e});
            }
            if(this.response.Header.ResponseCode === '200'){
              resolve();
            }
            else{
              reject({ResponseCode:this.response.Header.ResponseCode, ResponseMessage: this.response.Header.ResponseMessage});
            }
          }
          else {
            console.log('Request failed.  Returned status of ' + xhr.responseText);
            reject({ResponseCode:xhr.status, ResponseMessage: xhr.statusText});
          }
        });

        xhr.onerror = ()=>{
          reject({ResponseCode:'', ResponseMessage: 'Could not complete request (Possible lack of connectivity)'});
        };

        xhr.ontimeout = ()=>{
          console.log( xhr);
          reject({ResponseCode:'408', ResponseMessage: 'The server timed out waiting for the request.'});
        };

        xhr.timeout = this.TIMEOUT;
        xhr.send(JSON.stringify(this.merge_dicts([headerData, ticketData])));
      });
    }
    catch(err) {
      reqPromise = Promise.reject({ResponseCode:'', ResponseMessage: 'Could not complete request (Possible lack of connectivity)'});
    }
    return reqPromise;
  }

  /**
   * Merge into one obj an array of dictionaries
   * @param  {array} arrayOfDicts
   * @return {dictionary}
  */
  merge_dicts (arrayOfDicts){
    var dicts = arrayOfDicts || [];
    var dict = {};
    if(dicts){
      for (let obj of dicts){
        for (var property in obj) {
          if (obj.hasOwnProperty(property)) {
            dict[property] = obj[property];
          }
        }
      }
    }
    return dict;
  }

  findByKeyValue (obj, key, value){
    for (var i = 0; i < obj.length; i++) {
      if(value){
        if (obj[i][key] == value) {
          return i;
        }
      }
      else{
        if (obj[i][key]) {
          return obj[i];
        }
      }
    }
  }

}

export default UpdateTicket;
