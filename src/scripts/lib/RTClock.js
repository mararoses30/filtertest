/*
 * SplashScreen
 * ============================================================================
 *
 * Shows a busy, decorated image, containing a widget displaying the resource
 * loading progress rate.
 */


class RTClock extends Phaser.Group {

  constructor (game, parent) {
    super(game, parent, 'Clock', false);
    var style = { font: 24 * game.SCALE_FACTOR + 'px Arial',
                  fill: '#fff',
                  };
    this.clockPos = this.game.cache.getJSON('layout')['layout']['clock'][0];
    this.timeText = new Phaser.Text(this.game,
                                this.clockPos.x,
                                this.clockPos.y,
                                '',
                                style);
    this.add(this.timeText);

    setInterval(this.updateTime.bind(this), 1000);
    this.timeString = '';
    this.secondsToggle = true;

  }

  updateTime (){
    var time = new Date();
    var hours = time.getHours();
    var minutes = time.getMinutes();

    if (hours < 10) {
      hours = '0' + hours;
    }
    if (minutes < 10) {
      minutes = '0' + minutes;
    }

    if(this.secondsToggle){
      this.timeString = hours + ':' + minutes;
    }
    else{
      this.timeString = hours + ' ' + minutes;
    }
    this.timeText.text = this.timeString;
    this.secondsToggle  = !this.secondsToggle;
  }
}


export default RTClock;
