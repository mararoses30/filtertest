/*
 * Sounds
 * ============================================================================
 *
 */

import Howler from 'howler';
import {AMBIENT} from '../data/assets';

class Sounds {
  constructor (game){
    this.game = game;

    this.isMute = false;

    this.ambientfx = new Howler.Howl({
      src: AMBIENT,
      autoplay: true,
      loop: true,
      volume: 0.5
    });

    //hack to bring object to Howler's format
    var sfx = JSON.stringify(this.game.cache.getJSON('sfx'));
    sfx = sfx.replace(/urls/g, 'src');
    sfx = sfx.replace(/static\//g, '');
    this.audioSfx = new Howler.Howl(JSON.parse(sfx));

    this.ambientfx.play();
  }

  play (sound) {
    this.audioSfx.play(sound);
  }

  stop (){
    this.audioSfx.stop();
  }

  disableSounds () {
    this.isMute = true;
    this.audioSfx.mute(true);
    this.ambientfx.mute(true);
  }

  enableSounds () {
    this.isMute = false;
    this.audioSfx.mute(false);
    this.ambientfx.mute(false);
  }

  destroySound (){
    this.audioSfx.unload();
    this.ambientfx.unload();
  }

  get mute (){
    return this.isMute;
  }

  set mute (value){
    this.isMute = value;
    this.audioSfx.mute(value);
    this.ambientfx.mute(value);
  }
}

export default Sounds;
