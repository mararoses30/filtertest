/*
 * Icon Text
 * ============================================================================
 *
 * This is the panel that displays Bet panel
 * It provides buttons to increase/decrease Bet Amount, Coin Value
 *
 */

export  class DynamicText extends Phaser.Text {
  constructor (game, text, x, y, width, height, style) {
    super(game, x, y, text, style);
    this.game = game;

    if(!style){
      style = {
        fontFamily: 'Arial',
        fontSize: 10,
        fontWeight: 'bold',
        fill: '#fff',
        offFill: '#5a615d',
        align: 'center',
        boundsAlignH: 'center',
        boundsAlignV: 'middle',
        padding: {left:0, right:0, top:0, bottom:0},
        shadowFill:true,
        shadowColor: '#000'
      };
    }

    this.style.padding.top = style.padding.top || 0;
    this.style.padding.bottom = style.padding.topbottom || 0;
    this.style.padding.left = style.padding.left || 0;
    this.style.padding.right = style.padding.right || 0;

    this.style = style;

    this.y = y + this.style.padding.top * this.game.SCALE_FACTOR;
    this.x = x + this.style.padding.left * this.game.SCALE_FACTOR;


    //caclulate the otimal size
    this.leftRightPadding = (this.style.padding.left + this.style.padding.right) * this.game.SCALE_FACTOR;
    this.topBottomPadding = (this.style.padding.top + this.style.padding.bottom) * this.game.SCALE_FACTOR;

    // check if shadow fill exists and apply
    if(this.style.shadowFill){
      this.setShadow(this.style.shadowOffsetX || 0, this.style.shadowOffsetY || 0,  this.style.shadowColor || '#000', this.style.shadowBlur || 30);
    }

    this.setTextBounds(0, 0, width - this.leftRightPadding, height - this.topBottomPadding);

    // calculate and find fontsize
    this.optimalFontSize = this.getOptimalFontSize(this.game, text, width -  this.leftRightPadding, height - this.topBottomPadding, this.style);
    this.style.fontSize = this.optimalFontSize;
    this.setStyle(this.style);
    // this.cacheAsBitmap = true;

  }

  getOptimalFontSize (game, text, width, height, style) {
    let txt;
    if (text instanceof Array) {
      txt = this.getLongestPhrase(text);
    } else if (typeof text === 'string') {
      txt = text;
    } else {
      throw new Error('Invalid argument: ' + JSON.stringify(text));
    }

    if (typeof style === 'undefined') {
      style = {};
    }

    let pixiText = new Phaser.Text(game, 0, 0, text, style);
    pixiText.fontSize = 5;

    if (pixiText.width > width || pixiText.height > height) {
      // Shrink
      while(pixiText.width > width || pixiText.height > height) {
        pixiText.fontSize -= 1;
      }
    } else {
      // Grow
      while(pixiText.width < width && pixiText.height < height) {
        pixiText.fontSize  += 1;
      }
      // Catch off-by-one errors
      if (pixiText.width > width || pixiText.height > height) {
        pixiText.fontSize -= 1;
      }
    }
    console.log('FontSize is : ' + pixiText.fontSize);
    return pixiText.fontSize;
  }

  getLongestPhrase (arrayOfPhrases) {
    let longestPhrase = '';
    arrayOfPhrases.forEach(function _findLongestPhrase (phrase) {
      if (phrase.length >= longestPhrase.length) {
        longestPhrase = phrase;
      }
    });
    return longestPhrase;
  }

  setText (value){
    // this.cacheAsBitmap = false;
    this.optimalFontSize = this.getOptimalFontSize(this.game, value, this.textBounds.width -  this.leftRightPadding, this.textBounds.height - this.topBottomPadding, this.style);
    this.style.fontSize = this.optimalFontSize;
    this.setStyle(this.style);
    Phaser.Text.prototype.setText.call(this, value);
    // this.cacheAsBitmap = true;
  }
}
