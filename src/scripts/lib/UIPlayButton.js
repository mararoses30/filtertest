/*
 * Play Button
 * ============================================================================
 *
 *
 */

import * as Machina from '../../../node_modules/machina/lib/machina';
import {TAtlasButton} from '../lib/buttons';

class PlayButton extends Phaser.Group {
  constructor (game, parent){
    super(game, parent, 'PlayButton', false);

    this.events = [];
    this.events.onPlaySelected = new Phaser.Signal();
    this.events.onStopSelected = new Phaser.Signal();

    this.buttonSpriteGroup = new Phaser.Group(game, this, 'buttonsSprite', true);
    this.add(this.buttonSpriteGroup);

    let FSM = Machina.Fsm.extend({
      namespace: 'playBtnFsm',
      initialState: 'INIT',

      states: {
        INIT: {
          _onEnter: ()=>{
            console.info('[Button]PlayButton->INIT->_onEnter()');
            //create autoplayPanel

            // create play button
            this.playBtn = new TAtlasButton(game, 0, 0, 'menu_ui', this.buttonPressed, this,  'SE 11', 'PlayButton_OVER', 'PlayButton_ON', 'PlayButton_OFF');
            this.buttonSpriteGroup.add(this.playBtn);
            // create play again button
            this.playAgainBtn = new TAtlasButton(game, 0, 0, 'menu_ui', this.buttonPressed, this,  'SE 11', 'PlayAgainButton_OVER', 'PlayAgainButton_ON', 'PlayAgainButton_OFF');
            this.buttonSpriteGroup.add(this.playAgainBtn);

            // create reveal button
            this.revealBtn = new TAtlasButton(game, 0, 0, 'menu_ui', this.buttonPressed, this,  'SE 17', 'RevealButton_OVER', 'RevealButton_ON', 'RevealButton_OFF');
            this.buttonSpriteGroup.add(this.revealBtn);

            // create reveal button
            this.stopBtn = new TAtlasButton(game, 0, 0, 'menu_ui', this.buttonPressed, this,  'SE 18', 'StopButton_OVER', 'StopButton_ON', 'StopButton_OFF');
            this.buttonSpriteGroup.add(this.stopBtn);

            // create nums left
            var fontsize = 60 * this.game.SCALE_FACTOR;
            var style = { font: fontsize + 'px Roboto-Bold',
                    fill: '#0A2D2D',
                    align: 'center', // the alignment of the text is independent of the bounds, try changing to 'center' or 'right'
                    boundsAlignH: 'center',
                    boundsAlignV: 'middle',
                    wordWrap: true,
                   };

            this.turnsLeftText = new Phaser.Text(this.game, 0, 0, 2, style);
            this.turnsLeftText.setShadow(2 * this.game.SCALE_FACTOR, 2 * this.game.SCALE_FACTOR, 'rgba(9,44,44,0.5)', 5 * this.game.SCALE_FACTOR);
            this.turnsLeftText.setTextBounds(this.playBtn._frame.spriteSourceSizeX, this.playBtn._frame.spriteSourceSizeY,  this.playBtn._frame.spriteSourceSizeW, this.playBtn._frame.spriteSourceSizeH );
            this.add(this.turnsLeftText);
          }
        },
        PLAY: {
          _onEnter: ()=>{
            console.info('[Button]PlayButton->PLAY->_onEnter()');
            this.turnsLeftText.visible = false;
            this.buttonSpriteGroup.setAll('visible', false);
            this.playBtn.visible = true;
            // [hack] .alpha = 0.001 is  for not flickering the reveal button's text
            this.revealBtn.alpha = 0.001;
            this.revealBtn.visible = true;
          },
          playagain: 'PLAYAGAIN'
        },
        PLAYTIMES: {
          _onEnter: ()=>{
            console.info('[Button]PlayButton->PLAY->_onEnter()');
            this.buttonSpriteGroup.setAll('visible', false);
            this.playBtn.visible = true;
            this.turnsLeftText.visible = true;
          },
          playagain: 'PLAYAGAIN'
        },
        PLAYAGAIN: {
          _onEnter: ()=>{
            console.info('[Button]PlayButton->PLAYAGAIN->_onEnter()');
            this.buttonSpriteGroup.setAll('visible', false);
            this.playAgainBtn.visible = true;
          },
          play: 'PLAY'
        },
        REVEAL:{
          _onEnter: ()=>{
            console.info('[Button]PlayButton->REVEAL->_onEnter()');
            this.buttonSpriteGroup.setAll('visible', false);
            // [hack] .alpha = 1 is  for not flickering the reveal button's text
            this.revealBtn.alpha =1;
            this.revealBtn.visible = true;
          }
        },
        STOP: {
          _onEnter: ()=>{
            console.info('[Button]PlayButton->STOP->_onEnter()');
            this.buttonSpriteGroup.setAll('visible', false);
            this.stopBtn.visible = true;
            this.turnsLeftText.visible = true;
          }
        }
      },
    });

    this.playBtnFsm = new FSM();
    this.playBtnFsm.transition('PLAY');
  }

  buttonPressed (){
    if(this.playBtnFsm.state != 'STOP'){
      console.info('[Button]PlayButton --> playPressed');
      this.events.onPlaySelected.dispatch(this);
    }
    else{
      console.info('[Button]PlayButton --> stopPressed');
      this.events.onStopSelected.dispatch(this);
    }
  }

  set state (value){
    this.playBtnFsm.transition(value);
  }

  get state (){
    return this.playBtnFsm.state;
  }

  disable (){
    this.buttonSpriteGroup.callAll('disable');
  }

  enable (){
    this.buttonSpriteGroup.callAll('enable');
  }

  set turnsLeft (value){
    this.turnsLeftText.setText(value);
  }
}

export default PlayButton;
