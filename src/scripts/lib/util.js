/*
 * Utility Class that provides handy functions
 * ============================================================================
 *
 *
 */

class Util {

  getDevicePixelRatio () {
    let canvas = document.createElement('canvas');
    let ctx = canvas.getContext('2d');
    let devicePixelRatio = window.devicePixelRatio || 1;
    let backingStoreRatio = ctx.webkitBackingStorePixelRatio ||
                    ctx.mozBackingStorePixelRatio ||
                    ctx.msBackingStorePixelRatio ||
                    ctx.oBackingStorePixelRatio ||
                    ctx.backingStorePixelRatio || 1;

    let uaString = navigator.userAgent;
    if (uaString.match('Lumia 920') || uaString.match('Lumia 928') || uaString.match('Lumia 1020')) {
      devicePixelRatio = 2.5;
    }
    return (devicePixelRatio !== backingStoreRatio) ? devicePixelRatio / backingStoreRatio : 1;
  }

  calculateGameScalingFactor () {
    let maxWidth = Phaser.Desktop ? 1920 : 960,
      maxHeight =  Phaser.Desktop ? 1080 : 540,
      vWidth = window.innerWidth,
      vHeight = window.innerHeight;

    if (maxWidth / vWidth > maxHeight / vHeight) {
      return vWidth / maxWidth;
    }
    return vHeight / maxHeight;
  }


  choice (array, howMany) {
    if (!(array instanceof Array)) {
      throw new TypeError('Invalid argument');
    }
    if (typeof howMany === 'undefined') {
      howMany = 1;
    }
    howMany = Math.max(Math.min(howMany, array.length), 1);
    array = this.shuffle(array);
    if (howMany === 1) {
      return array[0];
    } else {
      return array.slice(0, howMany);
    }
  }

  shuffle (array) {
    if (!(array instanceof Array)) {
      throw new TypeError('Invalid argument');
    }

    let currentIndex = array.length, temporaryValue, randomIndex;

    while (currentIndex !== 0) {

      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  }

  getQueryletiable (letiable) {
    let query = window.location.search.substring(1);
    let lets = query.split('&');
    for (let i = 0; i < lets.length; i++) {
      let pair = lets[i].split('=');

      if (decodeURIComponent(pair[0]) === letiable) {
        return decodeURIComponent(pair[1]);
      }
    }
  }

  getBaseURL () {
    let baseURL = window.location.origin || [
      window.location.protocol,
      '//',
      window.location.hostname,
      ':',
      window.location.port
    ].join();

    return baseURL;
  }

  throttle (callback, delay, context) {
    let ctx = context || window;
    let lastCall = Date.now();
    return ()=> {
      let now = Date.now();
      if (now - delay > lastCall) {
        lastCall = now;
        let args = Array.prototype.slice.call(arguments);
        callback.apply(ctx, args);
      }
    };
  }

  getLongestPhrase (arrayOfPhrases) {
    let longestPhrase = '';
    arrayOfPhrases.forEach(function _findLongestPhrase (phrase) {
      if (phrase.length >= longestPhrase.length) {
        longestPhrase = phrase;
      }
    });
    return longestPhrase;
  }

  getOptimalFontSize (game, text, width, height, style) {
    let txt;
    if (text instanceof Array) {
      txt = this.getLongestPhrase(text);
    } else if (typeof text === 'string') {
      txt = text;
    } else {
      throw new Error('Invalid argument: ' + JSON.stringify(text));
    }

    if (typeof style === 'undefined') {
      style = {};
    }

    let phaserText = new Phaser.Text(
        game,
        0, 0,
        txt,
        style
    );
    if (style.font) {
      let items = style.font.split(' ');
      let font = items[items.length - 1];
      phaserText.font = font;
    }

    if (phaserText.width > width || phaserText.height > height) {
      // Shrink
      while(phaserText.width > width || phaserText.height > height) {
        phaserText.fontSize -= 1;
      }
    } else {
      // Grow
      while(phaserText.width < width && phaserText.height < height) {
        phaserText.fontSize  += 1;
      }
      // Catch off-by-one errors
      if (phaserText.width > width || phaserText.height > height) {
        phaserText.fontSize -= 1;
      }
    }

    return phaserText.fontSize;
  }

  makeTranslucentRect (game, width, height, color) {

    if (typeof color === 'undefined') {
      color = 'rgba(255, 0, 0, 0.5)';
    }

    let canvas = document.createElement('canvas');
    canvas.width = width;
    canvas.height = height;
    let ctx = canvas.getContext('2d');
    ctx.fillStyle = color;
    ctx.rect(0, 0, canvas.width, canvas.height);
    ctx.fill();

    let sprite = new Phaser.Sprite(
        game,
        0, 0,
        new PIXI.Texture(new PIXI.BaseTexture(canvas))
    );

    return sprite;
  }

  screenToLocalCoords (point, toSystem) {
    return {
      x: point.x - toSystem.x,
      y: -point.y  + toSystem.y,
    };
  }

  localToScreenCoords (point, localSystem) {
    return {
      x: point.x + localSystem.x,
      y: localSystem.y - point.y
    };
  }

  vectorLength (vector) {
    return Math.sqrt(vector.x * vector.x + vector.y * vector.y);
  }

  vectorRadians (vector) {
    return Math.atan2(vector.y, vector.x);
  }

  vectorFromRadians (radians, length) {
    return {
      x: Math.cos(radians) * length,
      y: Math.sin(radians) * length
    };
  }
  /**
   * [getCenterPoint description]
   * @param  {gameObj} thie game object
   * @param  {string} atlas json file
   * @param  {string} frame frame within atlas
   * @return {{x:, y:}}  Returns center point of image in game
   */
  getBounds (game, atlas, frame){
    var frameData = game.cache.getFrameData(atlas);
    var posX = frameData.getFrameByName(frame).spriteSourceSizeX;
    var posY = frameData.getFrameByName(frame).spriteSourceSizeY;
    var W = frameData.getFrameByName(frame).spriteSourceSizeW;
    var H = frameData.getFrameByName(frame).spriteSourceSizeH;

    return {x: posX, y: posY, w: W, h: H};
  }

}

export default Util;
