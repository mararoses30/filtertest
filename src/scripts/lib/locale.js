/*
 * Localization and currency module
 * ============================================================================
 *
 *
 */

import * as accounting from '../../../node_modules/accounting';

const local_settings = {
  code: 'en-US',
  currency: {
    format: '%v %s',
    symbol: '$',
    thousand: '.',
    decimal: ','
  }
};

class Locale {

  constructor (game, settings) {
    // choose local_settings if no settings defined

    this.game = game;

    this.settings = settings ? settings : local_settings;
    // this.translations = game.cache.getJSON('langs');

    this._tokenMap = null;
  }

  /*
  * @param {string} translation key
  * @return {string} translation value
  */

  get (key) {
    // return this.translations[key][this.settings.code];
    return this.translations[key];
  }
  /**
   * [formatCurrency description]
   * @param  {number} number    [description]
   * @param  {integer} precision [Number of digits after decimal mark]
   * @return {[string]}           [formated number as currency]
   */
  formatCurrency (number, precision) {
    this.settings.precision = precision;
    return accounting.formatMoney(number, this.settings);
  }

  getCurrency () {
    return this.settings.currency.symbol;
  }

  formatNumber (number) {
    return accounting.formatNumber(number, this.settings);
  }

  // Create TokenMap 
  get translations () {

    if (!this._tokenMap) {
      this._tokenMap = {};
      this.parseTokenMap(this.settings.code);
    }

    return this._tokenMap;
  }

  parseTokenMap (localeID) {
    console.log('Start creation of TokenMap');

    this.resourceData = null;

    if(false){
    //if (this.game.api.config.response.Game.Resources) {
      this.resourceData = this.game.api.config.response.Game.Resources;
    }
    else {
      console.warn('Locale::parseTokenMap() => Can\'t find Resources Section in Config response!');
      console.warn('Using offline/langs.json');



      let xhr = new XMLHttpRequest();
      xhr.open('GET', 'offline/langs/langs.json', false);
      xhr.send(null);  

      if (xhr.status === 200) {
        try {
          this.resourceData = JSON.parse(xhr.responseText);
          console.log(this.resourceData);
        }
        catch (e) {
          console.warn('Could not GET offline/langs.json');
        }
      }

      console.log('THEN');
      console.log(this.resourceData);
    }

    // At this point we have acquired a resourceData Object either 
    // through Config from RGS or by loading an offline langs.json file

    if (this.resourceData) {
      let resourceSection = null;

      console.log(this.settings.code);

      this.resourceData.forEach((resourceEntry) => {
        if (resourceEntry.Locale.toUpperCase() === localeID.toUpperCase()) {
          resourceSection = resourceEntry;
        }
      });

      // LOCALE NOT FOUND 
      if (!resourceSection) {
        console.warn('Locale::parseTokenMap() => Can\'t find Locale: ', localeID, ' in Resources of Config!');
        return;
      }

      // Create the TokenMap 
      resourceSection.Tokens.forEach((ResourceTokenEntry) => {
        // Using Object.defineProperty makes entries immutable 
        Object.defineProperty(this._tokenMap, ResourceTokenEntry.TokenID,
            { value: ResourceTokenEntry.Text });

        // Debug log 
        console.log('TokenMap: Added tokenID: [', ResourceTokenEntry.TokenID,
          '] with value: [', this._tokenMap[ResourceTokenEntry.TokenID], ']');
      });

      console.log('Created TokenMap: ', this._tokenMap);
    }
    else {
      console.warn('locale::parseTokenMap() no resourceData found');
    }

    console.log('End parseTokenMap');
    

  }
}
export default Locale;
