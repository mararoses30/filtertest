import {DynamicText} from '../lib/DynamicText';

export class DynamicCompositeText  extends Phaser.Group {
  constructor (game, parent, str, arrayofSubs, x, y, width, height, style, scale=1, anchorY=0, anchorX=0){
    super(game, parent, 'HelpText', false);
    // this.debugTextBounds(x, y, width, height);
    this.game = game;
    this.arrayofSubs = arrayofSubs;
    this.text = str;

    this.positionY = y;
    this.boundX = x;
    this.boundY = y;
    this.boundWidth = width;
    this.boundHeight = height;
    this._scale = scale;
    this._anchorY = anchorY;
    this._anchorX = anchorX;

    if(!style){
      style = {
        fontFamily: 'Arial',
        fontSize: 30,
        fontWeight: 'bold',
        fill: '#fff',
        offFill: '#5a615d',
        align: 'left',
        boundsAlignH: 'left',
        boundsAlignV: 'middle',
        margin: {left:10 * this.game.SCALE_FACTOR},
        padding:{x:0, y:20 * this.game.SCALE_FACTOR}
      };
    }
    this.style = style;

    this.textContainer = new Phaser.Group(game, this, 'textContainer', true);
    this.add(this.textContainer);

    // split text
    // get keys to substitute with images
    this.subst_keys = [];

    arrayofSubs.forEach((entry)=>{
      this.subst_keys.push(Object.keys(entry)[0]);
    });

    // this.split_text = this.decomposeText(this.text, this.subst_keys);
    this.split_text = this.decomposeText1(this.text);

    // variables for relativr positioning
    this.initXPos = x + this.style.margin.left;
    this.initYPos = y;
    this.currentXPos = x + this.style.margin.left;
    this.currentYPos = y;
    this.currentHeight = 0;

    this.createText().then(()=>{
      this.textContainer.y =  this.textContainer.y +  this.positionY - this.textContainer.getBounds().y;
    });

  }


  createText (){
    var reqPromise = new Promise((resolve)=>{
      // Is a text without images
      if(this.split_text.length === 0){
        let partial_text = new Phaser.Text(this.game, this.currentXPos, this.positionY, this.text, this.style);
        partial_text.padding.y = this.style.padding.y;
        this.currentXPos += partial_text.width;
        // partial_text.setTextBounds( this.currentXPos, this.positionY, this.positionWidth -this.currentXPos , this.positionHeight);
        this.textContainer.add(partial_text);
        resolve();
      }
      // Is a text with images
      else{
        var lineGroup = new Phaser.Group(this.game, this, 'LineText', true);
        var idx = 0;
        this.textContainer.addChild(lineGroup);

        this.split_text.forEach((element, txtIdx)=>{
          //subtext is not to be replaced by image
          if( this.subst_keys.indexOf(element) === -1){

            var partial_text = new Phaser.Text(this.game, this.currentXPos, this.currentYPos, element + ' ', this.style);
            partial_text.padding.y = this.style.padding.y;

            if (partial_text.x + partial_text.width > this.boundX + this.boundWidth){
              lineGroup = new Phaser.Group(this.game, this, 'LineText', true);
              this.textContainer.addChild(lineGroup);
              idx += 1;

              // this.currentYPos = this.textContainer.children[idx].y + this.textContainer.children[idx].height ;
              this.currentYPos = this.currentYPos + this.currentHeight;
              // add a tab
              partial_text.text = '\t\t\t\t' + partial_text.text;
              partial_text.x = this.initXPos;
              partial_text.y = this.currentYPos;
              this.currentXPos = partial_text.x + partial_text.width;
            }
            else{
              partial_text.x = this.currentXPos;
              partial_text.y = this.currentYPos;

              this.currentXPos = partial_text.x + partial_text.width;
            }
            this.text_height = partial_text.height;
            this.textContainer.children[idx].add(partial_text);
            this.currentHeight = this.textContainer.children[idx].height;
          }
          else{//image
            this.arrayofSubs.forEach((keyImagObj)=>{
              var sprite;
              if(element === Object.keys(keyImagObj)[0]){
                let atlas = keyImagObj[element].atlas;
                let frame = keyImagObj[element].frame;

                sprite = new Phaser.Sprite(this.game, this.currentXPos, this.currentYPos, atlas, frame);

                 //check if image has text
                if(keyImagObj[element].text){
                  var boundBox;
                  if(keyImagObj[element].boundBox){
                    boundBox = keyImagObj[element].boundBox;
                  }
                  else{
                    boundBox = {x:0, y:0, width:sprite.width, height: sprite.height};
                  }
                  let imageText = new DynamicText(this.game,
                    keyImagObj[element].text,
                    boundBox.x,
                    boundBox.y,
                    boundBox.width,
                    boundBox.height,
                    keyImagObj[element].style
                  );

                  // imageText.anchor.y = 0.5;
                
                  sprite.addChild(imageText);
                }

                if (sprite.x + sprite.width > this.boundX + this.boundWidth){
                  lineGroup = new Phaser.Group(this.game, this, 'LineText', true);
                  this.textContainer.addChild(lineGroup);
                  idx += 1;

                  sprite.x = this.initXPos;
                  sprite.y = this.currentYPos + this.textContainer.children[idx-1].height + sprite.height/2 - this.style.padding.y;
                  sprite.y =  sprite.y - sprite.height/2;

                  this.currentXPos = sprite.x + sprite.width + 10 * this.game.SCALE_FACTOR;
                  this.currentYPos = sprite.y + sprite.height/2 - this.text_height/2 + this.style.padding.y;

                }
                else{
                  sprite.x = this.currentXPos;
                  sprite.y = this.currentYPos;

                  this.currentXPos = sprite.x + sprite.width + 10 * this.game.SCALE_FACTOR;
                  sprite.y =  sprite.y - sprite.height/2 + (this.text_height - this.style.padding.y)/2;

                }
                sprite.anchor.x = this._anchorX;
                sprite.anchor.y = this._anchorY;
                sprite.scale.setTo(this._scale, this._scale);

                this.textContainer.children[idx].add(sprite);
              }
            });
          }

          if(txtIdx === this.split_text.length - 1){
            resolve();
          }
        });

      }
    });
    return reqPromise;
  }
  decomposeText1 (text){
    var final_array = [];
    var tmp = text.split(' ');
    tmp.forEach((substr)=>{
      final_array.push(substr);
    });
    return final_array;
  }

  decomposeText (str, arrayOfImages){
    var array = [];
    var text = str;

    for(var i=0; i<arrayOfImages.length; i++){
      if(i !=arrayOfImages.length-1 ){
        let res = text.split(arrayOfImages[i]);
        array.push(res[0]);
        array.push(arrayOfImages[i]);
        text = res[1];
      }
      else{
        let res = text.split(arrayOfImages[i]);
        array.push(res[0]);
        array.push(arrayOfImages[i]);
        array.push(res[1]);
      }
    }
    return array;
  }

  debugTextBounds (x, y, w, h){
    // make debug rectangle to show the text's bounds
    this.debugRectangle = this.game.add.graphics(0, 0);
    this.debugRectangle.beginFill(0x1cc81c);
    this.debugRectangle.drawRect(0, 0, w, h);

    this.addChild(this.debugRectangle);

    var debugSprite = new Phaser.Sprite(this.game, x, y);
    debugSprite.addChild(this.debugRectangle);
    debugSprite.visible = true;
    debugSprite.alpha = 0.4;

    this.addChild(debugSprite);
  }

  getOptimalFontSize (game, text, width, height, style) {
    let txt;
    if (text instanceof Array) {
      txt = this.getLongestPhrase(text);
    } else if (typeof text === 'string') {
      txt = text;
    } else {
      throw new Error('Invalid argument: ' + JSON.stringify(text));
    }

    if (typeof style === 'undefined') {
      style = {};
    }

    let pixiText = new Phaser.Text(game, 0, 0, txt, style);
    if (style.font) {
      let items = style.font.split(' ');
      let font = items[items.length - 1];
      pixiText.style.font = font;
    }

    if(!pixiText.style.fontSize){
      var fontSize = pixiText.fontSize.split('pt');
      pixiText.fontSize = fontSize[0];
    }

    if (pixiText.width > width || pixiText.height > height) {
      // Shrink
      while(pixiText.width > width || pixiText.height > height) {
        pixiText.fontSize -= 1;
      }
    } else {
      // Grow
      while(pixiText.width < width && pixiText.height < height) {
        pixiText.fontSize  += 1;
      }
      // Catch off-by-one errors
      if (pixiText.width > width || pixiText.height > height) {
        pixiText.fontSize -= 1;
      }
    }
    console.log('FontSize is : ' + pixiText.fontSize);
    return pixiText.fontSize;
  }

  getLongestPhrase (arrayOfPhrases) {
    let longestPhrase = '';
    arrayOfPhrases.forEach(function _findLongestPhrase (phrase) {
      if (phrase.length >= longestPhrase.length) {
        longestPhrase = phrase;
      }
    });
    return longestPhrase;
  }
}
