/*
 * Play Button
 * ============================================================================
 *
 *
 */

import * as Machina from '../../../node_modules/machina/lib/machina';
import {TAtlasButton} from '../lib/buttons';
import {DynamicText} from '../lib/DynamicText';

class UIAutoPlayPanel extends Phaser.Group {
  constructor (game, parent, availableAutoplays){
    super(game, parent, 'AutoPlayPanel', false);

    // the number of availabe plays allowed by theRGS
    this.availableAutoplays = availableAutoplays;

    // Initial and final position for tween
    this.INITIAL_Y = -10 * this.game.SCALE_FACTOR;
    this.FINAL_Y = -160 * this.game.SCALE_FACTOR;

    this.events = [];
    // when autoplaypanel is tapped
    this.events.onAutoPlayClicked = new Phaser.Signal();
    // when autoplaypanel' minus button  is tapped
    this.events.onAutoPlayMinusClicked = new Phaser.Signal();
    // when autoplaypanel' plus button  is tapped
    this.events.onAutoPlayPlusClicked = new Phaser.Signal();


    this.buttonSpriteGroup = new Phaser.Group(game, this, 'buttonsSprite', true);
    this.add(this.buttonSpriteGroup);

    this.easingFunctions = {
      'Phaser.Easing.Linear.None' : Phaser.Easing.Linear.None,
      'Phaser.Easing.Elastic.In': Phaser.Easing.Elastic.In,
      'Phaser.Easing.Elastic.Out': Phaser.Easing.Elastic.Out,
      'Phaser.Easing.Elastic.InOut': Phaser.Easing.Elastic.InOut
    };

    // current autoplay
    this._currentAutoplayChosen = this.availableAutoplays[0];

    this.current_MAX = Math.max.apply(null, this.availableAutoplays);
    this.current_MIN = Math.min.apply(null, this.availableAutoplays);

     // Boolean isOpen
    this.isOpen = false;

    let FSM = Machina.Fsm.extend({
      namespace: 'autoplayFsm',
      initialState: 'INIT',

      states: {
        INIT: {
          _onEnter: ()=>{
            console.info('PlayButton->INIT->_onEnter()');
            //create autoplayPanel
            this.barButton = new TAtlasButton(game, 0, 0, 'menu_ui', this.barPressed, this,  'SE 14', 'AutoPlay_Bar_OVER', 'AutoPlay_Bar_ON', 'AutoPlay_Bar_OFF');
            // this.barButton.inputEnabled = false;
            this.buttonSpriteGroup.add(this.barButton);

            // create play button
            this.barButtonPlus = new TAtlasButton(game, 0, 0, 'menu_ui', this.plusPressed, this,  'SE 15', 'AutoPlay_Plus_OVER', 'AutoPlay_Plus_ON', 'AutoPlay_Plus_OFF');
            this.buttonSpriteGroup.add(this.barButtonPlus);
            this.barButtonPlus.alpha = 0;

            this.barButtonMinus = new TAtlasButton(game, 0, 0, 'menu_ui', this.minusPressed, this,  'SE 15', 'AutoPlay_Minus_OVER', 'AutoPlay_Minus_ON', 'AutoPlay_Minus_OFF');
            this.buttonSpriteGroup.add(this.barButtonMinus);
            this.barButtonMinus.alpha = 0;

            // Autoplay btn panel
            this.autoplayTimesPanelTextStyle = {
              fontFamily: 'Arial',
              fontSize: 10,
              fontWeight: 'bold',
              fill: '#525252',
              overFill: '#8f8524',
              offFill: '#141414',
              align: 'center',
              boundsAlignH: 'center',
              boundsAlignV: 'middle',
              padding: {left:0, right:0, top:0, bottom:0},
              wordWrap: true,
              stroke: '#000000',
              overStroke: '#5a5417',
              strokeThickness: 1
            };
            // Add button to close help screen
            var autoplayTimesPanelTextDimension = game.cache.getJSON('layout')['layout']['autoplayTimesPanelText'][0];
            this.autoplayTimesPanelText = new DynamicText(this.game,
                                        this.game.locale.get('g_autoplay_panel_text'),
                                        autoplayTimesPanelTextDimension.x,
                                        autoplayTimesPanelTextDimension.y,
                                        autoplayTimesPanelTextDimension.w,
                                        autoplayTimesPanelTextDimension.h,
                                        this.clone(this.autoplayTimesPanelTextStyle));

            this.autoplayTimesPanelText.events.onInputOver.add(this.onInputOver.bind(this));
            this.autoplayTimesPanelText.events.onInputDown.add(this.onInputDown.bind(this));
            this.autoplayTimesPanelText.events.onInputOut.add(this.onInputOut.bind(this));
            this.add(this.autoplayTimesPanelText);

            // create nums left
            var fontsize = 70 * this.game.SCALE_FACTOR;
            var style = { font: fontsize + 'px Roboto',
                    fill: '#000',
                    align: 'center', // the alignment of the text is independent of the bounds, try changing to 'center' or 'right'
                    boundsAlignH: 'center',
                    boundsAlignV: 'middle',
                    wordWrap: true,
                   };

            this.autoPlayTimesText = new Phaser.Text(this.game, 0, 0, this._currentAutoplayChosen, style);
            this.autoPlayTimesText.setShadow(2 * this.game.SCALE_FACTOR, 2 * this.game.SCALE_FACTOR, 'rgba(70,70,70,0.5)', 5 * this.game.SCALE_FACTOR);
            this.autoPlayTimesText.setTextBounds(this.barButton._frame.spriteSourceSizeX, this.barButton._frame.spriteSourceSizeY,  this.barButton._frame.spriteSourceSizeW, this.barButton._frame.spriteSourceSizeH * 0.8);
            this.add(this.autoPlayTimesText);
          }
        },
        OPEN: {
          _onEnter: ()=>{
            console.info('[Button]AutoPlayPanel->OPEN->_onEnter()');
            this.barButton.enable();
            this.y = this.FINAL_Y;
            this.barButtonPlus.visible = true;
            this.barButtonMinus.visible = true;
            this.updateButtons();
          },
          plusClicked: ()=>{
            var num = this.availableAutoplays.shift();
            this.availableAutoplays.push(num);
            this.autoPlayTimesChosen = this.availableAutoplays[0];
            this.events.onAutoPlayPlusClicked.dispatch(this);
            this.updateButtons();
          },
          minusClicked: ()=>{
            var num = this.availableAutoplays.pop();
            this.availableAutoplays.unshift(num);
            this.autoPlayTimesChosen = this.availableAutoplays[0];
            this.events.onAutoPlayMinusClicked.dispatch(this);
            this.updateButtons();
          },
          close: ()=>{
            this.barButton.inputEnabled = false;
            this.barButtonPlus.alpha = 0;
            this.barButtonMinus.alpha = 0;
            this.animate(this, 'y', this.INITIAL_Y, 'Phaser.Easing.Linear.None', 100).then(()=>{
              this.closeResolve();
              this.autoplayFsm.transition('CLOSE');
            });
          },
          barClicked: ()=>{
            this.events.onAutoPlayClicked.dispatch(this);
          }
        },
        CLOSE: {
          _onEnter: ()=>{
            console.info('[Button]AutoPlayPanel->CLOSE->_onEnter()');
            this.barButton.inputEnabled = true;
            this.isOpen = false;
            this.y = this.INITIAL_Y;
            // minus plus buttons are invisible
            this.barButtonPlus.visible = false;
            this.barButtonMinus.visible = false;
          },
          open: ()=>{
            this.updateButtons();

            this.isOpen = true;
            this.barButton.inputEnabled = false;
            this.animate(this, 'y', this.FINAL_Y, 'Phaser.Easing.Linear.None', 50).then(()=>{
              this.animate(this.barButtonPlus, 'alpha', 1, 'Phaser.Easing.Elastic.In', 50);
              this.animate(this.barButtonMinus, 'alpha', 1, 'Phaser.Easing.Elastic.In', 50).then(()=>{
                this.openResolve();
                this.autoplayFsm.transition('OPEN');
              });
            });
          },
          barClicked: ()=>{
            this.events.onAutoPlayClicked.dispatch(this);
          }
        }
      },
    });

    this.autoplayFsm = new FSM();
    this.autoplayFsm.transition('CLOSE');

  }

  onInputOver (){
    this.autoplayTimesPanelText.fill = this.autoplayTimesPanelTextStyle.overFill || '#fff';
    this.autoplayTimesPanelText.stroke = this.autoplayTimesPanelTextStyle.overStroke;
  }

  onInputDown (){
    this.autoplayTimesPanelText.fill = this.autoplayTimesPanelTextStyle.offFill || '#fff';
  }

  onInputOut (){
    this.autoplayTimesPanelText.fill = this.autoplayTimesPanelTextStyle.fill || '#fff';
    this.autoplayTimesPanelText.stroke = this.autoplayTimesPanelTextStyle.stroke;
  }

  debugTextBounds (x, y, w, h){
    // make debug rectangle to show the text's bounds
    this.debugRectangle = this.game.add.graphics(0, 0);
    this.debugRectangle.beginFill(0x1cc81c);
    this.debugRectangle.drawRect(x, y, w, h);

    this.addChild(this.debugRectangle);

    var debugSprite = new Phaser.Sprite(this.game, 0, 0);
    debugSprite.addChild(this.debugRectangle);
    debugSprite.visible = true;
    debugSprite.alpha = 0.4;
    this.addChild(debugSprite);
  }

  animate (object, attribute, value, easing, time){
    var animatePromise = new Promise((resolve)=>{
      var animObject = {};
      if (attribute === 'scale'){
        animObject['x'] = value;
        animObject['y'] = value;
        object = object.scale;
      }
      animObject[attribute] = value;
      var tween = this.game.add.tween(object).to(animObject, time, this.easingFunctions[easing], true);
      tween.onComplete.add(()=>{
        resolve();
      });
    });
    return animatePromise;
  }

  barPressed (){
    console.info(' AutoPlayPanel --> barPressed');
    this.autoplayFsm.handle('barClicked');
  }

  plusPressed (){
    console.info(' AutoPlayPanel --> minusPressed');
    //remove first element and put it last
    this.autoplayFsm.handle('plusClicked');
  }

  minusPressed (){
    console.info(' AutoPlayPanel --> plusPressed');
    //remove last element and put it first
    this.autoplayFsm.handle('minusClicked');
  }

  set state (value){
    this.autoplayFsm.transition(value);
  }

  open (){
    var openPromise = new Promise((resolve)=>{
      this.openResolve = resolve;
      this.autoplayFsm.handle('open');
    });
    return openPromise;
  }

  close (){
    var closePromise = new Promise((resolve)=>{
      this.closeResolve = resolve;
      this.autoplayFsm.handle('close');
    });
    if(!this.isOpen){
      this.closeResolve();
    }
    return closePromise;
  }

  disablePlusButton (){
    this.barButtonPlus.disable();
    this.barButtonPlus.inputEnabled = false;
  }

  disableMinusButton (){
    this.barButtonMinus.disable();
    this.barButtonMinus.inputEnabled = false;
  }

  enablePlusButton (){
    this.barButtonPlus.enable();
    this.barButtonPlus.inputEnabled = true;
  }

  enableMinusButton (){
    this.barButtonMinus.enable();
    this.barButtonMinus.inputEnabled = true;
  }

  updateButtons (){
    // check available bets and disabe/enable Plus/Minus functions
    if(this._currentAutoplayChosen === this.current_MAX){
      this.disablePlusButton();
    }
    else{
      this.enablePlusButton();
    }
    if(this._currentAutoplayChosen === this.current_MIN){
      this.disableMinusButton();
    }
    else{
      this.enableMinusButton();
    }
  }

  disable (){
    this.buttonSpriteGroup.callAll('disable');
  }

  enable (){
    this.buttonSpriteGroup.callAll('enable');
  }

  set autoPlayTimesChosen (value){
    this._currentAutoplayChosen = value;
    this.autoPlayTimesText.setText(this._currentAutoplayChosen);
  }

  get autoPlayTimesChosen (){
    return this._currentAutoplayChosen;
  }

  resetAvailableAutoplays () {
    this.availableAutoplays = this.game.api.config.availableAutoplays;
  }

  clone (obj){
    var clone = {};
    clone.prototype = obj.prototype;
    for (var property in obj){
      clone[property] = obj[property];
    }
    return clone;
  }
}

export default UIAutoPlayPanel;
