/*
 * Spinner
 * ============================================================================
 *
 *
 */

class Spinner extends Phaser.Sprite {

  constructor (game, parent, x, y, key) {
    super(game, x, y, key);
    this.animations.add('spin', null, 20, true);
    this.anchor.x = 0.5;
    this.anchor.y = 0.5;
    this.x = parent.width/2;
    this.y = parent.height/4;

    this.visible = false;
  }

  start (){
    this.visible = true;
    this.play('spin');
  }

  stop (){
    this.visible = false;
    this.animations.stop(null, true);
  }

}

export default Spinner;
