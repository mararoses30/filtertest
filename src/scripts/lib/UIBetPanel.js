/*
 * Play Button
 * ============================================================================
 *
 *
 */

import * as Machina from '../../../node_modules/machina/lib/machina';
import {TAtlasButton} from '../lib/buttons';

class UIBetPanel extends Phaser.Group {
  constructor (game, parent, behindtheUIBar){
    super(game, parent, 'BetPanel', false);

    this.behindtheUIBar = behindtheUIBar;

    this.availableBets = this.game.api.config.availableBets;

    this.MIN_BET = Math.min.apply(null, this.availableBets);
    this.MAX_BET = Math.max.apply(null, this.availableBets);

    // Initial and final position for tween
    this.INITIAL_Y = 400 * this.game.SCALE_FACTOR;
    this.FINAL_Y = 0 * this.game.SCALE_FACTOR;

    this.events = [];
    this.events.onBetPanelSelected = new Phaser.Signal();
    this.events.onUpClicked = new Phaser.Signal();
    this.events.onDownClicked = new Phaser.Signal();

    this.textGroup = new Phaser.Group(game, this, 'buttonsSprite', true);
    this.add(this.textGroup);
    this.easingFunctions = {
      'Phaser.Easing.Linear.None' : Phaser.Easing.Linear.None,
      'Phaser.Easing.Elastic.In': Phaser.Easing.Elastic.In,
      'Phaser.Easing.Elastic.Out': Phaser.Easing.Elastic.Out,
      'Phaser.Easing.Elastic.InOut': Phaser.Easing.Elastic.InOut
    };


    // current autoplay
    this._currentWager = this.availableBets[0];

    // Boolean isOpen
    this.isOpen = false;

    let FSM = Machina.Fsm.extend({
      namespace: 'betFsm',
      initialState: 'INIT',

      states: {
        INIT: {
          _onEnter: ()=>{
            console.info('[Button]BetPanel->INIT->_onEnter()');
            //create autoplayPanel
            this.betButton = new TAtlasButton(game, 0, 0, 'menu_ui', this.betButtonPressed, this,  'SE 12', 'WagerButton_OVER', 'WagerButton_ON', 'WagerButton_OFF');
            // this.betButton.inputEnabled = false;
            this.parent.add(this.betButton);

            this.arroWPanel = new Phaser.Sprite(game, 0, 0, 'menu_ui', 'WagerSelectPanel');
            // this.betButton.inputEnabled = false;
            this.behindtheUIBar.add(this.arroWPanel);

            // create play button
            this.betButtonUp = new TAtlasButton(game, 0, 0, 'menu_ui', this.upPressed, this,  'SE 13', 'Arrow_Up_ON', 'Arrow_Up_ON', 'Arrow_Up_OFF');
            this.betButtonUp.alpha = 1;
            this.behindtheUIBar.add(this.betButtonUp);

            this.betButtonDown = new TAtlasButton(game, 0, 0, 'menu_ui', this.downPressed, this,  'SE 13', 'Arrow_Down_ON', 'Arrow_Down_ON', 'Arrow_Down_OFF');
            this.betButtonDown.alpha = 1;
            this.behindtheUIBar.add(this.betButtonDown);

            // create nums left
            var style = { font: 10 + 'px Roboto',
                    fill: '#fff',
                    align: 'center', // the alignment of the text is independent of the bounds, try changing to 'center' or 'right'
                    boundsAlignH: 'right',
                    boundsAlignV: 'middle'
                  };

            this.betTextBound = this.game.cache.getJSON('layout')['layout']['betTextBound'][0];
            var fontsize = this.getMaxFontSizeFromArray( this.game.api.config.availableBets, this.betTextBound.w, this.betTextBound.h, style);

            this.betText = new Phaser.Text(this.game, 0, 0, this.game.locale.formatCurrency(this._currentWager, this.game.api.config.currencyPrecision), style);
            this.betText.fontSize = fontsize;
            this.betText.setTextBounds(this.betTextBound.x, this.betTextBound.y,  this.betTextBound.w, this.betTextBound.h);
            this.parent.add(this.betText);
          }
        },
        OPEN: {
          _onEnter: ()=>{
            this.isOpen = true;
            this.y = this.FINAL_Y;
            this.betButtonUp.inputEnabled = true;
            this.betButtonDown.inputEnabled = true;
            this.updateButtons();

          },
          upClicked: ()=>{
            this.synchronizeBetsArrowsUp();
            this.events.onUpClicked.dispatch(this);
            this.updateButtons();
          },
          downClicked: ()=>{
            this.synchronizeBetsArrowsDown();
            this.events.onDownClicked.dispatch(this);
            this.updateButtons();
          },
          close: ()=>{
            this.betButtonUp.inputEnabled = false;
            this.betButtonDown.inputEnabled = false;
            this.animate(this.behindtheUIBar, 'y', this.INITIAL_Y, 'Phaser.Easing.Linear.None', 100).then(()=>{
              this.closeResolve();
              this.betFsm.transition('CLOSE');
            });
          },
          betButtonClicked: ()=>{
            this.events.onBetPanelSelected.dispatch(this);
          },
          noAvailableBet: ()=>{
            this.disable();
          },
          oneAvailableBet: ()=>{
            this.disableUpDownButtons();
          },
          multipleAvailableBet: ()=>{
            this.enableUpDownButtons();
          }
        },
        CLOSE: {
          _onEnter: ()=>{
            console.info('[Button]BetPanel->CLOSE->_onEnter()');
            this.isOpen = false;
            this.behindtheUIBar.y = this.INITIAL_Y;
            this.betButtonUp.inputEnabled = false;
            this.betButtonDown.inputEnabled = false;
            // minus plus buttons are invisible
          },
          open: ()=>{
            this.updateButtons();
            this.isOpen = true;
            this.animate(this.behindtheUIBar, 'y', this.FINAL_Y, 'Phaser.Easing.Linear.None', 100).then(()=>{
              this.animate(this.betButtonUp, 'alpha', 1, 'Phaser.Easing.Elastic.In', 50);
              this.animate(this.betButtonDown, 'alpha', 1, 'Phaser.Easing.Elastic.In', 50).then(()=>{
                this.openResolve();
                this.betFsm.transition('OPEN');
              });
            });
          },
          betButtonClicked: ()=>{
            this.events.onBetPanelSelected.dispatch(this);
          },
          noAvailableBet: ()=>{
            this.disable();
          },
          oneAvailableBet: ()=>{
            this.disableUpDownButtons();
          },
          multipleAvailableBet: ()=>{
            this.enableUpDownButtons();
          }
        }
      },
    });

    this.betFsm = new FSM();
    this.betFsm.transition('CLOSE');
  }

  animate (object, attribute, value, easing, time){
    this.tweens = [];
    var animatePromise = new Promise((resolve)=>{
      var animObject = {};
      if (attribute === 'scale'){
        animObject['x'] = value;
        animObject['y'] = value;
        object = object.scale;
      }
      animObject[attribute] = value;
      var tween = this.game.add.tween(object).to(animObject, time, this.easingFunctions[easing], true);
      tween.onComplete.add(()=>{
        this.isTweening = false;
        resolve();
      });
      tween.onStart.add(()=>{
        this.isTweening = true;
      });
      this.tweens.push(tween);
    });
    return animatePromise;
  }

  synchronizeBetsArrowsUp () {
    let wagerIndex = this._availableBets.indexOf(this.wager);
    if (wagerIndex > -1) {
      this.wager = this._availableBets[wagerIndex+1];
    } else {
      this.wager = this._availableBets[this._availableBets.length-1];
    }
  }


  synchronizeBetsArrowsDown () {
    let wagerIndex = this._availableBets.indexOf(this.wager);
    if (wagerIndex > -1) {
      this.wager = this._availableBets[wagerIndex-1];
    } else {
      this.wager = this._availableBets[this._availableBets.length-1];
    }
  }

  betButtonPressed (){
    this.betFsm.handle('betButtonClicked');
  }

  upPressed (){
    //remove first element and put it last
    this.betFsm.handle('upClicked');
  }

  downPressed (){
    //remove last element and put it first
    this.betFsm.handle('downClicked');
  }

  set state (value){
    this.betFsm.transition(value);
  }

  open (){
    var openPromise = new Promise((resolve)=>{
      this.openResolve = resolve;
      this.betFsm.handle('open');
    });
    return openPromise;
  }

  close (){
    var closePromise = new Promise((resolve)=>{
      this.closeResolve = resolve;
      this.betFsm.handle('close');
    });
    if(!this.isOpen){
      this.closeResolve();
    }
    return closePromise;
  }

  disableUpDownButtons (){
    this.disableUpButton();
    this.disableDownButton();
  }

  disableUpButton (){
    this.betButtonUp.disable();
    this.betButtonUp.inputEnabled = false;
  }

  disableDownButton (){
    this.betButtonDown.disable();
    this.betButtonDown.inputEnabled = false;
  }

  enableUpDownButtons (){
    this.enableUpButton();
    this.enableDownButton();
  }

  enableUpButton (){
    this.betButtonUp.enable();
    this.betButtonUp.inputEnabled = true;
  }

  enableDownButton (){
    this.betButtonDown.enable();
    this.betButtonDown.inputEnabled = true;
  }

  // disables only bet button when disabled
  // the up/down arrows cannot be shown
  disable (){
    this.betButton.inputEnabled = false;
  }

  enable (){
    this.betButton.inputEnabled = true;
    this.betText.alpha = 1;
  }

  inactive (){
    this.betButton.inputEnabled = false;
    this.betText.alpha = 0.5;
  }

  set wager (value){
    this._currentWager = value;
    this.betText.cacheAsBitmap = false;
    this.betText.setText(this.game.locale.formatCurrency(this._currentWager, this.game.api.config.currencyPrecision));

    this.betText.cacheAsBitmap = true;
  }

  get wager (){
    return this._currentWager;
  }

  updateButtons (){
    // check available bets and disabe/enable Up/Down functions
    if(this._currentWager === this.current_MAX){
      this.disableUpButton();
    }
    else{
      this.enableUpButton();
    }
    if(this._currentWager === this.current_MIN){
      this.disableDownButton();
    }
    else{
      this.enableDownButton();
    }
  }

  updateBets (balance){
    var configAvailBets = this.game.api.config.availableBets;
    this._availableBets = [];
    this._unavailableBets = [];
    configAvailBets.forEach((bet)=>{
      if(bet <= balance){
        this._availableBets.push(bet);
      }
      else{
        this._unavailableBets.push(bet);
      }
    });

    if(this._availableBets.length > 0){
      this.current_MIN = Math.min.apply(Math, this._availableBets);
      this.current_MAX = Math.max.apply(Math, this._availableBets);
    }
    else{
      this.current_MIN = configAvailBets[0];
      this.current_MAX = configAvailBets[0];
    }

    if(this._unavailableBets) {
      if(this._unavailableBets.indexOf(this.wager) != -1){
        this.wager = this.current_MAX;
      }
    }

    return  this._availableBets;
  }

  getMaxFontSizeFromArray (values, width, height, style){
    var texts = [];

    values.forEach((value)=>{
      texts.push(this.game.locale.formatCurrency(value, this.game.api.config.currencyPrecision));
    });

    return this.getOptimalFontSize(this.game, texts, width, height, style);
  }

  getOptimalFontSize (game, text, width, height, style) {
    function getLongestPhrase (arrayOfPhrases) {
      let longestPhrase = '';
      arrayOfPhrases.forEach( (phrase)=> {
        if (phrase.toString().length >= longestPhrase.length) {
          longestPhrase = phrase;
        }
      });
      return longestPhrase;
    }
    var txt;
    if (text instanceof Array) {
      txt = getLongestPhrase(text);
    } else if (typeof text === 'string') {
      txt = text;
    } else {
      throw new Error('Invalid argument: ' + JSON.stringify(text));
    }

    if (typeof style === 'undefined') {
      style = {};
    }

    let pixiText = new Phaser.Text(game, 0, 0, txt, style);

    if (pixiText.width > width || pixiText.height > height) {
      // Shrink
      while(pixiText.width > width || pixiText.height > height) {
        pixiText.fontSize -= 1;
      }
    } else {
      // Grow
      while(pixiText.width < width && pixiText.height < height) {
        pixiText.fontSize  += 1;
      }
      // Catch off-by-one errors
      if (pixiText.width > width || pixiText.height > height) {
        pixiText.fontSize -= 1;
      }
    }
    return pixiText.fontSize;
  }
}

export default UIBetPanel;
