/*
 * Storage
 * ============================================================================
 *
 *
 */

class Storage{

  constructor (game) {
    this.game = game;

    this.hasLocaleStorage = this._checkLocalStorage();
    console.log('LocalStorage: ', this.hasLocaleStorage);

    this.gameName = this.game.api.config.response.Game.Configuration.OperatorGameName;
    console.log(this.gameName);
  }

  setItem (item, value){
    if(!item) return false;

    item = this.gameName + ':' + item;
    if(this.hasLocaleStorage){
      window.localStorage.setItem(item, value);
      return true;
    }
    if (this.hasItem(item)) this.removeItem(item);
    document.cookie = encodeURIComponent(item) + '=' + encodeURIComponent(value) + '; expires=Fri, 31 Dec 9999 23:59:59 GMT';
    return true;
  }

  removeItem (item) {
    if (!item) return false;

    item = this.gameName + ':' + item;
    if (this.hasLocaleStorage && window.localStorage.getItem(item)) {
      window.localStorage.removeItem(item);
      return true;
    }
    if (!item || !this.hasItem(item)) return false;
    document.cookie = encodeURIComponent(item) + '=; expires=Thu, 01 Jan 1970 00:00:00 GMT';
    return true;
  }

  getItem (item) {
    if (!item) return null;

    item = this.gameName + ':' + item;
    if (this.hasLocaleStorage && window.localStorage.getItem(item) !== null) {
      return window.localStorage.getItem(item);
    }
    return decodeURIComponent(document.cookie.replace(new RegExp('(?:(?:^|.*;)\\s*' + encodeURIComponent(item).replace(/[\-\.\+\*]/g, '\\$&') + '\\s*\\=\\s*([^;]*).*$)|^.*$'), '$1')) || null;
  }

  hasItem (item) {
    item = this.gameName + ':' + item;

    if (this.hasLocaleStorage && window.localStorage.getItem(item)) {
      return true;
    }
    return (new RegExp('(?:^|;\\s*)' + encodeURIComponent(item).replace(/[\-\.\+\*]/g, '\\$&') + '\\s*\\=')).test(document.cookie);
  }

  _checkLocalStorage (){
    try {
      localStorage.setItem('testLocaleStorage', 'test');
      localStorage.removeItem('testLocaleStorage');
      return true;
    } catch(e) {
      return false;
    }
  }
}

export default Storage;
