import {DynamicText} from '../lib/DynamicText';
/*
 * An extended Phaser.Button Class that creates a button from an atlas texture
 * image while calculates any custome hitarea that is provided with hitarea.json
 * created with PhysicsEditor.
 */

export class TAtlasButton extends Phaser.Button {
  /**
   * @param  {game} - The game object
   * @param  {x} - The x position to the world
   * @param  {y} - The y position to the world
   * @param  {atlas} - The atlas key where the image resided
   * @param  {callback} - The callback function to be called on Click
   * @param  {callbackContext} - The callback context object
   * @param  {sound} - the sound key of the soundsprite
   * @param  {overFrame} - The frame that is used for the over state. It is also the out and up state
   * @param  {downFrame} - The frame that is used for the down state.
   * @param  {sound} - the sound key of the soundsprite
   * @param {text} - Text object containing text and style. {text:'...', style:{...}, boundingBox: {...}}
   * @return {TAtlasButton}
   */

  constructor (game, x, y, atlas, callback, callbackContext, sound, overFrame, outFrame, downFrame, textObj){
    if(game.device.desktop && (game.net.medium != 'gb')){
      super(game, x, y, atlas, callback, callbackContext, overFrame, outFrame, downFrame, outFrame);
    }
    else{// if is mobile (no hover state) then make the over state to be the same as out frame
      super(game, x, y, atlas, callback, callbackContext, outFrame, outFrame, downFrame, outFrame);
    }

    var hitarea;
    var has_key_in_hitareajson = false;
    // check if this button has a dedicated hitarea in hitarea.json
    if( game.cache.checkJSONKey('layout')){
      var hitareaData = game.cache.getJSON('layout');
      if(hitareaData['hitareas'] && outFrame in hitareaData['hitareas']){
        has_key_in_hitareajson = true;
        let shape = hitareaData['hitareas'][outFrame][0]['shape'];
        let points = [];

        for (let point of shape){
          points.push(new Phaser.Point(point.x, point.y));
        }

        hitarea = new Phaser.Polygon();
        hitarea.setTo(points);
      }
    }
    if(!has_key_in_hitareajson){//we assume that the hitarea is rectangle
      // calculate hitArea of button from atlas file
      let cleaBtnY = this._frame.spriteSourceSizeY;
      let cleaBtnW = this._frame.spriteSourceSizeW;
      let cleaBtnH = this._frame.spriteSourceSizeH;
      let cleaBtnX = this._frame.spriteSourceSizeX;

      hitarea = new Phaser.Rectangle(cleaBtnX, cleaBtnY, cleaBtnW, cleaBtnH);
    }

    this.hitArea  = hitarea;

    // create the text to be displayed
    // this.game, this.currentCredits, this.betPanelCreditsValuePosition.x, this.betPanelCreditsValuePosition.y, 50, 20, this.coinsCreditsValueStyle
    if(textObj){
      this.textObj = textObj;
      let x, y, w, h;
      // bounding bos is provided then apply values else apply spriteSource
      if (textObj.boundingBox){
        x = textObj.boundingBox.x;
        y = textObj.boundingBox.y;
        w = textObj.boundingBox.w;
        h = textObj.boundingBox.h;
      }
      else{
        x = this._frame.spriteSourceSizeX;
        y = this._frame.spriteSourceSizeY;
        w = this._frame.spriteSourceSizeW;
        h = this._frame.spriteSourceSizeH;
      }

      // this.debugTextBounds(x, y, w, h);

      this.text = new DynamicText(this.game, textObj.text, x, y, w, h, this.clone(textObj.style));

      // if there is text the on input Up revert color
      this.events.onInputOut.add(this.fillOnText.bind(this));
      this.events.onInputOver.add(this.fillOverText.bind(this));
      this.events.onInputOut.add(this.enableShadow.bind(this));

      this.addChild(this.text);
    }

    this.events.onInputDown.add(()=>{
      if(sound){
        this.game.fx.play(sound);
      }

      this.fillDownText();
      this.disableShadow();
    });
  }

  debugTextBounds (x, y, w, h){
    // make debug rectangle to show the text's bounds
    this.debugRectangle = this.game.add.graphics(0, 0);
    this.debugRectangle.beginFill(0x1cc81c);
    this.debugRectangle.drawRect(x, y, w, h);

    this.addChild(this.debugRectangle);

    var debugSprite = new Phaser.Sprite(this.game, 0, 0);
    debugSprite.addChild(this.debugRectangle);
    debugSprite.visible = true;
    debugSprite.alpha = 0.4;
    this.addChild(debugSprite);
  }
  /*
  Disable the button
   */
  enable () {
    this.freezeFrames = false;
    this.inputEnabled = true;
    this.input.useHandCursor = true;
    this.frameName = this._onOutFrame;
    this.fillOnText();
    this.enableShadow();
  }
  enableClick () {
    this.inputEnabled = true;
    this.input.useHandCursor = true;
  }

  disable (){
    this.frameName = this._onDownFrame;
    this.freezeFrames = true;
    this.inputEnabled = false;

    this.fillDownText();
    this.disableShadow();
  }

  disableClick () {
    this.inputEnabled = false;
    this.input.useHandCursor = false;
  }

  clear (){
    this.inputEnabled = true;
    this.input.useHandCursor = true;
    this.frameName = this._onOutFrame;
    this.fillOnText();
  }

  fillOnText (){
    if(this.text){
      this.text.fill = this.textObj.style.fill || '#fff';
    }
  }

  fillDownText (){
    if(this.text){
      this.text.fill = this.textObj.style.offFill || '#5a615d';
    }
  }

  fillOverText (){
    if(this.text){
      this.text.fill = this.textObj.style.overFill || '#d3d3d3';
    }

    // this.disableShadow();
    this.text.shadowColor = this.textObj.style.overShadowColor || '#c8c8c8';
    this.text.updateShadow(true);
  }

  clone (obj){
    var clone = {};
    clone.prototype = obj.prototype;
    for (var property in obj){
      clone[property] = obj[property];
    }
    return clone;
  }

  setShadow (x, y, color, blur, shadowRangeMultiplier = 1, shadowStroke, shadowFill) {
    this.shadowX = x;
    this.shadowY = y;
    this.shadowColor = color;
    this.shadowBlur = blur;
    this.shadowStroke = shadowStroke;
    this.shadowFill = shadowFill;

    this.shadowRangeMultiplier = shadowRangeMultiplier;

    this.enableShadow()
  }

  enableShadow () {
    if(this.text) {
      this.text.setShadow(this.shadowX, this.shadowY, this.shadowColor, (this.shadowBlur * this.shadowRangeMultiplier), this.shadowStroke, this.shadowFill);

    }
  }

  disableShadow () {
    if (this.text) {
      // We are only interested in the fourth component (alpha)
      this.text.shadowColor = 'rgba(0, 0, 0, 0)';
      // this.text.updateShadow(false);
      this.text.updateShadow(true);
    }
  }

}

export class TAtlasBetButton extends TAtlasButton{
  /**
   * @param  {game} - The game object
   * @param  {x} - The x position to the world
   * @param  {y} - The y position to the world
   * @param  {atlas} - The atlas key where the image resided
   * @param  {callback} - The callback function to be called on Click
   * @param  {callbackContext} - The callback context object
   * @param  {sound} - the sound key of the soundsprite
   * @param  {overFrame} - The frame that is used for the over state. It is also the out and up state
   * @param  {downFrame} - The frame that is used for the down state.
   * @param  {selectedFrame} - The frame that is used when bet is Selected .
   * @return {TAtlasButton}
   */
  constructor (game, x, y, atlas, callback, callbackContext, wager, sound, overFrame, outFrame, downFrame, selectedFrame){
        //  (game, x, y, atlas, callback, callbackContext, sound, overFrame,outFrame, downFrame)
    super(game, x, y, atlas, callback, callbackContext, sound, overFrame, outFrame, downFrame);

    this.wager = wager;
    this.selectedFrame = selectedFrame;
    this.on = false;
  }

  select (){
    this.frameName = this.selectedFrame;
    this.inputEnabled = false;
    this.on = true;
  }

  clear (){
    this.on = false;
    this.inputEnabled = true;
    this.frameName = this._onOutFrame;
  }

  update (){
    if (this.on){
      this.frameName = this.selectedFrame;
    }
  }

}
