<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.5.0</string>
        <key>fileName</key>
        <string>C:/HTML5/test/resources/Intralot_images/atlas_tp.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string>@3x</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.666667</double>
                <key>extension</key>
                <string>@2x</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.333333</double>
                <key>extension</key>
                <string>@1x</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>phaserexporter</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">PngQuantHigh</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png8</enum>
        <key>borderPadding</key>
        <uint>2</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>4096</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>phaserjson</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../TP_exports/atlas{v}.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">atlas/Ancient_Plate/$.png</key>
            <key type="filename">atlas/Ancient_Plate/0.png</key>
            <key type="filename">atlas/Ancient_Plate/1.png</key>
            <key type="filename">atlas/Ancient_Plate/2.png</key>
            <key type="filename">atlas/Ancient_Plate/3.png</key>
            <key type="filename">atlas/Ancient_Plate/4.png</key>
            <key type="filename">atlas/Ancient_Plate/5.png</key>
            <key type="filename">atlas/Ancient_Plate/6.png</key>
            <key type="filename">atlas/Ancient_Plate/7.png</key>
            <key type="filename">atlas/Ancient_Plate/8.png</key>
            <key type="filename">atlas/Ancient_Plate/9.png</key>
            <key type="filename">atlas/Ancient_Plate/comma.png</key>
            <key type="filename">atlas/Ancient_Plate/dot.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>35,53,70,105</rect>
                <key>scale9Paddings</key>
                <rect>35,53,70,105</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">atlas/Ancient_Plate/Sorry_try_again.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>229,193,458,386</rect>
                <key>scale9Paddings</key>
                <rect>229,193,458,386</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">atlas/Arrows/Left_arrow/Larrow_Normal.png</key>
            <key type="filename">atlas/Arrows/Left_arrow/Larrow_Off.png</key>
            <key type="filename">atlas/Arrows/Left_arrow/Larrow_Over.png</key>
            <key type="filename">atlas/Arrows/Left_arrow/Larrow_Press.png</key>
            <key type="filename">atlas/Arrows/Right_arrow/RLarrow_Normal.png</key>
            <key type="filename">atlas/Arrows/Right_arrow/RLarrow_Off.png</key>
            <key type="filename">atlas/Arrows/Right_arrow/RLarrow_Over.png</key>
            <key type="filename">atlas/Arrows/Right_arrow/RLarrow_Press.png</key>
            <key type="filename">atlas/Bets/$1/$1_Normal.png</key>
            <key type="filename">atlas/Bets/$1/$1_Off.png</key>
            <key type="filename">atlas/Bets/$1/$1_Over.png</key>
            <key type="filename">atlas/Bets/$1/$1_Select.png</key>
            <key type="filename">atlas/Bets/$10/$10_Normal.png</key>
            <key type="filename">atlas/Bets/$10/$10_Off.png</key>
            <key type="filename">atlas/Bets/$10/$10_Over.png</key>
            <key type="filename">atlas/Bets/$10/$10_Select.png</key>
            <key type="filename">atlas/Bets/$2/$2_Normal.png</key>
            <key type="filename">atlas/Bets/$2/$2_Off.png</key>
            <key type="filename">atlas/Bets/$2/$2_Over.png</key>
            <key type="filename">atlas/Bets/$2/$2_Select.png</key>
            <key type="filename">atlas/Bets/$20/$20_Normal.png</key>
            <key type="filename">atlas/Bets/$20/$20_Off.png</key>
            <key type="filename">atlas/Bets/$20/$20_Over.png</key>
            <key type="filename">atlas/Bets/$20/$20_Select.png</key>
            <key type="filename">atlas/Bets/$3/$3_Normal.png</key>
            <key type="filename">atlas/Bets/$3/$3_Off.png</key>
            <key type="filename">atlas/Bets/$3/$3_Over.png</key>
            <key type="filename">atlas/Bets/$3/$3_Select.png</key>
            <key type="filename">atlas/Bets/$5/$5_Normal.png</key>
            <key type="filename">atlas/Bets/$5/$5_Off.png</key>
            <key type="filename">atlas/Bets/$5/$5_Over.png</key>
            <key type="filename">atlas/Bets/$5/$5_Select.png</key>
            <key type="filename">atlas/Exit_Help_Sound/Exit/Exit_Normal.png</key>
            <key type="filename">atlas/Exit_Help_Sound/Exit/Exit_Off.png</key>
            <key type="filename">atlas/Exit_Help_Sound/Exit/Exit_Over.png</key>
            <key type="filename">atlas/Exit_Help_Sound/Exit/Exit_Press.png</key>
            <key type="filename">atlas/Exit_Help_Sound/Help/Help_Normal.png</key>
            <key type="filename">atlas/Exit_Help_Sound/Help/Help_Off.png</key>
            <key type="filename">atlas/Exit_Help_Sound/Help/Help_Over.png</key>
            <key type="filename">atlas/Exit_Help_Sound/Help/Help_Press.png</key>
            <key type="filename">atlas/Exit_Help_Sound/Sound/Sound_Normal.png</key>
            <key type="filename">atlas/Exit_Help_Sound/Sound/Sound_Off.png</key>
            <key type="filename">atlas/Exit_Help_Sound/Sound/Sound_Over.png</key>
            <key type="filename">atlas/Exit_Help_Sound/Sound/Sound_Press.png</key>
            <key type="filename">atlas/Exit_Help_Sound/Sound/Sound_disable.png</key>
            <key type="filename">atlas/box.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>630,270,1260,540</rect>
                <key>scale9Paddings</key>
                <rect>630,270,1260,540</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">atlas/test.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>128,128,256,256</rect>
                <key>scale9Paddings</key>
                <rect>128,128,256,256</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>atlas</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
