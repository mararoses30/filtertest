
1. At "GET https://<irgs_host:port>/api/iwg/v1/game/<game_id>" do i have to send the below object ?
{
        "Header":
        {
            "APIVersionID": "1" /*@type{Integer}*/ /*@desc{Set by Game Client, iRGS API Version}*/ /*@own{Game Client}*/,
            "ResponseCode": "200" /*@type{Integer}*/ /*@desc{Set by iRGS, HTTP Status Code, Normally: 200 - OK}*/ /*@own{iRGS}*/,
            "GameID": "00000000-0000-0000-0000-000000000000" /*@type{String}*/ /*@desc{Set by Game Client, URL defined Game Instance ID for current Operator ID}*/ /*@own{Game Client}*/,
            "GameLocaleID": "en-GB" /*@type{String}*/ /*@desc{Set by Game Client, the locale Id defined as 'xx-yy', where 'xx' is the ISO639-1 code and 'yy' is the ISO3166-1-alpha-2 country code}*/ /*@own{Game Client}*/,
            "OperatorID": "00000000-0000-0000-0000-000000000000" /*@type{String}*/ /*@desc{Set by Game Client, URL defined Operator ID}*/ /*@own{Game Client}*/,
            "UserToken": "00000000-0000-0000-0000-000000000000" /*@type{String}*/ /*@desc{Set by Game Client, URL defined PAM Authentication Token retrieved from PAM during user login, normally a GUID}*/ /*@own{Game Client}*/,
        },
        "Device":
        {
            "DeviceClass": "iOS" /*@type{String}*/ /*@desc{Set by Game Client and iRGS Client Detection Framework, the device Class (iOS, Android, PC, etc.)}*/ /*@own{Game Client}*/,
            "DeviceModel": "iPhone6" /*@type{String}*/ /*@desc{Set by Game Client and iRGS Client Detection Framework, device model if it can be detected}*/ /*@own{Game Client}*/,
            "DeviceSDKVersion": "9" /*@type{String}*/ /*@desc{Set by Game Client and iRGS Client Detection Framework, device OS version for iOS or SDK version for Android}*/ /*@own{Game Client}*/,
            "DeviceMobileBrowserName": "Safari" /*@type{String}*/ /*@desc{Set by Game Client and iRGS Client Detection Framework, mobile browser name (eg. Chrome, Safari) or null}*/ /*@own{Game Client}*/,
            "DeviceScreenWidth": "0" /*@type{Integer}*/ /*@desc{Set by Game Client and iRGS Client Detection Framework, device Screen Width in pixels}*/ /*@own{Game Client}*/,
            "DeviceScreenHeight": "0" /*@type{Integer}*/ /*@desc{Set by Game Client and iRGS Client Detection Framework, device Screen Height in pixels}*/ /*@own{Game Client}*/,
            "DevicePixelRatio": "1.5" /*@type{Float}*/ /*@desc{Set by Game Client and iRGS Client Detection Framework, device pixel ratio}*/ /*@own{Game Client}*/,
            "CData": null /*@type{Object}*/ /*@desc{Set by Game Client and iRGS Client Detection Framework, game-specific additional Device detection data}*/ /*@own{Game Client}*/
        }
    }

2. At "POST https://<irgs_host:port>/api/iwg/v1/game/<game_id>/ticket" do i have to send the below object ?
    {
        "Header":
        {
            "APIVersionID": "1" /*@type{Integer}*/ /*@desc{Set by Game Client, iRGS API Version}*/ /*@own{Game Client}*/,
            "ResponseCode": "200" /*@type{Integer}*/ /*@desc{Set by iRGS, HTTP Status Code, Normally: 200 - OK}*/ /*@own{iRGS}*/,
            "GameID": "00000000-0000-0000-0000-000000000000" /*@type{String}*/ /*@desc{Set by Game Client, URL defined Game Instance ID for current Operator ID}*/ /*@own{Game Client}*/,
            "GameLocaleID": "en-GB" /*@type{String}*/ /*@desc{Set by Game Client, the locale Id defined as 'xx-yy', where 'xx' is the ISO639-1 code and 'yy' is the ISO3166-1-alpha-2 country code}*/ /*@own{Game Client}*/,
            "OperatorID": "00000000-0000-0000-0000-000000000000" /*@type{String}*/ /*@desc{Set by Game Client, URL defined Operator ID}*/ /*@own{Game Client}*/,
            "UserToken": "00000000-0000-0000-0000-000000000000" /*@type{String}*/ /*@desc{Set by Game Client, URL defined PAM Authentication Token retrieved from PAM during user login, normally a GUID}*/ /*@own{Game Client}*/,
        },
        "Wager":
        {
            
            "Amount": "0" /*@type{Integer}*/ /*@desc{Set by Game Client, this is the Bet Amount selected by Player}*/ /*@own{Game Client}*/,
            "BetMultiplier": "0" /*@type{Integer}*/ /*@desc{Set by Game Client, this is the Bet Multiplier selected by Player}*/ /*@own{Game Client}*/,
        },
    }


 3. At "PUT https://<irgs_host:port>/api/iwg/v1/ticket/<ticket_id>" do i have to send the below object ?
    {
        "Header":
        {
            "APIVersionID": "1" /*@type{Integer}*/ /*@desc{Set by Game Client, iRGS API Version}*/ /*@own{Game Client}*/,
            "ResponseCode": "200" /*@type{Integer}*/ /*@desc{Set by iRGS, HTTP Status Code, Normally: 200 - OK}*/ /*@own{iRGS}*/,
            "GameID": "00000000-0000-0000-0000-000000000000" /*@type{String}*/ /*@desc{Set by Game Client, URL defined Game Instance ID for current Operator ID}*/ /*@own{Game Client}*/,
            "GameLocaleID": "en-GB" /*@type{String}*/ /*@desc{Set by Game Client, the locale Id defined as 'xx-yy', where 'xx' is the ISO639-1 code and 'yy' is the ISO3166-1-alpha-2 country code}*/ /*@own{Game Client}*/,
            "OperatorID": "00000000-0000-0000-0000-000000000000" /*@type{String}*/ /*@desc{Set by Game Client, URL defined Operator ID}*/ /*@own{Game Client}*/,
            "UserToken": "00000000-0000-0000-0000-000000000000" /*@type{String}*/ /*@desc{Set by Game Client, URL defined PAM Authentication Token retrieved from PAM during user login, normally a GUID}*/ /*@own{Game Client}*/,
        },
        "Ticket":
        {
            "TicketID": "00000000-0000-0000-0000-000000000000" /*@type{String}*/ /*@desc{Set by Game Client, this is the Ticket ID received from iRGS}*/ /*@own{Game Client}*/,
            "State": "IRGS_TICKET_STATE_ENUM" /*@type{IRGS_TICKET_STATE_ENUM}*/ /*@desc{Set by Game Client, 0-Closed: ticket is fully revealed to the player and the round is complete, 1-Pending: some parts of the ticket are revealed to the player but the round is not complete}*/ /*@own{Game Client}*/,
            "ActionID": "00000000-0000-0000-0000-000000000000" /*@type{String}*/ /*@desc{Set by Game Client, required both when closing the ticket (last action id) or saving its state (current action id).}*/ /*@own{Game Client}*/,
            "SymbolDistribution":
            [

                {
                    "SymbolID": "00000000-0000-0000-0000-000000000000" /*@type{String}*/ /*@desc{Set by Game Client, required when Ticket.State = 1}*/ /*@own{Game Client}*/,
                    "SymbolState": "IRGS_IW_SYMBOL_STATE_ENUM" /*@type{IRGS_IW_SYMBOL_STATE_ENUM}*/ /*@desc{Set by Game Client, required when Ticket.State = 1}*/ /*@own{Game Client}*/,
                    "Revealed": "0" /*@type{Integer}*/ /*@desc{Set by Game Client, 0-Concealed, 1-Revealed (Scratched)}*/ /*@own{Game Client}*/,
                    "CData": null /*@type{Object}*/ /*@desc{Set by iRGS, additional game-specific and action-specific Symbol Distribution item data}*/ /*@own{Game Client}*/
                }
            ]
        }
    }