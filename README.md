
## Batteries included

Every project created with this generator includes the following tool-set:

*   [Gulp][gulp], a popular task manager and build streaming, to handle
    development and distribution tasks.

*   [Browserify][brsy] support for easier management of components and
    dependency tracking.

*   [BrowserSync][bsnc] development server, for cross-device testing.

*   [EditorConfig][edcf] support, for consistent editor configuration between
    collaborators — [check if EditorConfig is available for your code
    editor][ecpl].

*   [ESLint][eslt] for code style and quality check.

*   [Babel][babl] to translate from ES6 to ES5 syntax.
    See which [language features][feat] are currently supported.

*   [Handlebars][hbs.] templates, [LESS][less] style sheets, source maps and
    more!


### Development Instructions 

```sh

npm install --global slush              # To install `slush`.

```

Install npm packages from packages.json

```sh

npm install

```

#### Install `ImageMagick`
##### Windows 
please install from http://www.imagemagick.org/script/binary-releases.php#windows
##### Ubuntu 14.04

```sh
sudo apt-get install imagemagick
```


#### Install `ffmpeg`

In order to create soundsprites `ffmpeg` is needed. 

##### Windows 
please go to [ffmpeg site](https://ffmpeg.org/download.html) 
##### Ubuntu 14.04

```sh
sudo apt-get --purge remove ffmpeg
sudo apt-get --purge autoremove

sudo apt-get install ppa-purge
sudo ppa-purge ppa:jon-severinsson/ffmpeg

sudo add-apt-repository ppa:mc3man/trusty-media
sudo apt-get update
sudo apt-get dist-upgrade

sudo apt-get install ffmpeg
```
### Available scripts

The following `npm` scripts are available to you, performing the tasks as
described below.

```sh
npm start     # Launch a development server. Same as `gulp dev`.
npm run dist  # Prepare the game release for distribution. Also `gulp dist`.
npm run clean # Delete temporary and distribution build files.
npm run audiosprite:sfx # Creates a soundsprite of the files inside resources/sfx.
npm run font:tiff2woff # Creates a woff font from tiff that resides inside resources/font
npm run intralot dist (-- --tag x.x.x) # creates a build for intralot from the latest tag if no --tag options is specified 
npm run git-tag  -- --tag x.x.x  # Tags current commit according to sem versioning  (edits package.json and meta.json)
 
```

### Setting up Sublime Text 3 for linting 

Install globaly eslint, babel-eslint

```sh
npm install -g eslint@latest
npm install -g babel-eslint@latest
```

You probably already have Package Control by Will Bond if you use Sublime Text (for installation of package Control go [here](https://packagecontrol.io/installation)).
 If you don’t, install it so you can manage Sublime plugins easily.
 You’ll need to also install 

 *  __SublimeLinter__
 *  __SublimeLinter-contrib-eslint__
 *  __babel__
 *  __babel-sublime__
 
