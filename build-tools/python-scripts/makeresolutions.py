#!/usr/bin/env python
from __future__ import with_statement
import glob
import os
import tempfile
import json
from string import Template
import shutil

TEMP_ROOT = tempfile.gettempdir()
ASSETJS = os.path.join('src', 'scripts', 'data', 'assets.js')

build_tools_dir = os.path.split(os.path.dirname(os.path.realpath(__file__)))[0]
root_dir = os.path.split(build_tools_dir)[0]

ASSETJSDIR = os.path.join(root_dir, ASSETJS)

writing = True
with open('package.json', 'r') as data_file:
    data = json.load(data_file)

# Remove variable RESOLUTION and it's components
file = ASSETJS
filename = os.path.split(file)[-1]
temp_file = os.path.join(TEMP_ROOT, filename)

print file, filename, tempfile
with open(file) as f:
    with open(temp_file, 'w+') as out:
        for line in f:
            if writing:
                if "RESOLUTIONS" in line:
                    writing = False
                elif "export" in line:
                    writing = False
                else:
                    out.write(line)


# Write new variable at the end of the file
s = Template('''
var RESOLUTIONS = {
  'HDR':$HDR,
  'HD': $HD,
  'SD': $SD
};
export {HDR, HD, SD, COMMON, AMBIENT, RESOLUTIONS};
''')

result = s.substitute(HDR= data['devConfig']['resolutions']['HDR'], HD= data['devConfig']['resolutions']['HD'], SD= data['devConfig']['resolutions']['SD'])

pattern = 'export'
with open(temp_file,'r') as out:
    lines = out.readlines()

lines.insert(-1, '\n')
lines.insert(-1, result.lstrip())
with open(temp_file,'w') as new_file:
    for line in lines:
        new_file.write(line)

os.remove(file)
shutil.move(temp_file, file)