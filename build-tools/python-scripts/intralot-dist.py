#!/usr/bin/env python
import os
import zipfile
import glob
import shutil
import subprocess
import json
import datetime
import re
from pybars import Compiler
import io

######################################################
#
# Inititalization
#
######################################################

# read values from package.json
with open('package.json') as data_file:
    data = json.load(data_file)

tag = data['version']
name = re.sub('[\s+]', '', data['title']).replace("'", "")
date = datetime.datetime.now().strftime("%d_%m_%Y")


INTRALOT_DIST = 'intralot-dist'
LAST_BUILD = os.path.join('intralot-dist', 'last_build')
BUILD_FOLDER = name + '_' + date + '_v' + tag
CHANGELOG_TEMPLATE = os.path.join('src', 'views', 'templates', 'CHANGELOG.md')

print name
print tag
print date
print INTRALOT_DIST
print LAST_BUILD
print BUILD_FOLDER

######################################################
#
# Move all files from last_build to intralo-dist
#
######################################################
items = glob.glob(os.path.join(LAST_BUILD, '*'))

for item in items:
    path, file = os.path.split(item)
    print item, os.path.exists(os.path.join(INTRALOT_DIST, file))
    if(os.path.exists(os.path.join(INTRALOT_DIST, file))):
        if(os.path.isdir(item)):
            shutil.rmtree(item)
        else:
            os.remove(item)
    else:
        shutil.move(item, INTRALOT_DIST)
######################################################
#
# call buildversion.py
#
######################################################
subprocess.check_output(
    'python build-tools/python-scripts/buildversion.py ' +
    tag +
    ' ' +
    LAST_BUILD +
    ' ' +
    BUILD_FOLDER +
    ' ' +
    name,
    shell=True)

######################################################
#
# call md5fier.py
#
######################################################
subprocess.check_output(
    'python build-tools/python-scripts/md5fier.py ' +
    name +
    ' ' +
    os.path.join(
        LAST_BUILD,
        BUILD_FOLDER,
        name),
    shell=True)

######################################################
#
# call changelog.py
#
######################################################
out = subprocess.check_output(
    'git describe --abbrev=0 ' + tag + '^^',
    shell=True)
last_tag = out.split()[0]

out = subprocess.check_output(
    'python build-tools/python-scripts/changelog.py ' +
    tag +
    ' ' +
    last_tag,
    shell=True)
json_data = json.loads(out)
print json_data

compiler = Compiler()

with io.open(CHANGELOG_TEMPLATE, encoding='utf-8') as changelog:
    template = compiler.compile(changelog.read())

data = {
    'date': datetime.datetime.now().strftime("%d %B %Y"),
    'version': tag,
    'commits': json_data
}
output = template(data)

with open(os.path.join(LAST_BUILD, BUILD_FOLDER, 'CHANGELOG.md'), 'w+') as changelog:
    changelog.write(''.join(output))
    changelog.close()

######################################################
#
# call md5fier
#
######################################################
subprocess.check_output(
    'python build-tools/python-scripts/zipgame.py',
    shell=True)