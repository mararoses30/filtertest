#!/usr/bin/env python
import os
import subprocess
import glob
import shutil
import hashlib
import string
import time
import datetime
import sys
import re
import json
from subprocess import Popen, PIPE, STDOUT

cmdargs = sys.argv
# Current tag
current_tag = cmdargs[1]
# previous tag
previous_tag = cmdargs[2]

p = Popen(['git', 'log', current_tag + '...' + previous_tag,
           '--format="%B"'], stdout=PIPE, stderr=STDOUT)
(pout, error) = p.communicate()

data = {
    'bugfixes': [],
    'updates': [],
    'features': []
}


def regText(string, pout):
    data = []
    for line in pout.splitlines():
        # print 5 * '*' + line
        result = re.search('\[\s*' + string + '\s*\]', line, re.I)
        # print result
        if result:
            new_line = line.replace(result.group(0), '')
            new_line = re.sub(
                r'^\s*"', '', new_line).lstrip().rstrip().capitalize()
            data.append(new_line)
    return data

data['bugfixes'] = regText('bugfix', pout)
data['updates'] = regText('update', pout)
data['features'] = regText('feature', pout)

print json.dumps(data)
