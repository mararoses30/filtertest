#!/usr/bin/env python

# Find if HDR, HD and SDS folder exist

import os
import glob
import shutil

EXPORT = os.path.join('resources', 'TP_exports')
LOCALE = os.path.join(EXPORT, 'locale')
MAIN = os.path.join('static', 'assets')

build_tools_dir = os.path.split(os.path.dirname(os.path.realpath(__file__)))[0]
root_dir = os.path.split(build_tools_dir)[0]

exportdir = os.path.join(root_dir, EXPORT)
maindir = os.path.join(root_dir, MAIN)
localedir = os.path.join(root_dir, LOCALE)

directories = ['HDR', 'HD', 'SD']

# find local folders
if(os.path.exists(LOCALE)):
    locale_IDs = os.listdir(LOCALE)


for directory in directories:
    if not os.path.exists(os.path.join(maindir, directory)):
        os.makedirs(os.path.join(maindir, directory))

    if(os.path.exists(LOCALE)):
        for locale in locale_IDs:
            # print directory, locale
            if not os.path.exists(os.path.join(maindir, directory, locale)):
                os.makedirs(os.path.join(maindir, directory, locale))

if(os.path.exists(LOCALE)):
    for locale in locale_IDs:
        # Find all {filename}@2.json and {filename}@2.png and move the  to HD
        files3x = glob.glob(
            os.path.join(
                exportdir,
                'locale',
                locale,
                '*@3x*' +
                locale +
                '*'))
        print files3x
        for filename in files3x:
            shutil.copy2(os.path.join(root_dir,
                                      filename),
                         os.path.join(root_dir,
                                      MAIN,
                                      'HDR',
                                      locale,
                                      os.path.split(filename)[-1].replace('@3x',
                                                                          '').replace('_' + locale,
                                                                                      '')))

        # Find all {filename}@2.json and {filename}@2.png and move the  to HD
        files2x = glob.glob(
            os.path.join(
                exportdir,
                'locale',
                locale,
                '*@2x*' +
                locale +
                '*'))
        for filename in files2x:
            shutil.copy2(os.path.join(root_dir,
                                      filename),
                         os.path.join(root_dir,
                                      MAIN,
                                      'HD',
                                      locale,
                                      os.path.split(filename)[-1].replace('@2x',
                                                                          '').replace('_' + locale,
                                                                                      '')))

        # Find all remaining files  and move the  to SD
        files1x = glob.glob(
            os.path.join(
                exportdir,
                'locale',
                locale,
                '*@1x*' +
                locale +
                '*'))
        for filename in files1x:
            shutil.copy2(os.path.join(root_dir,
                                      filename),
                         os.path.join(root_dir,
                                      MAIN,
                                      'SD',
                                      locale,
                                      os.path.split(filename)[-1].replace('@1x',
                                                                          '').replace('_' + locale,
                                                                                      '')))


# Find all {filename}@3.json and {filename}@3.png and move the  to HDR
files3x = glob.glob(os.path.join(exportdir, '*@3x*'))
for filename in files3x:
    shutil.copy2(os.path.join(root_dir, filename), os.path.join(
        root_dir, MAIN, 'HDR', os.path.split(filename)[-1].replace('@3x', '')))

# Find all {filename}@2.json and {filename}@2.png and move the  to HD
files2x = glob.glob(os.path.join(exportdir, '*@2x*'))
for filename in files2x:
    shutil.copy2(os.path.join(root_dir, filename), os.path.join(
        root_dir, MAIN, 'HD', os.path.split(filename)[-1].replace('@2x', '')))

# Find all remaining files  and move the  to SD
files1x = glob.glob(os.path.join(exportdir, '*@1x*'))
for filename in files1x:
    shutil.copy2(os.path.join(root_dir, filename), os.path.join(
        root_dir, MAIN, 'SD', os.path.split(filename)[-1].replace('@1x', '')))
