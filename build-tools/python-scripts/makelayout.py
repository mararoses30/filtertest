#!/usr/bin/env python
import os
import json
import glob
from copy import deepcopy

resolutions = ['HDR', 'HD', 'SD']

with open('package.json', 'r') as data_file:
    data = json.load(data_file)

scaling_factors = {
    'HDR': data['devConfig']['resolutions']['HDR'],
    'HD': data['devConfig']['resolutions']['HD'],
    'SD': data['devConfig']['resolutions']['SD']}

build_tools_dir = os.path.split(os.path.dirname(os.path.realpath(__file__)))[0]
basedir = os.path.split(build_tools_dir)[0]

LAYOUT_DIR = os.path.join(basedir, 'resources', 'layout', 'layout.json')
HITAREA_DIR = os.path.join(basedir, 'resources', 'PE_exports', 'hitarea.json')
ASSETS_DIR = os.path.join(basedir, 'static', 'assets')

has_layout = False
has_hitarea = False

# Try to open layout.json if there is any
try:
    with open(LAYOUT_DIR) as f_layout:
        has_layout = True
        layout_data = json.load(f_layout)
        f_layout.close()
except IOError as e:
    print e
    has_layout = False

# Try to open hitarea.json if there is any
try:
    with open(HITAREA_DIR) as f_hitarea:
        has_hitarea = True
        hitarea_data = json.load(f_hitarea)
        f_hitarea.close()
except IOError as e:
    has_hitarea = False


def makeLayout(resolution):
    data = deepcopy(layout_data)
    for key, value in layout_data.iteritems():
        for key1, value1 in enumerate(value):# array
          for key2, value2 in value1.iteritems(): # dicts
              if type(value2) is dict:
                for key3, value3 in value2.iteritems(): # dicts
                  if type(value3) is float or type(value3) is int:
                    data[key][key1][key2][key3] = data[key][key1][key2][key3] * scaling_factors[resolution]
              elif type(value2) is list:
                for key3, value3 in enumerate(value2):# array
                  if type(value3) is dict:
                    for key4, value4 in value3.iteritems(): # dicts
                      if type(value4) is float or type(value4) is int:
                        data[key][key1][key2][key3][key4] = data[key][key1][key2][key3][key4] * scaling_factors[resolution]
              elif type(value2) is int or type(value2) is float:# int or float
                  data[key][key1][key2] = data[key][key1][key2] * scaling_factors[resolution]
    return data


def makeHitarea(resolution):
    data = deepcopy(hitarea_data)
    for key, value in hitarea_data.iteritems():
        # Multiply with the apropriate scale factor its x,y inside hitarea
        for point in range(len(hitarea_data[key][0]['shape'])):
            # get the dictionary keys and iterate
            for attr in hitarea_data[key][0]['shape'][point].keys():
                data[key][0]['shape'][point][
                    attr] *= scaling_factors[resolution]

    return data


def writeToFile(data, resolution):
    with open(os.path.join(ASSETS_DIR, resolution, 'layout.json'), 'w') as f:
        f.write(json.dumps(data, sort_keys=True, indent=4))
    f.close()

# NO hitarea only layout
if has_layout and not has_hitarea:
    for resolution in resolutions:
        data = {}
        layout = makeLayout(resolution)
        data['layout'] = layout
        writeToFile(data, resolution)

# No layout.json only hitarea.json
elif has_hitarea and not has_layout:
    for resolution in resolutions:
        data = {}
        hitarea = makeHitarea(resolution)

        data['hitareas'] = hitarea
        writeToFile(hitarea, resolution)

# layout.json and hitarea.json both exist
elif has_hitarea and has_layout:
    for resolution in resolutions:
        data = {}
        layout = makeLayout(resolution)
        hitarea = makeHitarea(resolution)
        data['layout'] = layout
        data['hitareas'] = hitarea

        writeToFile(data, resolution)

elif not has_layout and not has_hitarea:
    # no need to do anythong
    pass

print has_layout, has_hitarea
