#!/usr/bin/env python
import os
import glob
import subprocess as sp
from subprocess import call, check_call

build_tools_dir = os.path.split(os.path.dirname(os.path.realpath(__file__)))[0]
root_dir = os.path.split(build_tools_dir)[0]

SOUNDS_SOURCE_DIR = os.path.join(root_dir, 'resources', 'sfx')
SOUNDS_OUTPUT_DIR = os.path.join(root_dir, 'static', 'assets', 'iesounds')


if not os.path.exists(SOUNDS_OUTPUT_DIR):
    os.makedirs(SOUNDS_OUTPUT_DIR)

if os.name == 'nt':
    FFMPEG_BIN = "ffmpeg.exe"  # on Windows
else:
    FFMPEG_BIN = "ffmpeg"  # on Linux ans Mac OS


def ffcommand(inputFile, outFile):
    command = [FFMPEG_BIN,
               '-y',
               '-i', inputFile,
               '-acodec', 'libmp3lame',
               '-ar', '44100',
               '-ab', '128k',
               outFile.replace('wav', 'mp3')
               ]
    print command
    p = check_call(command)


audioFiles = glob.glob(os.path.join(SOUNDS_SOURCE_DIR, '*.wav'))

for filename in audioFiles:
    print os.path.join(SOUNDS_OUTPUT_DIR, os.path.basename(filename))
    ffcommand(
        filename,
        os.path.join(
            SOUNDS_OUTPUT_DIR,
            os.path.basename(filename)))
