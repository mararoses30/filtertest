#!/usr/bin/env python
import os
from mmap import mmap,ACCESS_READ
from xlrd import open_workbook
import json
import glob
import sys 

cmdargs = sys.argv

print cmdargs

build_tools_dir = os.path.split(os.path.dirname(os.path.realpath(__file__)))[0]
basedir = os.path.split(build_tools_dir)[0]

EXCEL_DIR = os.path.join(basedir, 'offline', 'langs')
OFFLINE_DIR = os.path.join(basedir, 'offline')


# argument should be the xlsx file that resides in offline/langs/name_of_excel.xlsx
if len(cmdargs) > 1:
    latest_file = os.path.join(EXCEL_DIR, cmdargs[1]) 
else: # get the latest by time xlsx file 
    list_of_files = glob.glob(EXCEL_DIR + '/*.xlsx') # * means all if need specific format then *.csv
    latest_file = max(list_of_files, key=os.path.getctime)

wb = open_workbook(latest_file)
# with open(EXCEL_DIR,'rb') as f:
#  print open_workbook(
#  file_contents=mmap(f.fileno(),0,access=ACCESS_READ)
#  )
# aString = open(EXCEL_DIR,'rb').read()
# print open_workbook(file_contents=aString)
def localeIndex (locales, locale_id):
    for index, item in enumerate(locales):
        if item ==  locale_id:
            return index

for s in wb.sheets():
    if s.name == 'In-game_Text_Fields':
        # Find all locales 
        locales = []
        for col in range(s.ncols):
            print s.cell(0,col).value.strip()
            locales.append(s.cell(0,col).value)
        
        locale_list = []
        
        for locale_id in locales:
            if locale_id != 'TokenID' and locale_id != 'Comment':
                locale_idx = localeIndex(locales, locale_id)
                locale_dict = {}
                locale_dict["Locale"] = locales[locale_idx].lower();
                locale_dict['Tokens'] = []


                for row in range(1, s.nrows):
                    token = {}
                    token['TokenID'] = s.cell(row, 0).value
                    token['Text'] = s.cell(row, locale_idx).value
                    token['CData'] = None
                    locale_dict['Tokens'].append(token)

                locale_list.append(locale_dict)            

def writeToFile(data):
    with open(os.path.join(OFFLINE_DIR, 'langs', 'langs.json'), 'w') as f:
        f.write(json.dumps(data, sort_keys=True, indent=4))
    f.close()

writeToFile(locale_list)