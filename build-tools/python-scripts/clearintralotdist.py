#!/usr/bin/env python
import os
import zipfile
import glob
import shutil
import subprocess
import json


INTRALOT_DIST = 'intralot-dist'

items = glob.glob(os.path.join(INTRALOT_DIST, '*'))
items = set(glob.glob(os.path.join(INTRALOT_DIST,
                                   '*'))) - set(glob.glob(os.path.join(INTRALOT_DIST,
                                                                       'last_build'))) - set(glob.glob(os.path.join(INTRALOT_DIST,
                                                                                                                    'README')))

for item in items:
    path, file = os.path.split(item)
    if(os.path.isdir(item)):
        shutil.rmtree(item)
    else:
        os.remove(item)
