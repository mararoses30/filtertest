#!/usr/bin/env python
import os
dir = os.path.dirname(__file__)
import glob
import shutil
import hashlib
import string
import time
import datetime
import sys

cmdargs = sys.argv


def timeinString():
    ts = time.time()
    return datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

GAMETITLE = cmdargs[1]
md5sum = hashlib.sha224(
    GAMETITLE +
    timeinString()).hexdigest()[
        0:12]  # get 12 digits

build_tools_dir = os.path.split(os.path.dirname(os.path.realpath(__file__)))[0]
root_dir = os.path.split(build_tools_dir)[0]

gameDir = os.path.join(root_dir, cmdargs[2])

# sys.exit(0)
# Locate all assets
gameJSlist = []
htmllist = []
csslist = []
sfxlist = []

ASSETDIR = os.path.join(gameDir, 'assets')
HDRDIR = os.path.join(gameDir, 'assets', 'HDR')
HDDIR = os.path.join(gameDir, 'assets', 'HD')
SDDIR = os.path.join(gameDir, 'assets', 'SD')
LANGSDIR = os.path.join(gameDir, 'langs')

# PNG files  from HDR, HD, SD
HDRPNGfiles = glob.glob(os.path.join(HDRDIR, '*png'))
gameJSlist.append(HDRPNGfiles)
localeHDRPNGfiles = glob.glob(os.path.join(HDRDIR, '*', '*png'))
gameJSlist.append(localeHDRPNGfiles)
HDPNGfiles = glob.glob(os.path.join(HDDIR, '*png'))
gameJSlist.append(HDPNGfiles)
localeHDPNGfiles = glob.glob(os.path.join(HDDIR, '*', '*png'))
gameJSlist.append(localeHDPNGfiles)
SDPNGfiles = glob.glob(os.path.join(SDDIR, '*png'))
gameJSlist.append(SDPNGfiles)
localeSDPNGfiles = glob.glob(os.path.join(SDDIR, '*', '*png'))
gameJSlist.append(localeSDPNGfiles)

# JPG files  from HDR, HD, SD
HDRJPGfiles = glob.glob(os.path.join(HDRDIR, '*jpg'))
gameJSlist.append(HDRJPGfiles)
localeHDRJPGfiles = glob.glob(os.path.join(HDRDIR, '*', '*jpg'))
gameJSlist.append(localeHDRJPGfiles)
HDJPGfiles = glob.glob(os.path.join(HDDIR, '*jpg'))
gameJSlist.append(HDJPGfiles)
localeHDJPGfiles = glob.glob(os.path.join(HDDIR, '*', '*jpg'))
gameJSlist.append(localeHDJPGfiles)
SDJPGfiles = glob.glob(os.path.join(SDDIR, '*jpg'))
gameJSlist.append(SDJPGfiles)
localeSDJPGfiles = glob.glob(os.path.join(SDDIR, '*', '*jpg'))
gameJSlist.append(localeSDJPGfiles)

# json files  from HDR, HD, SD
HDRJSONfiles = glob.glob(os.path.join(HDRDIR, '*json'))
gameJSlist.append(HDRJSONfiles)
localeHDRJSONfiles = glob.glob(os.path.join(HDRDIR, '*', '*json'))
gameJSlist.append(localeHDRJSONfiles)
HDJSONfiles = glob.glob(os.path.join(HDDIR, '*json'))
gameJSlist.append(HDJSONfiles)
localeHDJSONfiles = glob.glob(os.path.join(HDDIR, '*', '*json'))
gameJSlist.append(localeHDJSONfiles)
SDJSONfiles = glob.glob(os.path.join(SDDIR, '*json'))
gameJSlist.append(SDJSONfiles)
localeSDJSONfiles = glob.glob(os.path.join(SDDIR, '*', '*json'))
gameJSlist.append(localeSDJSONfiles)

# json files  jangs
LANGJSONfiles = glob.glob(os.path.join(LANGSDIR, '*json'))
gameJSlist.append(LANGJSONfiles)

# m4a files  jangs
M4afiles = glob.glob(os.path.join(ASSETDIR, '*m4a'))
gameJSlist.append(M4afiles)

# mp3 files  jangs
MP3files = glob.glob(os.path.join(ASSETDIR, '*mp3'))
gameJSlist.append(MP3files)

# ogg files  jangs
OGGiles = glob.glob(os.path.join(ASSETDIR, '*ogg'))
gameJSlist.append(OGGiles)

# music json files
SOUNDJSONfiles = glob.glob(os.path.join(ASSETDIR, '*json'))
gameJSlist.append(SOUNDJSONfiles)

# woff files
WOFFfiles = glob.glob(os.path.join(ASSETDIR, '*woff'))
csslist.append(WOFFfiles)

# png css  files
rotateCSSfiles = glob.glob(os.path.join(ASSETDIR, '*rotate.png'))
csslist.append(rotateCSSfiles)

# png css  files
swipeCSSfiles = glob.glob(os.path.join(ASSETDIR, '*swipe.png'))
csslist.append(swipeCSSfiles)

# CSS file
CSSfiles = glob.glob(os.path.join(gameDir, '*css'))
htmllist.append(CSSfiles)

# game.js file
GAMEfiles = glob.glob(os.path.join(gameDir, '*game.js'))
htmllist.append(GAMEfiles)

# game.js file
sfxfile = glob.glob(os.path.join(ASSETDIR, '*sfx.*'))
sfxlist.append(sfxfile)


def injectMD5(string):
    splitted = string.split('.')
    new_string = '.'.join([splitted[0], md5sum, splitted[-1]])
    return new_string


def renamefileWithMD5(filename, filename_stripped):
    os.rename(
        filename,
        os.path.join(
            os.path.split(filename)[0],
            injectMD5(filename_stripped)))


def replaseinFile(file, listoffilenames, true):
    for filename in listoffilenames:
        filename_stripped = os.path.split(filename)[-1]
        print filename
        with open(file) as f:
            contents = f.read()
            replaced_contents = string.replace(
                contents, filename_stripped, injectMD5(filename_stripped))

        with open(file, 'w') as f:
            f.write(replaced_contents)
            if(true):
                renamefileWithMD5(filename, filename_stripped)


def winFontReplace(list, true):
    # iterate list of asset lists
    for listitem in list:
        replaseinFile(os.path.join(gameDir, 'game.js'), listitem, true)
        print 50 * '--'


def isSFXReplace(list, true):
    # iterate list of asset lists
    for listitem in list:
        replaseinFile(
            os.path.join(
                gameDir,
                'assets',
                'sfx.json'),
            listitem,
            true)
        print 50 * '--'


def gameJSReplace(list, true):
    # iterate list of asset lists
    for listitem in list:
        replaseinFile(os.path.join(gameDir, 'game.js'), listitem, true)
        print 50 * '--'


def cssReplace(list, true):
    # iterate list of asset lists
    for listitem in list:
        replaseinFile(os.path.join(gameDir, 'styles.css'), listitem, true)
        print 50 * '--'


def htmlReplace(list, true):
    # iterate list of asset lists
    for listitem in list:
        replaseinFile(os.path.join(gameDir, 'index.html'), listitem, true)
        print 50 * '--'

isSFXReplace(sfxlist, False)
gameJSReplace(gameJSlist, True)
cssReplace(csslist, True)
htmlReplace(htmllist, True)
