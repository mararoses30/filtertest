#!/usr/bin/env python
import sys
import os
import subprocess
dir = os.path.dirname(__file__)
import shutil
import errno
import json
import stat
import fileinput
import tempfile
import hashlib
import datetime


# create unic alphanuemeric
md5 = hashlib.md5()
datetime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
md5.update(datetime)
md5srting = md5.hexdigest()

cmdargs = sys.argv

# Tag
tag = cmdargs[1]
# dist folder
dest_build_root_folder = cmdargs[2]
# build folder
dest_build_folder = cmdargs[3]
# game's asset folder
dest_build_folder_game = cmdargs[4]

# Copy game to temp folder
build_tools_dir = os.path.split(os.path.dirname(os.path.realpath(__file__)))[0]
root_dir = os.path.split(build_tools_dir)[0]

print build_tools_dir, root_dir


TEMP_ROOT = tempfile.gettempdir()
TEMP_GAMEFODER = 'tmpgame' + md5srting

temp_root = os.path.join(TEMP_ROOT, TEMP_GAMEFODER)
temp_dir = os.path.join(TEMP_ROOT, TEMP_GAMEFODER, dest_build_folder_game)


def stringTitle():
    a = 79 * '*' + '\n'
    a += 10 * ' ' + 'COMMIT MESSAGES\n'
    a += 79 * '*' + '\n'
    return a


def toLowercase():
    with open('package.json', 'r') as data_file:
        data = json.load(data_file)
        data_file.close()

    # # HACK HACK in
    if 'name' in data:
        data['name'] = data['name'].lower().replace(" ", "")
        with open('package.json', 'w+') as data_file:
            json.dump(data, data_file)
            data_file.close()


# remove old temp folder ifexists
if os.path.exists(temp_root):
    try:
        shutil.rmtree(temp_root)
    except IOError as e:
        print "Unable to delete directory. %s" % e
# create destination folders
if not os.path.exists(os.path.join(root_dir, dest_build_root_folder)):
    os.makedirs(os.path.join(root_dir, dest_build_root_folder))

if not os.path.exists(
    os.path.join(
        root_dir,
        dest_build_root_folder,
        dest_build_folder)):
    os.makedirs(
        os.path.join(
            root_dir,
            dest_build_root_folder,
            dest_build_folder))

# Switched to cloned project directory
# check if there is a key "name" inside package.json and make it lowercase
toLowercase()

print 'the dir is ' + temp_dir

# copy gulp tasks
shutil.copytree(
    os.path.join(
        root_dir,
        'build-tools'),
    os.path.join(
        temp_dir,
        'build-tools'))

# copy package.json
shutil.copyfile(
    os.path.join(
        root_dir, 'package.json'), os.path.join(
            temp_dir, 'package.json'))

# copy gulpfile.js
shutil.copyfile(
    os.path.join(
        root_dir, 'gulpfile.js'), os.path.join(
            temp_dir, 'gulpfile.js'))


subprocess.check_call('git reset --hard ', shell=True)
# delete previous existing build branch
try:
    subprocess.check_call('git branch -D  build-' + tag, shell=True)
except subprocess.CalledProcessError as e:
    print e.output

try:
    subprocess.check_call('git tag -d  build-' + tag, shell=True)
except subprocess.CalledProcessError as e:
    print e.output

subprocess.check_call('git checkout ' + tag + ' -b build-' + tag, shell=True)
toLowercase()

# removed past directories
if os.path.exists(os.path.join(root_dir, 'build-tools')):
    shutil.rmtree(os.path.join(root_dir, 'build-tools'))

# copy current gulp file
# due to older version of build tools gulpfile might appera in the folder so
# we have to check if exists and then build
if os.path.exists(os.path.join(root_dir, 'gulpfile.js')):
    try:
        shutil.rmtree(os.path.join(root_dir, 'gulpfile.js'))
    except OSError as e:
        print "Unable to delete directory. %s" % e
        pass

shutil.copyfile(
    os.path.join(
        temp_dir, 'gulpfile.js'), os.path.join(
            root_dir, 'gulpfile.js'))
shutil.copyfile(
    os.path.join(
        temp_dir, 'package.json'), os.path.join(
            root_dir, 'package.json'))
shutil.copytree(
    os.path.join(
        temp_dir,
        'build-tools'),
    os.path.join(
        root_dir,
        'build-tools'))

subprocess.check_call('gulp dist ', shell=True)

# move produces dist folder to inside intralot-dist folder
shutil.move(
    os.path.join(
        root_dir,
        'dist'),
    os.path.join(
        root_dir,
        dest_build_root_folder,
        dest_build_folder,
        dest_build_folder_game))

subprocess.check_call('git clean -fd ', shell=True)  # clean untracked files
subprocess.check_call('git reset --hard ', shell=True)
subprocess.check_call('git tag  build-' + tag, shell=True)
subprocess.check_call('git checkout master ', shell=True)
subprocess.check_call('git branch -D  build-' + tag, shell=True)

# add a tag which indicates that this commit was build

#
# remove tmp folder
#
shutil.rmtree(temp_root)
