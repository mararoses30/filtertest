#!/usr/bin/env python
import os
import zipfile
import glob
import zipfile
import tempfile
import shutil
game = glob.glob(os.path.join('intralot-dist', 'last_build', '*'))



def zip(src, dst):
    zf = zipfile.ZipFile("%s.zip" % (dst), "w", zipfile.ZIP_DEFLATED)
    abs_src = os.path.abspath(src)
    for dirname, subdirs, files in os.walk(src):
        for filename in files:
            absname = os.path.abspath(os.path.join(dirname, filename))
            arcname = absname[len(abs_src) + 1:]
            zf.write(absname, os.path.join(os.path.split(game[0])[-1],arcname))
    zf.close()

zip(game[0], game[0])
