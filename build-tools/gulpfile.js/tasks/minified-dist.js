/*
 * Distribution tasks.
 * ============================================================================
 */

'use strict';


module.exports = function (gulp, $, config) {

  var del = require('del');
  var stripDebug = require('gulp-strip-debug');

  var dirs    = config.dirs;
  var globs   = config.globs;
  var options = config.pluginOptions;
  var fs = require('fs-extra');

  // Wipes `build` and `dist` directories before any task.
  gulp.task('minified-dist:clean', function () {
    return del([ dirs.build, dirs.dist ]);
  });

  // Process any markup files for distribution.
  gulp.task('minified-dist:views', [ 'dev:build:views', 'dev:gitversion'], function () {
    return gulp.src(dirs.build + '/*.html')
      .pipe($.processhtml())
      .pipe(gulp.dest(dirs.dist));
  });

  // Copy and minify all style sheet files.
  gulp.task('minified-dist:styles', [ 'dev:build:styles' ], function () {
    return gulp.src(dirs.build + '/*.css')
      .pipe($.minifyCss(options['minified-dist:styles']))
      .pipe(gulp.dest(dirs.dist));
  });

  // Bundle all scripts together for distribution.
  gulp.task('minified-dist:scripts', [ 'dev:copy:configjson', 'dev:build:scripts', 'dev:copy:gamerules', 'dev:copy:offline'], function () {
    var config = JSON.parse(fs.readFileSync('./package.json', 'utf-8'));
    var phaser = config['devConfig']['phaser'];
    return gulp.src([
      phaser,
      dirs.build + '/game.js'
    ])
      .pipe($.concat('game.js'))
      .pipe(stripDebug())
      .pipe($.sourcemaps.write('.'))
      .pipe(gulp.dest(dirs.dist));
  });

  // Copy all dependent application assets into the final build directory.
  gulp.task('minified-dist:assets', function () {
    return gulp.src(globs.assets)
      .pipe(gulp.dest(dirs.dist));
  });

  gulp.task('minified-dist:copy:configjson', ['minified-dist:scripts'], function (){
    return gulp.src(dirs.build + '/config.json')
      .pipe(gulp.dest(dirs.dist));
  });

  gulp.task('minified-dist:copy:gamerules', function (){
    console.log(config.dirs.gamerules);
    return gulp.src(config.dirs.gamerules + '/**/*')
      .pipe(gulp.dest(dirs.dist + '/gamerules'));
  });

  gulp.task('minified-dist:copy:offline', function (){
    console.log(config.dirs.offline);
    return gulp.src(config.dirs.offline + '/**/*')
      .pipe(gulp.dest(dirs.dist + '/offline'));
  });

  // The main distribution task.
  gulp.task('minified-dist', [ 'minified-dist:clean' ], function (done) {
    gulp.start([
      'minified-dist:views',
      'minified-dist:assets',
      'minified-dist:styles',
      'minified-dist:scripts',
      'minified-dist:copy:configjson',
      'minified-dist:copy:gamerules',
      'minified-dist:copy:offline',
    ], done);
  });

};
