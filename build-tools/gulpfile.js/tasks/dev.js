/*
 * Development tasks.
 * ============================================================================
 */

'use strict';


module.exports = function (gulp, $, config) {

  // Are we in development mode?
  var isWatching = false;
  var git_rev = require('git-rev');
  var bump = require('gulp-bump');
  var gutil = require('gulp-util');
  var chalk = require('chalk');
  var buffer       = require('vinyl-buffer');
  var source       = require('vinyl-source-stream');
  var browserSync  = require('browser-sync').create();
  var autoprefixer = require('autoprefixer');
  var exec = require('child_process').exec;
  var gulp_exec = require('gulp-exec');
  var ttf2woff = require('gulp-ttf2woff');
  var notify = require('gulp-notify');
  var fs = require('fs-extra');
  var handleErrors = notify.onError('<%= error.message %>');
  var git = require('gulp-git');
  var semver = require('semver');
  var dirs  = config.dirs;
  var globs = config.globs;

  var bundler = require('./helpers/bundler');

  // Compile template views into HTML files.
  gulp.task('dev:build:views', ['dev:lint'], function () {
    var viewsGlobs = globs.views;

    return gulp.src(viewsGlobs.templates)
      .pipe($.hb({
        data: viewsGlobs.data,
        partials: viewsGlobs.partials,
        bustCache: true
      }))
      .pipe($.rename({ extname: '.html' }))
      .pipe(gulp.dest(dirs.build))
      .pipe(browserSync.stream());
  });

  // Compile style sheet templates, prefix proposed and non-standard rules.
  gulp.task('dev:build:styles', ['dev:build:views'], function () {
    return gulp.src(globs.styles)
      .pipe($.sourcemaps.init())
      .pipe($.less())
      .on('error', handleErrors)
      .pipe($.postcss([
        autoprefixer()
      ]))
      .pipe($.sourcemaps.write('.'))
      .pipe(gulp.dest(dirs.build))
      .pipe(browserSync.stream({ match: '**/*.css' }));
  });

  gulp.task('dev:copy:LobbyProxy', [], function (){
    console.log(config.dirs.extscripts + '/*.js');
    return gulp.src(config.dirs.extscripts + '/*')
      .pipe(gulp.dest(dirs.build));
  });

  gulp.task('dev:copy:configjson', ['dev:copy-phaser'], function (){
    console.log(config.dirs.configuration);
    return gulp.src(config.dirs.configuration)
      .pipe(gulp.dest(dirs.build));
  });

  gulp.task('dev:copy:gamerules', ['dev:copy:configjson'], function (){
    console.log(config.dirs.gamerules);
    return gulp.src(config.dirs.gamerules + '/**/*')
      .pipe(gulp.dest(dirs.build + '/gamerules'));
  });

  gulp.task('dev:copy:offline', ['dev:copy:gamerules'], function (){
    console.log(config.dirs.offline);
    return gulp.src(config.dirs.offline + '/**/*')
      .pipe(gulp.dest(dirs.build + '/offline'));
  });

  // Bundle the application source code using Browserify.
  gulp.task('dev:build:scripts', ['dev:lint'], function () {
    return bundler(config.bundle, isWatching)
      .bundle()
      .on('error', handleErrors)
      .pipe(source('game.js'))
      .pipe(buffer())
      .pipe($.sourcemaps.init({ loadMaps: true }))
      .pipe($.sourcemaps.write('.'))
      .pipe(gulp.dest(dirs.build))
      .pipe(browserSync.stream());
  });
  // Copy a chosen Phaser build for development.
  gulp.task('dev:copy-resolutions', ['dev:gitversion'], function () {
    return gulp.src('./')
      .pipe(gulp_exec('python build-tools/python-scripts/makeresolutions.py'))
      .pipe(browserSync.stream());
  });

  // Copy a chosen Phaser build for development.
  gulp.task('dev:copy-phaser', function () {
    var phaser;
    var config = JSON.parse(fs.readFileSync('./package.json', 'utf-8'));

    phaser = config['devConfig']['phaser'];
    return gulp.src(phaser)
      .pipe($.rename('phaser.js'))
      .pipe(gulp.dest(dirs.build));
  });


  gulp.task('dev:build:copy-fonts', ['dev:copy:offline'], function (){
    return gulp.src(globs.fonts)
      .pipe(gulp.dest(dirs.build));
  });

  // Instantiate a live web development server for cross-browser, cross-device
  // testing.
  gulp.task('dev:server', ['dev:build:scripts'], function () {
    browserSync.init({
      open: false,
      server: {
        baseDir: [
          dirs.static,
          dirs.build
        ]
      },
      ghostMode: false,
      notify: true
    });
  });

  // Monitors files for changes, trigger rebuilds as needed.
  gulp.task('dev:watch', ['dev:server'], function () {
    isWatching = true;

    gulp.watch(globs.resources.PE_Exports, [ 'dev:build:layout']);
    gulp.watch(globs.resources.TP_Exports, [ 'dev:makeatlas']);
    //watch assetPack 'src/scripts/data/assets.js' for changes
    // and caclulate the GPU RAM size.
    gulp.watch(globs.resources.layout, [ 'dev:build:layout']);
    gulp.watch(globs.scripts, ['dev:build:scripts']);
    gulp.watch(globs.styles,  ['dev:build:styles']);
    gulp.watch([
      globs.views.templates,
      globs.views.data
    ], [ 'dev:build:views' ]);
  });

  // Pass through modified script files and issue warnings about
  // non-conformances.
  gulp.task('dev:lint', [], function () {
    return gulp.src([ globs.scripts ])
      .pipe($.cached('dev:lint'))
      .pipe($.eslint())
      .pipe($.eslint.format('stylish', process.stderr));
  });

  //greate layout file for all resolutions. Source file is under
  //resources/layout
  gulp.task('dev:build:layout', function (cb) {
    return gulp.src('./')
      .pipe(gulp_exec('python build-tools/python-scripts/makelayout.py'))
      .pipe(browserSync.stream());
  });

  // If new atlas file in resources/TP_Exports
  gulp.task('dev:makeatlas', function (){
    return gulp.src('./')
      .pipe(gulp_exec('python build-tools/python-scripts/makeatlas.py'))
      .pipe(browserSync.stream());
  });

  // Update bower, component, npm at once:
  gulp.task('dev:gitversion', ['dev:gamename'], function (){
    git_rev.tag(function (str) {

      fs.readJson('./package.json', function (err, packageObj) {

        packageObj['version'] = str;
        fs.writeJson('./package.json', packageObj, function (err) {
          console.log(err);
        });
      });

      fs.readJson('./src/views/data/meta.json', function (err, metaObj) {
        metaObj['version'] = 'v'+ str;
        fs.writeJson('./src/views/data/meta.json', metaObj, function (err) {
          console.log(err);
        });
      });

    });
  });

  gulp.task('dev:tag', function (){
    var args = require('yargs').argv;
    var arg_tag = args.tag;

    function getLatestTag (cb){
      var package_json = JSON.parse(fs.readFileSync('./package.json'));

      return package_json['version'];
    }

    exec('git tag -d jenkins', { cwd: __dirname }, function (err, stdout, stderr) {});

    if(args.jenkins){
      var tag_jenkins = 'jenkins';
    }

    if(!arg_tag){
      arg_tag = semver.inc(getLatestTag(), 'patch');
    }

    // edit package.json and meta.json
    fs.readJson('./package.json', function (err, packageObj) {

      packageObj['version'] = arg_tag;
      fs.writeJson('./package.json', packageObj, function (err) {
        console.log(err);
      });
    });

    fs.readJson('./src/views/data/meta.json', function (err, metaObj) {
      metaObj['version'] = 'v'+ arg_tag;
      fs.writeJson('./src/views/data/meta.json', metaObj, function (err) {
        exec('git add ../../../src/views/data/meta.json  ../../../package.json', { cwd: __dirname }, function (err, stdout, stderr) {
          console.log(err, stdout, stderr);
          exec('git commit  -m "Bumped to version '+ arg_tag+'"', { cwd: __dirname }, function (err, stdout, stderr) {
            console.log(err, stdout, stderr);
            git.tag(arg_tag, 'Version v'+ arg_tag, function (err) {
              if (err) throw err;
            });

            if(tag_jenkins){
              git.tag(tag_jenkins, 'Jenkins build tag indicator', function (err) {
                if (err) throw err;
              });
            }

          });
        });
      });
    });
  });

  // Update bower, component, npm at once:
  gulp.task('dev:gamename', ['dev:build:copy-fonts'], function (){
    var meta_json = JSON.parse(fs.readFileSync('src/views/data/meta.json'));
    var package_json = JSON.parse(fs.readFileSync('./package.json'));

    var game_name = meta_json.title;
    package_json['title'] = game_name.trim(); // trim trailing Whitespaces

    fs.writeJson('./package.json', package_json, function (err) {
      console.log(err);
    });
  });

  gulp.task('dev:gpuRam:calculator', function (){
    require('babel-core/register')({
      presets: [ 'es2015' ]
    });

    var assets = require('../../../src/scripts/data/assets');
    var gm = require('gm').subClass({imageMagick: true});
    var path = require('path');
    // var fs = require('fs');


    var HDRsize = 0;
    var HDsize = 0;
    var SDsize = 0;

    var counter = 0;
    var pixels;

    var promisesArr = [];

    function calculateGPUMemorySize (asset, resolution){

      var fileName = path.join(__dirname, '../../../static/assets/' + resolution + '/');
      for (var i = 0, len = asset[resolution].game.length; i < len; i++) {
        var el = asset[resolution].game[i];
        if (el.type === 'atlasJSONHash' || el.type === 'image' ){
          counter += 1;
          var file;
          var format;

          // make filename
          if(el.textureUrl  || (el.type === 'image' && el.url)){
            file = fileName + (el.textureUrl || el.url);

            format = file.split('.')[1];
          }
          else if(el.type === 'atlasJSONHash'){
            file = fileName + el.key + '.png';
            format = 'png';
          }
          else if(el.type === 'image'){
            file = fileName + el.key + '.png';
            format = 'png';
          }
          // define pixels
          if(format === 'png'){
            pixels = 4;
          }
          else if (format === 'jpg'){
            pixels = 3;
          }

          var promise = caclulateSize(file, resolution);
          promisesArr.push(promise);
        }
      }

      return Promise.all(promisesArr);
    }

    function caclulateSize (file, resolution){

      var promise = new Promise((resolve)=>{
        gm(file).size((err, size)=>{
          if(resolution === 'HDR'){
            HDRsize = HDRsize + (size.width * size.height * pixels);
          }
          else if(resolution === 'HD'){
            HDsize = HDsize + (size.width * size.height * pixels);
          }
          else if(resolution === 'SD'){
            SDsize = SDsize + (size.width * size.height * pixels);
          }
          resolve();
        });
      });
      return promise;
    }

    calculateGPUMemorySize(assets, 'HDR')
      .then(()=>{
        calculateGPUMemorySize(assets, 'HD')
        .then(()=>{
          calculateGPUMemorySize(assets, 'SD').then(()=>{
            gutil.log(chalk.green('HDR GPU ram size: ' + HDRsize/1000000 + 'Mb'));
            if(HDsize/1000000 >= 512){
              gutil.log(chalk.bgRed(chalk.black('!!!!') + 'POSSIBLE IPAD2 ERROR- PLEASE FIX - HD GPU ram size: ' + HDsize/1000000 + 'Mb'));
            }
            else if(HDsize/1000000 < 512){
              gutil.log(chalk.green('HD GPU ram size: ' + HDsize/1000000 + 'Mb'));
            }

            if(SDsize/1000000 >= 512){
              gutil.log(chalk.red('POSSIBLE IPOD 4/4s GEN ERROR- PLEASE FIX - SD GPU ram size: ' + SDsize/1000000 + 'Mb'));
            }
            else if(SDsize/1000000 < 512){
              gutil.log(chalk.green('SD GPU ram size: ' + SDsize/1000000 + 'Mb'));
            }
          });
        });
      });
  });
  //converts tiff files to woff
  gulp.task('tiff2woff', function (){
    gulp.src([dirs.resources + '/fonts/*.ttf'])
      .pipe(ttf2woff())
      .pipe(gulp.dest(dirs.static+'/assets'));
  });

  // The main development task.
  gulp.task('dev', [
    'dev:copy-phaser',
    'dev:copy:configjson',
    'dev:copy:gamerules',
    'dev:copy:offline',
    'dev:build:copy-fonts',
    'dev:gamename',
    'dev:gitversion',
    'dev:copy-resolutions',
    'dev:copy:LobbyProxy',
    'dev:lint',
    'dev:build:views',
    'dev:build:styles',
    'dev:build:scripts',
    'dev:server',
    'dev:watch'
  ]);

  // Aliasing `dev` as default task.
  gulp.task('default', [ 'dev' ]);

};
