/*
 * Distribution tasks.
 * ============================================================================
 */

'use strict';


module.exports = function (gulp, $, config) {

  var del = require('del');
  var stripDebug = require('gulp-strip-debug');

  var dirs    = config.dirs;
  var globs   = config.globs;
  var options = config.pluginOptions;
  var sourcemaps = require('gulp-sourcemaps');
  var fs = require('fs-extra');


  // Wipes `build` and `dist` directories before any task.
  gulp.task('dist:clean', function () {
    return del([ dirs.build, dirs.dist ]);
  });

  // Process any markup files for distribution.
  gulp.task('dist:views', [ 'dev:build:views' ], function () {
    return gulp.src(dirs.build + '/*.html')
      .pipe($.processhtml())
      .pipe(gulp.dest(dirs.dist));
  });

  // Copy and minify all style sheet files.
  gulp.task('dist:styles', [ 'dev:build:styles' ], function () {
    return gulp.src(dirs.build + '/*.css')
      .pipe($.rename({ extname: '.css' }))
      .pipe(sourcemaps.init({loadMaps:true}))
      .pipe(sourcemaps.write('', {addComment: false}))
      .pipe(gulp.dest(dirs.dist));
  });

  // Bundle all scripts together for distribution.
  gulp.task('dist:scripts', ['dev:copy:configjson', 'dev:build:scripts', 'dev:copy:gamerules', 'dev:copy:offline' ], function () {
    var config = JSON.parse(fs.readFileSync('./package.json', 'utf-8'));
    var phaser = config['devConfig']['phaser'];
    return gulp.src([
      phaser,
      dirs.build + '/game.js'
    ])
      .pipe($.concat('game.js'))
      .pipe(stripDebug())
      .pipe(sourcemaps.init({loadMaps:true}))
      .pipe(sourcemaps.write('', {addComment: false}))
      .pipe(gulp.dest(dirs.dist));
  });

  // Copy all dependent application assets into the final build directory.
  gulp.task('dist:assets', function () {
    return gulp.src(globs.assets)
      .pipe(gulp.dest(dirs.dist));
  });

  gulp.task('dist:copy:configjson', ['dist:scripts'], function (){
    return gulp.src(dirs.build + '/config.json')
      .pipe(gulp.dest(dirs.dist));
  });

  gulp.task('dist:copy:gamerules', function (){
    return gulp.src(config.dirs.gamerules + '/**/*')
      .pipe(gulp.dest(dirs.dist + '/gamerules'));
  });

  gulp.task('dist:copy:LobbyProxy', [], function (){
    return gulp.src(config.dirs.extscripts + '/*')
      .pipe(gulp.dest(dirs.dist));
  });

  // The main distribution task.
  gulp.task('dist', [ 'dist:clean' ], function (done) {
    gulp.start([
      'dist:views',
      'dist:assets',
      'dist:styles',
      'dist:scripts',
      'dist:copy:configjson',
      'dist:copy:gamerules',
      'dist:copy:LobbyProxy'
    ], done);
  });

};
