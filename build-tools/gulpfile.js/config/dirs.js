/*
 * Directories
 * ============================================================================
 *
 * This module keeps some data about the project directory structure.
 */

'use strict';


module.exports = {
  // Where compiled scripts and assets should be placed.
  dist: 'dist',
  intralot: 'intralot-dist',
  build: 'build',

  // Where this project source code lives.
  views: 'src/views',
  styles: 'src/styles',
  scripts: 'src/scripts',
  resources_dir: 'resources',
  gamerules: 'gamerules',
  extscripts: 'src/extscripts',
  offline: 'offline',
  // Where static assets (textures, fonts, sprites, sounds etc.) are stored.
  static: 'static',
  configuration: 'config/config.json',
  // Paths required by `slush-phaser-plus` sub-generators.
  get states  () { return this.scripts + '/states'; },
  get objects () { return this.scripts + '/objects'; },
  get plugins () { return this.scripts + '/plugins'; },
  get resources () { return this.resources_dir; }
};
